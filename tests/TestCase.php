<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use App\User;
use App\Color;
use App\Proveedor;
use App\Rubro;
use App\TipoMaterial;
use App\Complejidad;
use App\DificultadTarea;
use App\Tarea;
use App\Parametro;
use App\Material;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    public function administrador(){
        Artisan::call('db:seed', ['--class'=>'RolSeeder']);

        $administrador = User::create([
            'name'          => 'Administrador',
            'email'         => 'administrador@gmail.com',
            'password'      => 'administrador',
            'rol_id'        => 1,
        ]);

        return $administrador;
    }

    public function confeccionista(){
        Artisan::call('db:seed', ['--class'=>'RolSeeder']);

        $confeccionista = User::create([
            'name'          => 'confeccionista',
            'email'         => 'confeccionista@gmail.com',
            'password'      => 'confeccionista',
            'rol_id'        => 2,
        ]);

        return $confeccionista;
    }

    public function auditor(){
        Artisan::call('db:seed', ['--class'=>'RolSeeder']);

        $auditor = User::create([
            'name'          => 'auditor',
            'email'         => 'auditor@gmail.com',
            'password'      => 'auditor',
            'rol_id'        => 3,
        ]);

        return $auditor;
    }

    public function registrador(){
        Artisan::call('db:seed', ['--class'=>'RolSeeder']);

        $registrador = User::create([
            'name'          => 'registrador',
            'email'         => 'registrador@gmail.com',
            'password'      => 'registrador',
            'rol_id'        => 4,
        ]);

        return $registrador;
    }

    public function color(){
        $color = Color::create([
            'nombre'    => 'Azul',
        ]);

        return $color;
    }
    
    public function proveedor(){
        $proveedor = Proveedor::create([
            'nombre'    => 'Pepe',
            'telefono'  => '3758489785',
            'email'     => 'proveedor@gmail.com',
            'costo_envio' => 300,
            'demora' => 6,
        ]);

        return $proveedor;
    }

    public function rubro(){
        $rubro = Rubro::create([
            'nombre'    => 'Merceria',
        ]);

        return $rubro;
    }

    public function tipoMaterial(){
        $tipoMaterial = TipoMaterial::create([
            'nombre'                    => 'Gabardina',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => 1,
            'costo'                     => 600,
        ]);

        Parametro::create([
            'clave'     => 'valor_dolar',
            'valor'     => '79.43',
            'updated_at'=> '2020-12-13',
        ]);

        return $tipoMaterial;
    }

    public function complejidad(){
        $complejidad = Complejidad::create([
            'tipo'    => 'Baja',
            'porcentaje_ganancia' => 100,
        ]);

        return $complejidad;
    }

    public function dificultadTarea(){
        $dificultadTarea = DificultadTarea::create([
            'nombre'    => 'alta',
            'tiempo_ejecucion' => 5,
        ]);

        return $dificultadTarea;
    }

    public function tarea(){
        $tarea = Tarea::create([
            'nombre'    => 'Corte',
        ]);

        return $tarea;
    }

    public function material(){

        $material = Material::create([
            'codigo' => 'T001',
            'stock' => '1500',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => $this->tipoMaterial()->id,
            'rubro_id' => '1',
            'color_id' => '1',
        ]);

        return $material;
    }
}
