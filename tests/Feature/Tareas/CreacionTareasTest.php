<?php

namespace Tests\Feature\Tareas;

use Tests\TestCase;

class CreacionTareasTest extends TestCase
{

    public function test_formulario_completo_crea_tarea(){
        $user = $this->administrador();

        $datos = [
            'nombre' => 'molde',
        ];

        $response = $this->actingAs($user)->post('/administracion/tareas/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tareas');
        $this->assertDatabaseHas('tareas', $datos);
    }

    public function test_no_se_crea_tarea_sin_nombre(){
        $user = $this->administrador();

        $datos = [];

        $response = $this->actingAs($user)->postJson('/administracion/tareas/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }
}
