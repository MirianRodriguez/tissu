<?php

namespace Tests\Feature\Tareas;

use Tests\TestCase;

class EdicionTareasTest extends TestCase
{

    public function test_formulario_completo_edita_tarea(){
        $user = $this->administrador();
        $tarea = $this->tarea();

        $datos = [
            'nombre' => 'costura',
        ];

        $response = $this->actingAs($user)->put("/administracion/tareas/editar/{$tarea->id}", $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tareas');
        $this->assertDatabaseHas('tareas', $datos);
    }

    public function test_no_se_edita_tarea_sin_nombre(){
        $user = $this->administrador();
        $tarea = $this->tarea();

        $datos = [];

        $response = $this->actingAs($user)->putJson("/administracion/tareas/editar/{$tarea->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }
}
