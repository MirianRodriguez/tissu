<?php

namespace Tests\Feature\Tareas;

use Tests\TestCase;
use App\ConfeccionTarea;
use App\TareaProducto;

class BorradoTareasTest extends TestCase
{
    public function test_administrador_puede_eliminar_tarea_sin_referencias(){
        $user = $this->administrador();
        $tarea = $this->tarea();

        $response = $this->actingAs($user)->delete("/administracion/tareas/eliminar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tareas');
        $this->assertDatabaseMissing('tareas', ['id' => $tarea->id]);
    }

    public function test_administrador_no_puede_eliminar_tareas_con_tarea_confeccion(){
        $user = $this->administrador();
        $tarea = $this->tarea();

        ConfeccionTarea::create([
            'confeccion_id'     => '1',
            'tarea_id'          => $tarea->id,
            'estado_tarea_id'   => '1',
        ]);

        $response = $this->actingAs($user)->delete("/administracion/tareas/eliminar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tareas');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('tareas', ['id' => $tarea->id]);
    } 

    public function test_administrador_no_puede_eliminar_tareas_con_tarea_producto(){
        $user = $this->administrador();
        $tarea = $this->tarea();

        TareaProducto::create([
            'predecesora_id'    => null,
            'dificultad_id'     => '1',
            'tarea_id'          => '1',
            'producto_id'       => '1',
        ]);

        $response = $this->actingAs($user)->delete("/administracion/tareas/eliminar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tareas');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('tareas', ['id' => $tarea->id]);
    } 
}
