<?php

namespace Test\Feature\Tareas;

use Tests\TestCase;

class PrivacidadTareasTest extends TestCase {

    public function test_guest_no_puede_ver_gestion_tareas(){
        $response = $this->get('/administracion/tareas');
        $response->assertRedirect('/login');

        $response = $this->get('/administracion/tareas/crear');
        $response->assertRedirect('/login');

        $response = $this->post('/administracion/tareas/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $tarea = $this->tarea();

        $response = $this->get("/administracion/tareas/editar/{$tarea->id}");
        $response->assertRedirect('/login');
        
        $response = $this->put("/administracion/tareas/editar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $response = $this->delete("/administracion/tareas/eliminar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertDatabaseHas('tareas',['id'=>$tarea->id]);
    }

    public function test_administrador_puede_ver_gestion_tareas(){
        $user = $this->administrador();

        $response = $this->actingAs($user)->get('/administracion/tareas');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.tareas.index');

        $response = $this->get('/administracion/tareas/crear');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.tareas.crear');

        $tarea = $this->tarea();

        $response = $this->get("/administracion/tareas/editar/{$tarea->id}");
        $response->assertSuccessful();
        $response->assertViewIs('administracion.tareas.editar');
    }

    public function test_confeccionista_no_puede_ver_gestion_tareas(){
        $user = $this->confeccionista();

        $response = $this->actingAs($user)->get('/administracion/tareas');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/tareas/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/tareas/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $tarea = $this->tarea();

        $response = $this->get("/administracion/tareas/editar/{$tarea->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/tareas/editar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/tareas/eliminar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('tareas',['id'=>$tarea->id]);
    }

    public function test_auditor_no_puede_ver_gestion_tareas(){
        $user = $this->auditor();

        $response = $this->actingAs($user)->get('/administracion/tareas');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/tareas/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/tareas/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $tarea = $this->tarea();

        $response = $this->get("/administracion/tareas/editar/{$tarea->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/tareas/editar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/tareas/eliminar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('tareas',['id'=>$tarea->id]);
    }

    public function test_registrador_no_puede_ver_gestion_tareas(){
        $user = $this->registrador();

        $response = $this->actingAs($user)->get('/administracion/tareas');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/tareas/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/tareas/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $tarea = $this->tarea();

        $response = $this->get("/administracion/tareas/editar/{$tarea->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/tareas/editar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/tareas/eliminar/{$tarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('tareas',['id'=>$tarea->id]);
    }

}