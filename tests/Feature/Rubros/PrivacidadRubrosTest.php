<?php

namespace Test\Feature\Rubros;

use Tests\TestCase;

class PrivacidadRubrosTest extends TestCase {

    public function test_guest_no_puede_ver_gestion_rubros(){
        $response = $this->get('/administracion/rubros');
        $response->assertRedirect('/login');

        $response = $this->get('/administracion/rubros/crear');
        $response->assertRedirect('/login');

        $response = $this->post('/administracion/rubros/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $rubro = $this->rubro();

        $response = $this->get("/administracion/rubros/editar/{$rubro->id}");
        $response->assertRedirect('/login');
        
        $response = $this->put("/administracion/rubros/editar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $response = $this->delete("/administracion/rubros/eliminar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertDatabaseHas('rubros',['id'=>$rubro->id]);
    }

    public function test_administrador_puede_ver_gestion_rubros(){
        $user = $this->administrador();

        $response = $this->actingAs($user)->get('/administracion/rubros');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.rubros.index');

        $response = $this->get('/administracion/rubros/crear');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.rubros.crear');

        $rubro = $this->rubro();

        $response = $this->get("/administracion/rubros/editar/{$rubro->id}");
        $response->assertSuccessful();
        $response->assertViewIs('administracion.rubros.editar');
    }

    public function test_confeccionista_no_puede_ver_gestion_rubros(){
        $user = $this->confeccionista();

        $response = $this->actingAs($user)->get('/administracion/rubros');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/rubros/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/rubros/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $rubro = $this->rubro();

        $response = $this->get("/administracion/rubros/editar/{$rubro->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/rubros/editar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/rubros/eliminar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('rubros',['id'=>$rubro->id]);
    }

    public function test_auditor_no_puede_ver_gestion_rubros(){
        $user = $this->auditor();

        $response = $this->actingAs($user)->get('/administracion/rubros');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/rubros/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/rubros/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $rubro = $this->rubro();

        $response = $this->get("/administracion/rubros/editar/{$rubro->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/rubros/editar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/rubros/eliminar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('rubros',['id'=>$rubro->id]);
    }

    public function test_registrador_no_puede_ver_gestion_rubros(){
        $user = $this->registrador();

        $response = $this->actingAs($user)->get('/administracion/rubros');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/rubros/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/rubros/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $rubro = $this->rubro();

        $response = $this->get("/administracion/rubros/editar/{$rubro->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/rubros/editar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/rubros/eliminar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('rubros',['id'=>$rubro->id]);
    }

}