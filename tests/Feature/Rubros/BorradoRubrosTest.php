<?php

namespace Tests\Feature\Rubros;

use Tests\TestCase;
use App\Material;

class BorradoRubrosTest extends TestCase
{
    public function test_administrador_puede_eliminar_rubro_sin_referencias(){
        $user = $this->administrador();
        $rubro = $this->rubro();

        $response = $this->actingAs($user)->delete("/administracion/rubros/eliminar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/administracion/rubros');
        $this->assertDatabaseMissing('rubros', ['id' => $rubro->id]);
    }

    public function test_administrador_no_puede_eliminar_rubros_con_referencias(){
        $user = $this->administrador();
        $rubro = $this->rubro();

        Material::create([
            'tipo_material_id' => 1,
            'rubro_id' => $rubro->id,
        ]);

        $response = $this->actingAs($user)->delete("/administracion/rubros/eliminar/{$rubro->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/rubros');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('rubros', ['id' => $rubro->id]);
    } 
}
