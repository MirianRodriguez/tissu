<?php

namespace Tests\Feature\Rubros;

use Tests\TestCase;

class CreacionRubrosTest extends TestCase
{

    public function test_formulario_completo_crea_rubro(){
        $user = $this->administrador();

        $datos = [
            'nombre' => 'telas',
        ];

        $response = $this->actingAs($user)->post('/administracion/rubros/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/rubros');
        $this->assertDatabaseHas('rubros', $datos);
    }

    public function test_no_se_crea_rubro_sin_nombre(){
        $user = $this->administrador();

        $datos = [];

        $response = $this->actingAs($user)->postJson('/administracion/rubros/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }
}
