<?php

namespace Tests\Feature\Rubros;

use Tests\TestCase;

class EdicionRubrosTest extends TestCase
{

    public function test_formulario_completo_edita_rubro(){
        $user = $this->administrador();
        $rubro = $this->rubro();

        $datos = [
            'nombre' => 'telas',
        ];

        $response = $this->actingAs($user)->put("/administracion/rubros/editar/{$rubro->id}", $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/rubros');
        $this->assertDatabaseHas('rubros', $datos);
    }

    public function test_no_se_edita_rubro_sin_nombre(){
        $user = $this->administrador();
        $rubro = $this->rubro();

        $datos = [];

        $response = $this->actingAs($user)->putJson("/administracion/rubros/editar/{$rubro->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }
}
