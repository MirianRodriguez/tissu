<?php

namespace Tests\Feature\DificultadesTareas;

use Tests\TestCase;

class EdicionDificultadesTareasTest extends TestCase
{

    public function test_formulario_completo_edita_dificultad_tarea(){
        $user = $this->administrador();
        $dificultadTarea = $this->dificultadTarea();

        $datos = [
            'nombre' => 'media',
            'tiempo_ejecucion' => 15,
        ];

        $response = $this->actingAs($user)->put("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}", $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/dificultades-tareas');
        $this->assertDatabaseHas('dificultades_tareas', $datos);
    }

    public function test_no_se_edita_dificultad_tarea_sin_nombre(){
        $user = $this->administrador();
        $dificultadTarea = $this->dificultadTarea();

        $datos = [
            'tiempo_ejecucion' => 15,
        ];

        $response = $this->actingAs($user)->putJson("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }

    public function test_no_se_edita_dificultad_tarea_sin_tiempo_ejecucion(){
        $user = $this->administrador();
        $dificultadTarea = $this->dificultadTarea();

        $datos = [
            'nombre' => 'media',
        ];

        $response = $this->actingAs($user)->putJson("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('tiempo_ejecucion');
    }
}
