<?php

namespace Test\Feature\DificultadesTareas;

use Tests\TestCase;

class PrivacidadDificultadesTareasTest extends TestCase {

    public function test_guest_no_puede_ver_gestion_dificultades_tareas(){
        $response = $this->get('/administracion/dificultades-tareas');
        $response->assertRedirect('/login');

        $response = $this->get('/administracion/dificultades-tareas/crear');
        $response->assertRedirect('/login');

        $response = $this->post('/administracion/dificultades-tareas/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $dificultadTarea = $this->dificultadTarea();

        $response = $this->get("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}");
        $response->assertRedirect('/login');
        
        $response = $this->put("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $response = $this->delete("/administracion/dificultades-tareas/eliminar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertDatabaseHas('dificultades_tareas',['id'=>$dificultadTarea->id]);
    }

    public function test_administrador_puede_ver_gestion_dificultades_tareas(){
        $user = $this->administrador();

        $response = $this->actingAs($user)->get('/administracion/dificultades-tareas');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.dificultades-tareas.index');

        $response = $this->get('/administracion/dificultades-tareas/crear');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.dificultades-tareas.crear');

        $dificultadTarea = $this->dificultadTarea();

        $response = $this->get("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}");
        $response->assertSuccessful();
        $response->assertViewIs('administracion.dificultades-tareas.editar');
    }

    public function test_confeccionista_no_puede_ver_gestion_dificultades_tareas(){
        $user = $this->confeccionista();

        $response = $this->actingAs($user)->get('/administracion/dificultades-tareas');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/dificultades-tareas/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/dificultades-tareas/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $dificultadTarea = $this->dificultadTarea();

        $response = $this->get("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/dificultades-tareas/eliminar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('dificultades_tareas',['id'=>$dificultadTarea->id]);
    }

    public function test_auditor_no_puede_ver_gestion_dificultades_tareas(){
        $user = $this->auditor();

        $response = $this->actingAs($user)->get('/administracion/dificultades-tareas');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/dificultades-tareas/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/dificultades-tareas/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $dificultadTarea = $this->dificultadTarea();

        $response = $this->get("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/dificultades-tareas/eliminar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('dificultades_tareas',['id'=>$dificultadTarea->id]);
    }

    public function test_registrador_no_puede_ver_gestion_dificultades_tareas(){
        $user = $this->registrador();

        $response = $this->actingAs($user)->get('/administracion/dificultades-tareas');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/dificultades-tareas/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/dificultades-tareas/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $dificultadTarea = $this->dificultadTarea();

        $response = $this->get("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/dificultades-tareas/editar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/dificultades-tareas/eliminar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('dificultades_tareas',['id'=>$dificultadTarea->id]);
    }

}