<?php

namespace Tests\Feature\DificultadesTareas;

use Tests\TestCase;

class CreacionDificultadesTareasTest extends TestCase
{

    public function test_formulario_completo_crea_dificultad_tarea(){
        $user = $this->administrador();

        $datos = [
            'nombre' => 'media',
            'tiempo_ejecucion' => 15,
        ];

        $response = $this->actingAs($user)->post('/administracion/dificultades-tareas/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/dificultades-tareas');
        $this->assertDatabaseHas('dificultades_tareas', $datos);
    }

    public function test_no_se_crea_dificultad_tarea_sin_nombre(){
        $user = $this->administrador();

        $datos = [
            'tiempo_ejecucion' => 15,
        ];

        $response = $this->actingAs($user)->postJson('/administracion/dificultades-tareas/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }

    public function test_no_se_crea_dificultad_tarea_sin_tiempo_ejecucion(){
        $user = $this->administrador();

        $datos = [
            'nombre' => 'media',
        ];

        $response = $this->actingAs($user)->postJson('/administracion/dificultades-tareas/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('tiempo_ejecucion');
    }
}
