<?php

namespace Tests\Feature\DificultadesTareas;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class BorradoDificultadesTareasTest extends TestCase
{
    public function test_administrador_puede_eliminar_dificultad_tarea_sin_referencias(){
        $user = $this->administrador();
        $dificultadTarea = $this->dificultadTarea();

        $response = $this->actingAs($user)->delete("/administracion/dificultades-tareas/eliminar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/administracion/dificultades-tareas');
        $this->assertDatabaseMissing('dificultades_tareas', ['id' => $dificultadTarea->id]);
    }

    public function test_administrador_no_puede_eliminar_dificultad_tarea_con_referencias(){
        $user = $this->administrador();
        $dificultadTarea = $this->dificultadTarea();

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => '1',
            'tarea_id'          => '1',
            'producto_id'       => '1',
        ]);

        $response = $this->actingAs($user)->delete("/administracion/dificultades-tareas/eliminar/{$dificultadTarea->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/dificultades-tareas');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('dificultades_tareas', ['id' => $dificultadTarea->id]);
    } 
}
