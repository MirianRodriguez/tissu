<?php

namespace Tests\Feature\Confecciones;

use Tests\TestCase;
use App\Pedido;
use App\Confeccion;
use App\ConfeccionTarea;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use App\Producto;
use App\DificultadTarea;

class InicioConfeccionesTest extends TestCase
{

    public function test_asignacion_tarea(){
        $pedido = Pedido::create([
            'presupuesto'           => '2500',
            'fecha_pactada'         => '2020/12/25',
            'fecha'                 => '2020/11/14',
            'porcentaje_avance'     => '0',
            'comentario'            => 'Mandar a sublimar imágen de San Cayetano',
            'cliente_id'            => '1',
        ]);

        $producto = Producto::create([
            'nombre'            => 'Camisa dama gabardina',
            'descripcion'       => 'Manga larga',
            'esta_activo'       => true,
            'tipo_producto_id'  => '5',
            'complejidad_id'    => '1',
        ]);

        $tm = $this->tipoMaterial();

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '70',
            'tipo_material_id'      => $tm->id,
            'producto_id'           => $producto->id,
        ]);

        $confeccion = Confeccion::create([
            'comentario'     => 'Tatiana',
            'cantidad'       => '1',
            'producto_id'    => $producto->id,
            'pedido_id'      => $pedido->id,
        ]);

        $confeccionTarea = ConfeccionTarea::create([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => $confeccion->id,
            'tarea_id'          => 1,
        ]);

        $material = $this->material();
        $material->update(['tipo_material_id' => $tm->id]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => $confeccion->id,
            'material_id'       => $material->id,
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '90',
            'confeccion_id'         => $confeccion->id,
            'medida_requerida_id'   => '1',
        ]);

        $dificultad = DificultadTarea::create([
            'nombre' => 'Baja',
            'tiempo_ejecucion' => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => $dificultad->id,
            'tarea_id'          => '1',
            'producto_id'       => $producto->id,
        ]);

        $confeccionista = $this->confeccionista();

        Artisan::call('tarea:planificacion');

        $this->assertDatabaseHas('asignaciones', ['confeccion_tarea_id' => $confeccionTarea->id, 'usuario_id'=>$confeccionista->id]);
        $this->assertDatabaseHas('confecciones_tareas',['id' => $confeccionTarea->id, 'estado_tarea_id'=>2]);
    }

    public function test_no_asigna_tarea_sin_medidas_en_confeccion(){
        $pedido = Pedido::create([
            'presupuesto'           => '2500',
            'fecha_pactada'         => '2020/12/25',
            'fecha'                 => '2020/11/14',
            'porcentaje_avance'     => '0',
            'comentario'            => 'Mandar a sublimar imágen de San Cayetano',
            'cliente_id'            => '1',
        ]);

        $producto = Producto::create([
            'nombre'            => 'Camisa dama gabardina',
            'descripcion'       => 'Manga larga',
            'esta_activo'       => true,
            'tipo_producto_id'  => '5',
            'complejidad_id'    => '1',
        ]);

        $tm = $this->tipoMaterial();

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '70',
            'tipo_material_id'      => $tm->id,
            'producto_id'           => $producto->id,
        ]);

        $confeccion = Confeccion::create([
            'comentario'     => 'Tatiana',
            'cantidad'       => '1',
            'producto_id'    => $producto->id,
            'pedido_id'      => $pedido->id,
        ]);

        $confeccionTarea = ConfeccionTarea::create([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => $confeccion->id,
            'tarea_id'          => 1,
        ]);

        $material = $this->material();
        $material->update(['tipo_material_id' => $tm->id]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => $confeccion->id,
            'material_id'       => $material->id,
        ]);

        $dificultad = DificultadTarea::create([
            'nombre' => 'Baja',
            'tiempo_ejecucion' => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => $dificultad->id,
            'tarea_id'          => '1',
            'producto_id'       => $producto->id,
        ]);

        $confeccionista = $this->confeccionista();

        Artisan::call('tarea:planificacion');

        $this->assertDatabaseMissing('asignaciones', ['confeccion_tarea_id' => $confeccionTarea->id, 'usuario_id'=>$confeccionista->id]);
        $this->assertDatabaseMissing('confecciones_tareas',['id' => $confeccionTarea->id, 'estado_tarea_id'=>2]);
    }

    public function test_no_asigna_tarea_sin_stock_materiales(){
        $pedido = Pedido::create([
            'presupuesto'           => '2500',
            'fecha_pactada'         => '2020/12/25',
            'fecha'                 => '2020/11/14',
            'porcentaje_avance'     => '0',
            'comentario'            => 'Mandar a sublimar imágen de San Cayetano',
            'cliente_id'            => '1',
        ]);

        $producto = Producto::create([
            'nombre'            => 'Camisa dama gabardina',
            'descripcion'       => 'Manga larga',
            'esta_activo'       => true,
            'tipo_producto_id'  => '5',
            'complejidad_id'    => '1',
        ]);

        $tm = $this->tipoMaterial();

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '70',
            'tipo_material_id'      => $tm->id,
            'producto_id'           => $producto->id,
        ]);

        $confeccion = Confeccion::create([
            'comentario'     => 'Tatiana',
            'cantidad'       => '1',
            'producto_id'    => $producto->id,
            'pedido_id'      => $pedido->id,
        ]);

        $confeccionTarea = ConfeccionTarea::create([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => $confeccion->id,
            'tarea_id'          => 1,
        ]);

        $material = $this->material();
        $material->update(['tipo_material_id' => $tm->id, 'stock' => 69]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => $confeccion->id,
            'material_id'       => $material->id,
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '90',
            'confeccion_id'         => $confeccion->id,
            'medida_requerida_id'   => '1',
        ]);

        $dificultad = DificultadTarea::create([
            'nombre' => 'Baja',
            'tiempo_ejecucion' => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => $dificultad->id,
            'tarea_id'          => '1',
            'producto_id'       => $producto->id,
        ]);

        $confeccionista = $this->confeccionista();
        $this->administrador();

        Artisan::call('tarea:planificacion');

        $this->assertDatabaseMissing('asignaciones', ['confeccion_tarea_id' => $confeccionTarea->id, 'usuario_id'=>$confeccionista->id]);
        $this->assertDatabaseMissing('confecciones_tareas',['id' => $confeccionTarea->id, 'estado_tarea_id'=>2]);
    }

    public function test_no_asigna_tarea_sin_materiales_definidos(){
        $pedido = Pedido::create([
            'presupuesto'           => '2500',
            'fecha_pactada'         => '2020/12/25',
            'fecha'                 => '2020/11/14',
            'porcentaje_avance'     => '0',
            'comentario'            => 'Mandar a sublimar imágen de San Cayetano',
            'cliente_id'            => '1',
        ]);

        $producto = Producto::create([
            'nombre'            => 'Camisa dama gabardina',
            'descripcion'       => 'Manga larga',
            'esta_activo'       => true,
            'tipo_producto_id'  => '5',
            'complejidad_id'    => '1',
        ]);

        $tm = $this->tipoMaterial();

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '70',
            'tipo_material_id'      => $tm->id,
            'producto_id'           => $producto->id,
        ]);

        $confeccion = Confeccion::create([
            'comentario'     => 'Tatiana',
            'cantidad'       => '1',
            'producto_id'    => $producto->id,
            'pedido_id'      => $pedido->id,
        ]);

        $confeccionTarea = ConfeccionTarea::create([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => $confeccion->id,
            'tarea_id'          => 1,
        ]);

        $material = $this->material();
        $material->update(['tipo_material_id' => $tm->id]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '90',
            'confeccion_id'         => $confeccion->id,
            'medida_requerida_id'   => '1',
        ]);

        $dificultad = DificultadTarea::create([
            'nombre' => 'Baja',
            'tiempo_ejecucion' => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => $dificultad->id,
            'tarea_id'          => '1',
            'producto_id'       => $producto->id,
        ]);

        $confeccionista = $this->confeccionista();

        Artisan::call('tarea:planificacion');

        $this->assertDatabaseMissing('asignaciones', ['confeccion_tarea_id' => $confeccionTarea->id, 'usuario_id'=>$confeccionista->id]);
        $this->assertDatabaseMissing('confecciones_tareas',['id' => $confeccionTarea->id, 'estado_tarea_id'=>2]);
    }

    public function test_no_asigna_tarea_con_pago_atrasado(){
        $pedido = Pedido::create([
            'presupuesto'           => '2500',
            'fecha_pactada'         => '2020/12/25',
            'fecha'                 => '2020/11/14',
            'porcentaje_avance'     => '0',
            'comentario'            => 'Mandar a sublimar imágen de San Cayetano',
            'cliente_id'            => '1',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => today('America/Argentina/Buenos_Aires'),
            'monto'     => '1500',
            'esta_pagado'=> false,
            'pedido_id' => $pedido->id,
        ]);

        $producto = Producto::create([
            'nombre'            => 'Camisa dama gabardina',
            'descripcion'       => 'Manga larga',
            'esta_activo'       => true,
            'tipo_producto_id'  => '5',
            'complejidad_id'    => '1',
        ]);

        $tm = $this->tipoMaterial();

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '70',
            'tipo_material_id'      => $tm->id,
            'producto_id'           => $producto->id,
        ]);

        $confeccion = Confeccion::create([
            'comentario'     => 'Tatiana',
            'cantidad'       => '1',
            'producto_id'    => $producto->id,
            'pedido_id'      => $pedido->id,
        ]);

        $confeccionTarea = ConfeccionTarea::create([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => $confeccion->id,
            'tarea_id'          => 1,
        ]);

        $material = $this->material();
        $material->update(['tipo_material_id' => $tm->id]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => $confeccion->id,
            'material_id'       => $material->id,
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '90',
            'confeccion_id'         => $confeccion->id,
            'medida_requerida_id'   => '1',
        ]);

        $dificultad = DificultadTarea::create([
            'nombre' => 'Baja',
            'tiempo_ejecucion' => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => $dificultad->id,
            'tarea_id'          => '1',
            'producto_id'       => $producto->id,
        ]);

        $confeccionista = $this->confeccionista();

        Artisan::call('tarea:planificacion');

        $this->assertDatabaseMissing('asignaciones', ['confeccion_tarea_id' => $confeccionTarea->id, 'usuario_id'=>$confeccionista->id]);
        $this->assertDatabaseMissing('confecciones_tareas',['id' => $confeccionTarea->id, 'estado_tarea_id'=>2]);
    }
}
