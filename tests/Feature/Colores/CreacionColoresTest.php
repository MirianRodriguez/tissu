<?php

namespace Tests\Feature\Colores;

use Tests\TestCase;

class CreacionColoresTest extends TestCase
{

    public function test_formulario_completo_crea_color(){
        $user = $this->administrador();

        $datos = [
            'nombre' => 'verde',
        ];

        $response = $this->actingAs($user)->post('/administracion/colores/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/colores');
        $this->assertDatabaseHas('colores', $datos);
    }

    public function test_no_se_crea_color_sin_nombre(){
        $user = $this->administrador();

        $datos = [];

        $response = $this->actingAs($user)->postJson('/administracion/colores/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }
}
