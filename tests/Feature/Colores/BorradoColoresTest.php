<?php

namespace Tests\Feature\Colores;

use Tests\TestCase;
use App\Material;

class BorradoColoresTest extends TestCase
{
    public function test_administrador_puede_eliminar_color_sin_referencias(){
        $user = $this->administrador();
        $color = $this->color();

        $response = $this->actingAs($user)->delete("/administracion/colores/eliminar/{$color->id}", [
            '_token' => csrf_token(),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/administracion/colores');
        $this->assertDatabaseMissing('colores', ['id' => $color->id]);
    }

    public function test_administrador_no_puede_eliminar_colores_con_referencias(){
        $user = $this->administrador();
        $color = $this->color();

        Material::create([
            'tipo_material_id' => 1,
            'color_id' => $color->id,
        ]);

        $response = $this->actingAs($user)->delete("/administracion/colores/eliminar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/colores');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('colores', ['id' => $color->id]);
    } 
}
