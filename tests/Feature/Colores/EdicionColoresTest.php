<?php

namespace Tests\Feature\Colores;

use Tests\TestCase;

class EdicionColoresTest extends TestCase
{

    public function test_formulario_completo_edita_color(){
        $user = $this->administrador();
        $color = $this->color();

        $datos = [
            'nombre' => 'verde',
        ];

        $response = $this->actingAs($user)->put("/administracion/colores/editar/{$color->id}", $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/colores');
        $this->assertDatabaseHas('colores', $datos);
    }

    public function test_no_se_edita_color_sin_nombre(){
        $user = $this->administrador();
        $color = $this->color();

        $datos = [];

        $response = $this->actingAs($user)->putJson("/administracion/colores/editar/{$color->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }
}
