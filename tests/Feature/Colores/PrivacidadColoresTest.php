<?php

namespace Test\Feature\Colores;

use Tests\TestCase;

class PrivacidadColoresTest extends TestCase {

    public function test_guest_no_puede_ver_gestion_colores(){
        $response = $this->get('/administracion/colores');
        $response->assertRedirect('/login');

        $response = $this->get('/administracion/colores/crear');
        $response->assertRedirect('/login');

        $response = $this->post('/administracion/colores/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $color = $this->color();

        $response = $this->get("/administracion/colores/editar/{$color->id}");
        $response->assertRedirect('/login');
        
        $response = $this->put("/administracion/colores/editar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $response = $this->delete("/administracion/colores/eliminar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertDatabaseHas('colores',['id'=>$color->id]);
    }

    public function test_administrador_puede_ver_gestion_colores(){
        $user = $this->administrador();

        $response = $this->actingAs($user)->get('/administracion/colores');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.colores.index');

        $response = $this->get('/administracion/colores/crear');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.colores.crear');

        $color = $this->color();

        $response = $this->get("/administracion/colores/editar/{$color->id}");
        $response->assertSuccessful();
        $response->assertViewIs('administracion.colores.editar');
    }

    public function test_confeccionista_no_puede_ver_gestion_colores(){
        $user = $this->confeccionista();

        $response = $this->actingAs($user)->get('/administracion/colores');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/colores/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/colores/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $color = $this->color();

        $response = $this->get("/administracion/colores/editar/{$color->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/colores/editar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/colores/eliminar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('colores',['id'=>$color->id]);
    }

    public function test_auditor_no_puede_ver_gestion_colores(){
        $user = $this->auditor();

        $response = $this->actingAs($user)->get('/administracion/colores');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/colores/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/colores/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $color = $this->color();

        $response = $this->get("/administracion/colores/editar/{$color->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/colores/editar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/colores/eliminar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('colores',['id'=>$color->id]);
    }

    public function test_registrador_no_puede_ver_gestion_colores(){
        $user = $this->registrador();

        $response = $this->actingAs($user)->get('/administracion/colores');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/colores/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/colores/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $color = $this->color();

        $response = $this->get("/administracion/colores/editar/{$color->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/colores/editar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/colores/eliminar/{$color->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('colores',['id'=>$color->id]);
    }

}