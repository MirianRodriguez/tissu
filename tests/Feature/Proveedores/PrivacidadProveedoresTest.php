<?php

namespace Test\Feature\Proveedores;

use Tests\TestCase;

class PrivacidadProveedoresTest extends TestCase {

    public function test_guest_no_puede_ver_gestion_proveedores(){
        $response = $this->get('/datos/proveedores');
        $response->assertRedirect('/login');

        $response = $this->get('/datos/proveedores/crear');
        $response->assertRedirect('/login');

        $response = $this->post('/datos/proveedores/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $proveedor = $this->proveedor();

        $response = $this->get("/datos/proveedores/editar/{$proveedor->id}");
        $response->assertRedirect('/login');
        
        $response = $this->put("/datos/proveedores/editar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $response = $this->delete("/datos/proveedores/eliminar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertDatabaseHas('proveedores',['id'=>$proveedor->id]);
    }

    public function test_registrador_puede_ver_gestion_proveedores(){
        $user = $this->registrador();

        $response = $this->actingAs($user)->get('/datos/proveedores');
        $response->assertSuccessful();
        $response->assertViewIs('registro.proveedores.index');

        $response = $this->get('/datos/proveedores/crear');
        $response->assertSuccessful();
        $response->assertViewIs('registro.proveedores.crear');

        $proveedor = $this->proveedor();

        $response = $this->get("/datos/proveedores/editar/{$proveedor->id}");
        $response->assertSuccessful();
        $response->assertViewIs('registro.proveedores.editar');
    }

    public function test_confeccionista_no_puede_ver_gestion_proveedores(){
        $user = $this->confeccionista();

        $response = $this->actingAs($user)->get('/datos/proveedores');
        $response->assertRedirect('/home');

        $response = $this->get('/datos/proveedores/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/datos/proveedores/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $proveedor = $this->proveedor();

        $response = $this->get("/datos/proveedores/editar/{$proveedor->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/datos/proveedores/editar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/datos/proveedores/eliminar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('proveedores',['id'=>$proveedor->id]);
    }

    public function test_auditor_no_puede_ver_gestion_proveedores(){
        $user = $this->auditor();

        $response = $this->actingAs($user)->get('/datos/proveedores');
        $response->assertRedirect('/home');

        $response = $this->get('/datos/proveedores/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/datos/proveedores/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $proveedor = $this->proveedor();

        $response = $this->get("/datos/proveedores/editar/{$proveedor->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/datos/proveedores/editar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/datos/proveedores/eliminar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('proveedores',['id'=>$proveedor->id]);
    }

    public function test_administrador_no_puede_ver_gestion_proveedores(){
        $user = $this->administrador();

        $response = $this->actingAs($user)->get('/datos/proveedores');
        $response->assertRedirect('/home');

        $response = $this->get('/datos/proveedores/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/datos/proveedores/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $proveedor = $this->proveedor();

        $response = $this->get("/datos/proveedores/editar/{$proveedor->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/datos/proveedores/editar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/datos/proveedores/eliminar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('proveedores',['id'=>$proveedor->id]);
    }

}