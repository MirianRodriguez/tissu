<?php

namespace Tests\Feature\Proveedores;

use Tests\TestCase;
use App\TipoMaterial;
use App\Compra;

class BorradoProveedoresTest extends TestCase
{
    public function test_registrador_puede_eliminar_proveedor_sin_referencias(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        $response = $this->actingAs($user)->delete("/datos/proveedores/eliminar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/datos/proveedores');
        $this->assertDatabaseMissing('proveedores', ['id' => $proveedor->id]);
    }

    public function test_registrador_no_puede_eliminar_proveedores_con_tipo_material(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        TipoMaterial::create([
            'nombre' => 'gabardina',
            'unidad_medida_por_bulto' => 'Mt.',
            'unidad_medida_en_stock' => 'Cm.',
            'proveedor_id' => $proveedor->id,
            'costo' => 500,
        ]);

        $response = $this->actingAs($user)->delete("/datos/proveedores/eliminar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/datos/proveedores');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('proveedores', ['id' => $proveedor->id]);
    } 

    
    public function test_registrador_no_puede_eliminar_proveedores_con_compra(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        Compra::create([
            'fecha' => '2020/12/31',
            'nro_comprobante' => '1111-12345678',
            'proveedor_id' => $proveedor->id,
            'total' => 500,
        ]);

        $response = $this->actingAs($user)->delete("/datos/proveedores/eliminar/{$proveedor->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/datos/proveedores');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('proveedores', ['id' => $proveedor->id]);
    }
}
