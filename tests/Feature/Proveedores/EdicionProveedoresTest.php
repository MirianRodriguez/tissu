<?php

namespace Tests\Feature\Proveedores;

use Tests\TestCase;

class EdicionProveedoresTest extends TestCase
{

    public function test_formulario_completo_edita_proveedor(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        $datos = [
            'nombre'    => 'Juan',
            'telefono'  => '3758489788',
            'email'     => 'proveedor2@gmail.com',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->put("/datos/proveedores/editar/{$proveedor->id}", $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/datos/proveedores');
        $this->assertDatabaseHas('proveedores', $datos);
    }

    public function test_formulario_datos_minimos_edita_proveedor(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        $datos = [
            'nombre'    => 'Juan',
            'telefono'  => '3758489788',
            'email'     => 'proveedor2@gmail.com',
        ];

        $response = $this->actingAs($user)->put("/datos/proveedores/editar/{$proveedor->id}", $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/datos/proveedores');
        $this->assertDatabaseHas('proveedores', $datos);
    }

    public function test_no_se_edita_proveedor_sin_nombre(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        $datos = [
            'telefono'  => '3758489788',
            'email'     => 'proveedor2@gmail.com',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->putJson("/datos/proveedores/editar/{$proveedor->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }

    public function test_no_se_edita_proveedor_sin_telefono(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        $datos = [
            'nombre'    => 'Juan',
            'email'     => 'proveedor2@gmail.com',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->putJson("/datos/proveedores/editar/{$proveedor->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('telefono');
    }

    public function test_no_se_edita_proveedor_sin_email(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        $datos = [
            'nombre'    => 'Juan',
            'telefono'  => '3758489788',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->putJson("/datos/proveedores/editar/{$proveedor->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }

    public function test_no_se_edita_proveedor_email_invalido(){
        $user = $this->registrador();
        $proveedor = $this->proveedor();

        $datos = [
            'nombre'    => 'Juan',
            'telefono'  => '3758489788',
            'email'     => 'proveedor2gmail',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->putJson("/datos/proveedores/editar/{$proveedor->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }
}
