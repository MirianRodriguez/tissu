<?php

namespace Tests\Feature\Proveedores;

use Tests\TestCase;

class CreacionProveedoresTest extends TestCase
{

    public function test_formulario_completo_crea_proveedor(){
        $user = $this->registrador();

        $datos = [
            'nombre'    => 'Juan',
            'telefono'  => '3758489788',
            'email'     => 'proveedor2@gmail.com',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->post('/datos/proveedores/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/datos/proveedores');
        $this->assertDatabaseHas('proveedores', $datos);
    }

    public function test_formulario_datos_minimos_crea_proveedor(){
        $user = $this->registrador();

        $datos = [
            'nombre'    => 'Juan',
            'telefono'  => '3758489788',
            'email'     => 'proveedor2@gmail.com',
        ];

        $response = $this->actingAs($user)->post('/datos/proveedores/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/datos/proveedores');
        $this->assertDatabaseHas('proveedores', $datos);
    }

    public function test_no_se_crea_proveedor_sin_nombre(){
        $user = $this->registrador();

        $datos = [
            'telefono'  => '3758489788',
            'email'     => 'proveedor2@gmail.com',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->postJson('/datos/proveedores/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }

    public function test_no_se_crea_proveedor_sin_telefono(){
        $user = $this->registrador();

        $datos = [
            'nombre'    => 'Juan',
            'email'     => 'proveedor2@gmail.com',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->postJson('/datos/proveedores/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('telefono');
    }

    public function test_no_se_crea_proveedor_sin_email(){
        $user = $this->registrador();

        $datos = [
            'nombre'    => 'Juan',
            'telefono'  => '3758489788',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->postJson('/datos/proveedores/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }

    public function test_no_se_crea_proveedor_email_invalido(){
        $user = $this->registrador();

        $datos = [
            'nombre'    => 'Juan',
            'telefono'  => '3758489788',
            'email'     => 'juangmail.com',
            'costo_envio' => 500,
            'demora' => 6,
        ];

        $response = $this->actingAs($user)->postJson('/datos/proveedores/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }
}
