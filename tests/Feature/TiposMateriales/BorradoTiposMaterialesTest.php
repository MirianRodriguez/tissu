<?php

namespace Tests\Feature\TiposMateriales;

use Tests\TestCase;
use App\Material;
use Illuminate\Support\Facades\DB;

class BorradoTiposMaterialesTest extends TestCase
{
    public function test_administrador_puede_eliminar_tipo_material_sin_referencias(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        $response = $this->actingAs($user)->delete("/administracion/tipos-de-materiales/eliminar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tipos-de-materiales');
        $this->assertDatabaseMissing('tipos_materiales', ['id' => $tipoMaterial->id]);
    }

    public function test_administrador_no_puede_eliminar_tipo_material_con_material(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        Material::create([
            'es_liso' => true,
            'tipo_material_id' => $tipoMaterial->id,
        ]);

        $response = $this->actingAs($user)->delete("/administracion/tipos-de-materiales/eliminar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tipos-de-materiales');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('tipos_materiales', ['id' => $tipoMaterial->id]);
    } 

    
    public function test_administrador_no_puede_eliminar_tipo_material_con_tipo_material_producto(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria' => 80,
            'tipo_material_id'   => $tipoMaterial->id,
            'producto_id'        => 1,
        ]);

        $response = $this->actingAs($user)->delete("/administracion/tipos-de-materiales/eliminar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tipos-de-materiales');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('tipos_materiales', ['id' => $tipoMaterial->id]);
    }
}
