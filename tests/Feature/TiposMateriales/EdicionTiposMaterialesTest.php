<?php

namespace Tests\Feature\TiposMateriales;

use Tests\TestCase;

class EdicionTiposMaterialesTest extends TestCase
{

    public function test_formulario_completo_edita_tipo_material(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        $datos = [
            'nombre'    => 'Gabardina elastizada',
            'unidad_medida_por_bulto'  => 'Mt.',
            'unidad_medida_en_stock'     => 'Cm.',
            'proveedor_id' => 2,
            'costo' => 600,
        ];

        $response = $this->actingAs($user)->put("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tipos-de-materiales');
        $this->assertDatabaseHas('tipos_materiales', $datos);
    }

    public function test_no_se_edita_tipo_material_sin_nombre(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        $datos = [
            'unidad_medida_por_bulto'  => 'Mt.',
            'unidad_medida_en_stock'     => 'Cm.',
            'proveedor_id' => 2,
            'costo' => 600,
        ];

        $response = $this->actingAs($user)->putJson("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }

    public function test_no_se_edita_tipo_material_sin_unidad_medida_por_bulto(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        $datos = [
            'nombre'    => 'Gabardina elastizada',
            'unidad_medida_en_stock'     => 'Cm.',
            'proveedor_id' => 2,
            'costo' => 600,
        ];

        $response = $this->actingAs($user)->putJson("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('unidad_medida_por_bulto');
    }

    public function test_no_se_edita_tipo_material_sin_unidad_medida_en_stock(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        $datos = [
            'nombre'    => 'Gabardina elastizada',
            'unidad_medida_por_bulto'  => 'Mt.',
            'proveedor_id' => 2,
            'costo' => 600,
        ];

        $response = $this->actingAs($user)->putJson("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('unidad_medida_en_stock');
    }

    public function test_no_se_edita_tipo_material_sin_proveedor(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        $datos = [
            'nombre'    => 'Gabardina elastizada',
            'unidad_medida_por_bulto'  => 'Mt.',
            'unidad_medida_en_stock'     => 'Cm.',
            'costo' => 600,
        ];

        $response = $this->actingAs($user)->putJson("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('proveedor_id');
    }

    public function test_no_se_edita_tipo_material_sin_costo(){
        $user = $this->administrador();
        $tipoMaterial = $this->tipoMaterial();

        $datos = [
            'nombre'    => 'Gabardina elastizada',
            'unidad_medida_por_bulto'  => 'Mt.',
            'unidad_medida_en_stock'     => 'Cm.',
            'proveedor_id' => 1,
        ];

        $response = $this->actingAs($user)->putJson("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('costo');
    }
}
