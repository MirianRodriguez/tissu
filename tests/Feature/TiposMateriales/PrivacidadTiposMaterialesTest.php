<?php

namespace Test\Feature\TiposMateriales;

use Tests\TestCase;
use App\Proveedor;

class PrivacidadTiposMaterialesTest extends TestCase {

    public function test_guest_no_puede_ver_gestion_tipos_materiales(){
        $response = $this->get('/administracion/tipos-de-materiales');
        $response->assertRedirect('/login');

        $response = $this->get('/administracion/tipos-de-materiales/crear');
        $response->assertRedirect('/login');

        $response = $this->post('/administracion/tipos-de-materiales/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $tipoMaterial = $this->tipoMaterial();

        $response = $this->get("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}");
        $response->assertRedirect('/login');
        
        $response = $this->put("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $response = $this->delete("/administracion/tipos-de-materiales/eliminar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertDatabaseHas('tipos_materiales',['id'=>$tipoMaterial->id]);
    }

    public function test_administrador_puede_ver_gestion_tipos_materiales(){
        $user = $this->administrador();

        $response = $this->actingAs($user)->get('/administracion/tipos-de-materiales');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.tipos-materiales.index');

        $response = $this->get('/administracion/tipos-de-materiales/crear');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.tipos-materiales.crear');

        $proveedor = Proveedor::create([
            'id'        => 1,
            'nombre'    => 'Pepe',
            'telefono'  => '3758489785',
            'email'     => 'proveedor@gmail.com',
            'costo_envio' => 300,
            'demora' => 6,
        ]);

        $tipoMaterial = $this->tipoMaterial();

        $response = $this->get("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}");
        $response->assertSuccessful();
        $response->assertViewIs('administracion.tipos-materiales.editar');
    }

    public function test_confeccionista_no_puede_ver_gestion_tipos_materiales(){
        $user = $this->confeccionista();

        $response = $this->actingAs($user)->get('/administracion/tipos-de-materiales');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/tipos-de-materiales/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/tipos-de-materiales/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $tipoMaterial = $this->tipoMaterial();

        $response = $this->get("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/tipos-de-materiales/eliminar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('tipos_materiales',['id'=>$tipoMaterial->id]);
    }

    public function test_auditor_no_puede_ver_gestion_tipos_materiales(){
        $user = $this->auditor();

        $response = $this->actingAs($user)->get('/administracion/tipos-de-materiales');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/tipos-de-materiales/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/tipos-de-materiales/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $tipoMaterial = $this->tipoMaterial();

        $response = $this->get("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/tipos-de-materiales/eliminar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('tipos_materiales',['id'=>$tipoMaterial->id]);
    }

    public function test_registrador_no_puede_ver_gestion_tipos_materiales(){
        $user = $this->registrador();

        $response = $this->actingAs($user)->get('/administracion/tipos-de-materiales');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/tipos-de-materiales/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/tipos-de-materiales/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $tipoMaterial = $this->tipoMaterial();

        $response = $this->get("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/tipos-de-materiales/editar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/tipos-de-materiales/eliminar/{$tipoMaterial->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('tipos_materiales',['id'=>$tipoMaterial->id]);
    }

}