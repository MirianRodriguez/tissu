<?php

namespace Tests\Feature\TiposMateriales;

use Tests\TestCase;
use App\Parametro;

class CreacionTiposMaterialesTest extends TestCase
{

    public function test_formulario_completo_crea_tipo_material(){
        $user = $this->administrador();

        Parametro::create([
            'clave'     => 'valor_dolar',
            'valor'     => '79.43',
            'updated_at'=> '2020-12-13',
        ]);

        $datos = [
            'nombre'                    => 'Pique',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => 1,
            'costo'                     => 700,
        ];

        $response = $this->actingAs($user)->post('/administracion/tipos-de-materiales/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/tipos-de-materiales');
        $this->assertDatabaseHas('tipos_materiales', [
            'nombre'                    => 'Pique',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => 1,
            'costo'                     => 8.8127911368501,
        ]);
    }

    public function test_no_se_crea_tipo_material_sin_nombre(){
        $user = $this->administrador();

        $datos = [
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => 1,
            'costo'                     => 700,
        ];


        $response = $this->actingAs($user)->postJson('/administracion/tipos-de-materiales/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('nombre');
    }

    public function test_no_se_crea_tipo_material_sin_unidad_medida_por_bulto(){
        $user = $this->administrador();

        $datos = [
            'nombre'                    => 'Pique',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => 1,
            'costo'                     => 700,
        ];

        $response = $this->actingAs($user)->postJson('/administracion/tipos-de-materiales/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('unidad_medida_por_bulto');
    }

    public function test_no_se_crea_tipo_material_sin_unidad_medida_en_stock(){
        $user = $this->administrador();

        $datos = [
            'nombre'                    => 'Pique',
            'unidad_medida_por_bulto'   => 'Mt.',
            'proveedor_id'              => 1,
            'costo'                     => 700,
        ];

        $response = $this->actingAs($user)->postJson('/administracion/tipos-de-materiales/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('unidad_medida_en_stock');
    }

    public function test_no_se_crea_tipo_material_sin_proveedor(){
        $user = $this->administrador();

        $datos = [
            'nombre'                    => 'Pique',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'costo'                     => 700,
        ];

        $response = $this->actingAs($user)->postJson('/administracion/tipos-de-materiales/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('proveedor_id');
    }

    
    public function test_no_se_crea_tipo_material_sin_costo(){
        $user = $this->administrador();

        $datos = [
            'nombre'                    => 'Pique',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => 1,
        ];

        $response = $this->actingAs($user)->postJson('/administracion/tipos-de-materiales/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('costo');
    }
}
