<?php

namespace Tests\Feature\AjustesStock;

use Tests\TestCase;
use App\Material;

class CreacionAjustesStockTest extends TestCase
{

    public function test_formulario_completo_crea_ajuste_stock(){
        $user = $this->administrador();

        $material = $this->material();

        $datos = [
            'cantidad'      => 10,
            'material_id'   => $material->id,
        ];

        $response = $this->actingAs($user)->post('/administracion/ajustes/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/ajustes');
        $this->assertDatabaseHas('ajustes_stock', ['cantidad' => -500, 'material_id' => $material->id]);
        $this->assertDatabaseHas('materiales', ['id' => $material->id, 'stock' => 1000]);
    }

    public function test_no_se_crea_ajuste_stock_sin_cantidad(){
        $user = $this->administrador();

        $material = $this->material();

        $datos = [
            'material_id'   => $material->id,
        ];

        $response = $this->actingAs($user)->postJson('/administracion/ajustes/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('cantidad');
        $this->assertDatabaseHas('materiales', ['id' => $material->id, 'stock' => 1500]);
    }

    public function test_no_se_crea_ajuste_stock_sin_material(){
        $user = $this->administrador();

        $datos = [
            'cantidad'      => -500,
        ];

        $response = $this->actingAs($user)->postJson('/administracion/ajustes/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('material_id');
    }
}
