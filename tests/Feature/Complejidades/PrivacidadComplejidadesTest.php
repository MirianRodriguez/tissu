<?php

namespace Test\Feature\Complejidades;

use Tests\TestCase;

class PrivacidadComplejidadesTest extends TestCase {

    public function test_guest_no_puede_ver_gestion_complejidades(){
        $response = $this->get('/administracion/complejidades');
        $response->assertRedirect('/login');

        $response = $this->get('/administracion/complejidades/crear');
        $response->assertRedirect('/login');

        $response = $this->post('/administracion/complejidades/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $complejidad = $this->complejidad();

        $response = $this->get("/administracion/complejidades/editar/{$complejidad->id}");
        $response->assertRedirect('/login');
        
        $response = $this->put("/administracion/complejidades/editar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $response = $this->delete("/administracion/complejidades/eliminar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertDatabaseHas('complejidades',['id'=>$complejidad->id]);
    }

    public function test_administrador_puede_ver_gestion_complejidades(){
        $user = $this->administrador();

        $response = $this->actingAs($user)->get('/administracion/complejidades');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.complejidades.index');

        $response = $this->get('/administracion/complejidades/crear');
        $response->assertSuccessful();
        $response->assertViewIs('administracion.complejidades.crear');

        $complejidad = $this->complejidad();

        $response = $this->get("/administracion/complejidades/editar/{$complejidad->id}");
        $response->assertSuccessful();
        $response->assertViewIs('administracion.complejidades.editar');
    }

    public function test_confeccionista_no_puede_ver_gestion_complejidades(){
        $user = $this->confeccionista();

        $response = $this->actingAs($user)->get('/administracion/complejidades');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/complejidades/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/complejidades/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $complejidad = $this->complejidad();

        $response = $this->get("/administracion/complejidades/editar/{$complejidad->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/complejidades/editar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/complejidades/eliminar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('complejidades',['id'=>$complejidad->id]);
    }

    public function test_auditor_no_puede_ver_gestion_complejidades(){
        $user = $this->auditor();

        $response = $this->actingAs($user)->get('/administracion/complejidades');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/complejidades/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/complejidades/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $complejidad = $this->complejidad();

        $response = $this->get("/administracion/complejidades/editar/{$complejidad->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/complejidades/editar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/complejidades/eliminar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('complejidades',['id'=>$complejidad->id]);
    }

    public function test_registrador_no_puede_ver_gestion_complejidades(){
        $user = $this->registrador();

        $response = $this->actingAs($user)->get('/administracion/complejidades');
        $response->assertRedirect('/home');

        $response = $this->get('/administracion/complejidades/crear');
        $response->assertRedirect('/home');

        $response = $this->post('/administracion/complejidades/crear', [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $complejidad = $this->complejidad();

        $response = $this->get("/administracion/complejidades/editar/{$complejidad->id}");
        $response->assertRedirect('/home');
        
        $response = $this->put("/administracion/complejidades/editar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        $response = $this->delete("/administracion/complejidades/eliminar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
        $this->assertDatabaseHas('complejidades',['id'=>$complejidad->id]);
    }

}