<?php

namespace Tests\Feature\Complejidades;

use Tests\TestCase;

class CreacionComplejidadesTest extends TestCase
{

    public function test_formulario_completo_crea_complejidad(){
        $user = $this->administrador();

        $datos = [
            'tipo' => 'media',
            'porcentaje_ganancia' => 150,
        ];

        $response = $this->actingAs($user)->post('/administracion/complejidades/crear', $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/complejidades');
        $this->assertDatabaseHas('complejidades', $datos);
    }

    public function test_no_se_crea_complejidad_sin_tipo(){
        $user = $this->administrador();

        $datos = [
            'porcentaje_ganancia' => 150,
        ];

        $response = $this->actingAs($user)->postJson('/administracion/complejidades/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('tipo');
    }

    public function test_no_se_crea_complejidad_sin_porcentaje_ganancia(){
        $user = $this->administrador();

        $datos = [
            'tipo' => 'Media',
        ];

        $response = $this->actingAs($user)->postJson('/administracion/complejidades/crear', $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('porcentaje_ganancia');
    }
}
