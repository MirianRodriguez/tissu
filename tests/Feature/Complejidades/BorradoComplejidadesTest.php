<?php

namespace Tests\Feature\Complejidades;

use Tests\TestCase;
use App\Producto;

class BorradoComplejidadesTest extends TestCase
{
    public function test_administrador_puede_eliminar_complejidad_sin_referencias(){
        $user = $this->administrador();
        $complejidad = $this->complejidad();

        $response = $this->actingAs($user)->delete("/administracion/complejidades/eliminar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/administracion/complejidades');
        $this->assertDatabaseMissing('complejidades', ['id' => $complejidad->id]);
    }

    public function test_administrador_no_puede_eliminar_complejidades_con_referencias(){
        $user = $this->administrador();
        $complejidad = $this->complejidad();

        Producto::create([
            'nombre' => 'pantalon',
            'esta_activo' => true,
            'tipo_producto_id' => 1,
            'complejidad_id' => $complejidad->id,
        ]);

        $response = $this->actingAs($user)->delete("/administracion/complejidades/eliminar/{$complejidad->id}", [
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/complejidades');
        $response->assertSessionHasErrors();
        $this->assertDatabaseHas('complejidades', ['id' => $complejidad->id]);
    } 
}
