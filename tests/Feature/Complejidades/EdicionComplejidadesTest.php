<?php

namespace Tests\Feature\Complejidades;

use Tests\TestCase;

class EdicionComplejidadesTest extends TestCase
{

    public function test_formulario_completo_edita_complejidad(){
        $user = $this->administrador();
        $complejidad = $this->complejidad();

        $datos = [
            'tipo' => 'media',
            'porcentaje_ganancia' => 150,
        ];

        $response = $this->actingAs($user)->put("/administracion/complejidades/editar/{$complejidad->id}", $datos);
        $response->assertStatus(302);
        $response->assertRedirect('/administracion/complejidades');
        $this->assertDatabaseHas('complejidades', $datos);
    }

    public function test_no_se_edita_complejidad_sin_tipo(){
        $user = $this->administrador();
        $complejidad = $this->complejidad();

        $datos = [
            'porcentaje_ganancia' => 150,
        ];

        $response = $this->actingAs($user)->putJson("/administracion/complejidades/editar/{$complejidad->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('tipo');
    }

    public function test_no_se_edita_complejidad_sin_porcentaje_ganancia(){
        $user = $this->administrador();
        $complejidad = $this->complejidad();

        $datos = [
            'tipo' => 'media',
        ];

        $response = $this->actingAs($user)->putJson("/administracion/complejidades/editar/{$complejidad->id}", $datos);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('porcentaje_ganancia');
    }
}
