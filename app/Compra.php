<?php

namespace App;

use App\Proveedor;

use App\DetalleCompra;

class Compra extends Auditable
{
    public $timestamps = false;

    protected $fillable = ['fecha', 'nro_comprobante', 'proveedor_id','total',];

    /* RELACIONES */

    public function detalles_compra(){
        return $this->hasMany('App\DetalleCompra');
    }

    public function proveedor(){
        return $this->belongsTo('App\Proveedor');
    }

    /* FORMATOS */

    public function fecha(){
        return date("d/m/Y", strtotime($this->fecha));
    }

    public function total(){
        return number_format($this->total, 2, ',', '.');
    }

}
