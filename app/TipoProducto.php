<?php

namespace App;


class TipoProducto extends Auditable
{
    public $timestamps = false;

    protected $table = 'tipos_productos'; 

    protected $fillable = ['nombre','descripcion',];

    public function medidas_requeridas(){
        return $this->belongsToMany('App\MedidaRequerida', 'medida_requerida_tipo_producto', 'tipo_producto_id', 'medida_requerida_id');
    }
}
