<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UserAuditable extends Authenticatable
{
    public static function boot()
    {
        static::created(function ($model) {

            $ahora = now()->format('Y-m-d H:i:s');

            foreach ($model->attributes as $key => $value)
            {
                if ($key != 'id' && $key != 'remember_token' && ! is_null($value))
                    Creado::create([
                        'tabla'     => $model->table,
                        'id_creado' => $model->id,
                        'clave'     => $key,
                        'valor'     => $value,
                        'fecha'     => $ahora,
                        'user_id'   => auth()->id(),
                    ]);
            }
        });


        static::updating(function ($model) {

            $ahora = now()->format('Y-m-d H:i:s');

            foreach ($model->attributes as $key => $value)
            {
                if ($key != 'remember_token')
                {
                    if (! array_key_exists($key, $model->original))
                    {
                        Modificado::create([
                            'tabla'          => $model->table,
                            'id_modificado'  => $model->id,
                            'clave'          => $key,
                            'valor_anterior' => null,
                            'valor_nuevo'    => $value,
                            'fecha'          => $ahora,
                            'user_id'        => auth()->id(),
                        ]);
                    }
                    elseif ($value != $model->original[$key])
                    {
                        Modificado::create([
                            'tabla'          => $model->table,
                            'id_modificado'  => $model->id,
                            'clave'          => $key,
                            'valor_anterior' => $model->original[$key],
                            'valor_nuevo'    => $value,
                            'fecha'          => $ahora,
                            'user_id'        => auth()->id(),
                        ]);
                    }
                }
            }
        });


        static::deleting(function ($model) {

            $ahora = now()->format('Y-m-d H:i:s');

            foreach ($model->original as $key => $value)
            {
                if ($key != 'id' && ! is_null($value))
                    Borrado::create([
                        'tabla'      => $model->table,
                        'id_borrado' => $model->id,
                        'clave'      => $key,
                        'valor'      => $value,
                        'fecha'      => $ahora,
                        'user_id'    => auth()->id(),
                    ]);
            }
        });


       parent::boot();
   }
}
