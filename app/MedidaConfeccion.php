<?php

namespace App;

class MedidaConfeccion extends Auditable
{
    public $timestamps = false;

    protected $table = 'medidas_confeccion'; 

    protected $fillable = ['valor','confeccion_id','medida_requerida_id'];

    /* Relaciones */
    public function confeccion(){
        return $this->belongsTo('App\Confeccion');
    }

    public function medida_requerida(){
        return $this->belongsTo('App\MedidaRequerida');
    }
}
