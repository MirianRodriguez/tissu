<?php

namespace App;

class Pedido extends Auditable
{
    public $timestamps = false;

    protected $fillable = ['presupuesto', 'fecha_pactada','fecha','porcentaje_avance', 'cliente_id','comentario','entregado'];

    /* RELACIONES */
    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }

    public function confecciones(){
        return $this->hasMany('App\Confeccion');
    }

    public function pagos_requeridos(){
        return $this->hasMany('App\PagoRequerido');
    }

    /* FORMATOS */
    public function fecha_pactada(){
        if (!is_null($this->fecha_pactada)){
            return date("d/m/Y", strtotime($this->fecha_pactada));
        }else{
            return "-";
        }
    }

    public function presupuesto(){
        return number_format($this->presupuesto, 2, ',', '.');
    }

    public function porcentaje_avance(){
        return number_format($this->porcentaje_avance, 0, ',', '.');
    }

    public function esta_cancelado(){
        $deuda = PagoRequerido::wherePedido_id($this->id)->whereEsta_pagado(false)->count();
        return $deuda == 0;
    }

}
