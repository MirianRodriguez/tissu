<?php

namespace App;

class UnidadMedida extends Auditable
{
    public $timestamps = false;

    protected $table = 'unidades_medida'; 

    protected $fillable = ['nombre',];
}
