<?php

namespace App;

class AjusteStock extends Auditable
{
    public $timestamps = false;

    protected $table = 'ajustes_stock'; 

    protected $fillable = ['cantidad', 'material_id'];

    public function material(){
        return $this->belongsTo('App\Material');
    }
}
