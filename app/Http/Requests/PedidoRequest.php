<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PedidoRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 4;
    }

    public function rules()
    {
        return [
            'cliente_id'        =>'required',
            'comentario'        =>'max:250',
            'fecha_pactada'     =>'required|date',
            'presupuesto'       =>'required|numeric',
            'cantidades'        =>'required',
        ];
    }

    public function messages(){
        return [
            'required'      => 'Campo obligatorio',
            'max'           => 'Longitud excedida',
            'date'          => 'Ingrese una fecha',
            'fecha_pactada.required' => 'Debe cargar una fecha de entrega.',
            'presupuesto.required'   => 'Debe cargar un presupuesto.'
        ];
    }
}
