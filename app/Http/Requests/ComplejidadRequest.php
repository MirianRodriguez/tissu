<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComplejidadRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 1;
    }

    public function rules()
    {
        return [
            'tipo'                  => 'required|max:50',
            'porcentaje_ganancia'   => 'required|integer|min:0|max:500',
        ];
    }

    public function messages(){
        return [
            'required'                  => 'Campo obligatorio',
            'tipo.max'                  => 'Longitud excedida',
            'integer'                   => 'Campo entero',
            'porcentaje_ganancia.min'   => 'Ingrese valor mayor a 0',
            'porcentaje_ganancia.max'   => 'Ingrese valor menor a 500',
        ];
    }
}
