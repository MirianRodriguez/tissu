<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DificultadTareaRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 1;
    }

    public function rules()
    {
        return [
            'nombre'             => 'required|max:50',
            'tiempo_ejecucion'   => 'required|numeric|min:0|max:100',
        ];
    }

    public function messages(){
        return [
            'required'                  => 'Campo obligatorio',
            'nombre.max'                => 'Longitud excedida',
            'numeric'                    => 'Campo numérico',
            'porcentaje_ganancia.min'   => 'Ingrese valor mayor a 0',
            'porcentaje_ganancia.max'   => 'Ingrese valor menor a 500',
        ];
    }
}
