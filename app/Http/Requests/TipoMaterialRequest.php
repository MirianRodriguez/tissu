<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoMaterialRequest extends FormRequest
{
    
    public function authorize()
    {
        return auth()->user()->rol_id == 1;
    }

    public function rules()
    {
        return [
            'nombre'                    => 'required|max:50',
            'unidad_medida_por_bulto'   => 'required|max:20',
            'unidad_medida_en_stock'    => 'required|max:20',
            'proveedor_id'              => 'required',
            'costo'                     => 'required|max:999999999|numeric',
        ];
    }

    public function messages(){
        return [
            'required'  => 'Campo obligatorio',
            'max'       => 'Longitud excedida',
        ];
    }
}
