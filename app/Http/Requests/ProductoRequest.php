<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 4;
    }

    public function rules()
    {
        return [
            'nombre'            =>'required|max:50',
            'descripcion'       =>'max:250',
            'tipo_producto_id'  =>'required|integer',
            'complejidad_id'    =>'required|integer',
            'cantidades'        =>'required',
        ];
    }

    public function messages(){
        return [
            'required'      => 'Campo obligatorio',
            'max'           => 'Longitud excedida',
            'integer'       => 'Campo numérico',
        ];
    }
}
