<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProveedorRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 4;
    }

    public function rules()
    {
        return [
            'nombre'     => 'required|max:50',
            'telefono'   => 'required|digits: 10',
            'email'      => 'required|email',
            'costo_envio'=> 'numeric|nullable',
            'demora'     => 'numeric|nullable',

        ];
    }

    public function messages(){
        return [
            'required'      => 'Campo obligatorio',
            'max'           => 'Longitud excedida',
            'digits'        => 'Formato no válido',
            'email'         => 'Formato no válido',
            'numeric'       => 'Campo numérico',
        ];
    }

}
