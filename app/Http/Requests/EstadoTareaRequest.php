<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EstadoTareaRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 1;
    }

    public function rules()
    {
        return [
            'nombre'             => 'required|max:50',
        ];
    }

    public function messages(){
        return [
            'required'           => 'Campo obligatorio',
            'max'                => 'Longitud excedida',
        ];
    }
}
