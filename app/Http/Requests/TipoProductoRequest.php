<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoProductoRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 1;
    }

    public function rules()
    {
        return [
            'nombre'        => 'required|max:50',
            'descripcion'   => 'max: 250',
            'medidas'       => 'required|array',
        ];
    }

    public function messages(){
        return [
            'required'  => 'Campo obligatorio',
            'max'       => 'Longitud excedida',
            'array'     => 'Dato no válido',
            'medidas.required'  => 'Debe cargar al menos una medida',
        ];
    }
}
