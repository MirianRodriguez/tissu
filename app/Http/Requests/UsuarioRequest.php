<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->rol_id == 1;
    }

    public function rules()
    {
        return [
            'name'     => 'required|max:50',
            'email'    => 'required|max:50',
            'password' => 'required|max:50|min:8',
            'rol_id'   => 'required',
        ];
    }

    public function messages(){
        return [
            'required'      => 'Campo obligatorio',
            'max'           => 'Longitud excedida',
            'min'           => 'Debe contener al menos 8 caracteres',
        ];
    }
}
