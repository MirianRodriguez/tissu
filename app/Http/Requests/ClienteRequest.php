<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 4;
    }

    public function rules()
    {
        return [
            'apellidos'  => 'required|max:50',
            'nombres'    => 'required|max:50',
            'telefono'   => 'required|digits: 10',
            'email'      => 'required|email',
            'saldo'      => 'numeric|nullable',
        ];
    }

    public function messages(){
        return [
            'required'      => 'Campo obligatorio',
            'max'           => 'Longitud excedida',
            'digits'        => 'Formato no válido',
            'email'         => 'Formato no válido',
            'numeric'       => 'Campo numérico',
        ];
    }
}
