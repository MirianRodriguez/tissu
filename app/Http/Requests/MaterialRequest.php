<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaterialRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 4;
    }

    public function rules()
    {
        return [
            'codigo'            => 'required|max:10|unique:materiales,codigo',
            'tipo_material_id'  => 'required',
            'color_id'          => 'required',
            'stock'             => 'max:999999999|numeric|nullable',
            'descripcion'       => 'max:300|nullable',
        ];
    }

    public function messages(){
        return [
            'required'      => 'Campo obligatorio',
            'max'           => 'Longitud excedida',
            'numeric'       => 'Campo numérico',
            'boolean'       => 'Campo booleano',
            'unique'        => 'Código ya ingresado',
        ];
    }
}
