<?php

namespace App\Http\Middleware;

use Closure;

class Registrador
{

    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->rol_id == 4)
            return $next($request);

        return redirect()->route('home');
    }
}
