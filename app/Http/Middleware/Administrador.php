<?php

namespace App\Http\Middleware;

use Closure;

class Administrador
{
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->rol_id == 1)
            return $next($request);

        return redirect()->route('home');
    }
}
