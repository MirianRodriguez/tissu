<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Complejidad;
use App\Http\Requests\ComplejidadRequest;
use App\Producto;

class ComplejidadController extends Controller
{
    public function index(){
        $complejidades = Complejidad::all(); 
        return view('administracion.complejidades.index', compact('complejidades'));
    }

    public function crear(){
        return view('administracion.complejidades.crear');
    }

    public function almacenar(ComplejidadRequest $request){
        Complejidad::updateOrCreate(['tipo'=>$request->tipo],$request->all());
        return redirect()->route('complejidades')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Complejidad $complejidad){
        return view('administracion.complejidades.editar', compact('complejidad'));
    }

    public function actualizar(ComplejidadRequest $request, Complejidad $complejidad){
        $complejidad->update($request->all());
        return redirect()->route('complejidades')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(Complejidad $complejidad){
        $referencias = Producto::whereComplejidad_id($complejidad->id)->count(); 
        if($referencias>0){
            return redirect()->route('complejidades')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $complejidad->delete();
        return redirect()->route('complejidades')->with('message', 'Registro eliminado exitosamente.');
    }
}
