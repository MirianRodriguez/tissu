<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Http\Requests\ClienteRequest;

class ClienteController extends Controller
{
    public function index(){
        $clientes = Cliente::orderBy('apellidos','asc')->orderBy('nombres','asc')->get();
        return view('registro.clientes.index', compact('clientes'));
    }

    public function crear(){
        return view('registro.clientes.crear');
    }

    public function almacenar(ClienteRequest $request){
        Cliente::updateOrCreate(['apellidos'=>$request->telefono],$request->all());
        return redirect()->route('clientes')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Cliente $cliente){
        return view('registro.clientes.editar', compact('cliente'));
    }

    public function actualizar(ClienteRequest $request, Cliente $cliente){
        $cliente->update($request->all());
        return redirect()->route('clientes')->with('message', 'Registro modificado exitosamente.');
    }

}
