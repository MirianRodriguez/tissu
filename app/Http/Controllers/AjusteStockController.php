<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AjusteStock;
use App\Material;

class AjusteStockController extends Controller
{
    public function index(){
        $ajustes = AjusteStock::orderBy('id','desc')->get();
        return view('administracion.ajustes-stock.index', compact('ajustes'));
    }

    public function crear(){
        $materiales = Material::with(['tipo_material','color'])->get();
        return view('administracion.ajustes-stock.crear', compact('materiales'));
    }

    public function almacenar(Request $request){
        //validaciones
        $request->validate([
            'cantidad'    => 'required|numeric',
            'material_id' => 'required',
        ],[
            'required'  => 'Campo obligatorio',
            'numeric' => 'Campo numérico',
        ]);
        //almacenamiento
        $material = Material::find($request->material_id);
        
        $ajuste = AjusteStock::create([
            'material_id' => $request->material_id,
            'cantidad'    => $material->tipo_material->convertir($request->cantidad, $material->tipo_material->unidad_medida_en_stock) -  $material->stock,
        ]);
        //actualizacion
        $material->update([
            'stock' => $material->stock + $ajuste->cantidad,
        ]);
        
        return redirect()->route('ajustes')->with('message', 'Registro almacenado exitosamente.');
    }
}
