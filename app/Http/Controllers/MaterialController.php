<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\MaterialRequest;
use App\Material;
use App\Rubro;
use App\Color;
use App\TipoMaterial;
use App\DetalleCompra;
use App\Mail\AvisoPedidoProveedor;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\AjusteStock;
use Illuminate\Support\Facades\DB;

class MaterialController extends Controller
{
    public function index(){
        $materiales = Material::with('rubro','color','tipo_material')->orderBy('id','desc')->get(); /*Recupera todos los datos de la bd*/ 
        return view('registro.materiales.index', compact('materiales'));/* llama a la vista y le envia la variable que contiene los datos */
    }

    /* muestra el formulario */
    public function crear(){
        $rubros = Rubro::all();
        $colores = Color::all();
        $tiposMateriales= DB::table('tipos_materiales')
        ->join('materiales', 'tipos_materiales.id', '=', 'materiales.tipo_material_id')
        ->select('tipos_materiales.id','nombre')
        ->groupBy('nombre','tipos_materiales.id')
        ->orderBy('rubro_id','asc')
        ->orderBy('nombre','asc')
        ->get();
        return view('registro.materiales.crear',compact('rubros','colores','tiposMateriales'));
    }

    /* crea el objeto nuevo y lo guarda en la BD. Vuelve a la vista index */
    public function almacenar(MaterialRequest $request){
        $m = Material::create($request->all());
        $m->update(['stock' => $m->tipo_material->convertir($m->stock,$m->tipo_material->unidad_medida_en_stock)]);
        return redirect()->route('materiales')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Material $material){
        $rubros = Rubro::all();
        $colores = Color::all();
        $tiposMateriales= DB::table('tipos_materiales')
        ->join('materiales', 'tipos_materiales.id', '=', 'materiales.tipo_material_id')
        ->select('tipos_materiales.id','nombre')
        ->groupBy('nombre','tipos_materiales.id')
        ->orderBy('rubro_id','asc')
        ->orderBy('nombre','asc')
        ->get();
        return view('registro.materiales.editar', compact('material','rubros','colores','tiposMateriales'));
    }

    public function actualizar(MaterialRequest $request, Material $material){
        if (! $request-> exists('es_liso')){
            $request->merge(['es_liso'=>false]);
        }
        $material->update($request->all());
        return redirect()->route('materiales')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(Material $material){
        $referencias = DetalleCompra::whereMaterial_id($material->id)->count(); 
        if($referencias>0){
            return redirect()->route('materiales')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }
        $referencias = AjusteStock::whereMaterial_id($material->id)->count(); 
        if($referencias>0){
            return redirect()->route('materiales')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }
        $referencias = DB::table('material_confeccion')->where('material_id', '=', $material->id)->count(); 
        if($referencias>0){
            return redirect()->route('materiales')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }
        $material->delete();
        return redirect()->route('materiales')->with('message', 'Registro eliminado exitosamente.');
    }

    public function verificarFaltanteMateriales(){
        $materiales = Material::where('faltante','>',0)->get();
        $detalle = array();
        foreach($materiales as $m){
            $detalle[$m->id]['nombre'] = $m->codigo . ' - ' . $m->tipo_material->nombre . ' ' . $m->color->nombre;
            $detalle[$m->id]['cantidad'] = $m->tipo_material->convertir($m->faltante, $m->tipo_material->unidad_medida_por_bulto);
            $detalle[$m->id]['unidadMedida'] = $m->tipo_material->unidad_medida_por_bulto;
        }
        /* si hay algo en el detalle, se envia un mail al administrador */
        if(count($detalle)>0){
            $datos['titulo'] = 'Faltante de materiales';
            $datos['detalle'] = $detalle;
            Mail::to(User::whereRol_id(1)->first())->send(new AvisoPedidoProveedor($datos));
            return redirect()->route('materiales')->withErrors('Existe faltante de materiales. Le enviamos por correo el detalle de los materiales a comprar.');
        }else{
            return redirect()->route('materiales')->with('message', 'No se registra faltante de materiales.');
        }
    }
}
