<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoProducto;
use App\Http\Requests\TipoProductoRequest;
use App\MedidaRequerida;
use App\Producto;

class TipoProductoController extends Controller
{
    public function index(){
        $tiposProductos = TipoProducto::with('medidas_requeridas')->get(); 
        return view('administracion.tipos-productos.index', compact('tiposProductos'));
    }

    public function crear(){
        $medidas = MedidaRequerida::all();
        return view('administracion.tipos-productos.crear', compact('medidas'));
    }

    public function almacenar(TipoProductoRequest $request){
        $tp= TipoProducto::create($request->all());
        $tp->medidas_requeridas()->attach($request->medidas);
        return redirect()->route('tipos-productos')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(TipoProducto $tipoProducto){
        $medidas = MedidaRequerida::all();
        return view('administracion.tipos-productos.editar', compact('tipoProducto','medidas'));
    }

    public function actualizar(TipoProductoRequest $request, TipoProducto $tipoProducto){   
        $tipoProducto->update($request->all());
        $tipoProducto->medidas_requeridas()->sync($request->medidas);
        return redirect()->route('tipos-productos')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(TipoProducto $tipoProducto){
        $referencias = Producto::whereTipo_producto_id($tipoProducto->id)->count(); 
        if($referencias>0){
            return redirect()->route('tipos-productos')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }
        $tipoProducto->medidas_requeridas()->detach();
        $tipoProducto->delete();
        return redirect()->route('tipos-productos')->with('message', 'Registro eliminado exitosamente.');
    }
}
