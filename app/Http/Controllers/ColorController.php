<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Color;

use App\Material;

use App\Http\Requests\ColorRequest;

class ColorController extends Controller
{
    public function index(){
        $colores = Color::all(); 
        return view('administracion.colores.index', compact('colores'));
    }

    public function crear(){
        return view('administracion.colores.crear');
    }

    public function almacenar(ColorRequest $request){
        Color::updateOrCreate(['nombre'=>$request->nombre],[]);
        return redirect()->route('colores')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Color $color){
        return view('administracion.colores.editar', compact('color'));
    }

    public function actualizar(ColorRequest $request, Color $color){
        $color->update($request->all());
        return redirect()->route('colores')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(Color $color){
        $referencias = Material::whereColor_id($color->id)->count(); 
        if($referencias>0){
            return redirect()->route('colores')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $color->delete();
        return redirect()->route('colores')->with('message', 'Registro eliminado exitosamente.');
    }
}
