<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Compra;
use App\Proveedor;
use App\Material;
use App\TipoMaterial;
use App\DetalleCompra;
use Illuminate\Validation\Rule;
use App\FuncionesTrait;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Artisan;

class CompraController extends Controller
{
    public function index(Request $request){
        $request->validate([
            'desde' => 'nullable|date|before_or_equal:now',
            'hasta' => 'nullable|date|after_or_equal:desde|before_or_equal:now',
        ],[
            'date'  => 'Ingrese una fecha válida',
            'desde.before_or_equal' => 'Fecha no válida',
            'hasta.before_or_equal' => 'Fecha no válida',
            'after_or_equal' => 'Ingrese una fecha posterior a "desde"',
        ]);

        $desde= $request->desde;
        $hasta= $request->hasta;
        $proveedor= $request->proveedor_id;

        $sql= Compra::with(['proveedor','detalles_compra','detalles_compra.material', 'detalles_compra.material.tipo_material'])->orderBy('id','DESC');
        
        if(!is_null($desde)){
            $sql= $sql->where('fecha', '>=', $desde);
        }

        if(!is_null($hasta)){
            $sql= $sql->where('fecha', '<=', $hasta);
        }

        if(!is_null($proveedor)){
            $sql= $sql->whereProveedor_id($proveedor);
        }

        $compras = $sql->get();

        $proveedores = Proveedor::all();

        if($request->exists('descargar')){
            return $this->descargar($compras, $desde, $hasta, $proveedor);
        }

        return view('registro.compras.index', compact('compras','proveedores','desde','hasta','proveedor'));
    }

    public function nueva(){
        $proveedores= Proveedor::all();
        $materiales= DB::table('tipos_materiales')
        ->join('materiales', 'tipos_materiales.id', '=', 'materiales.tipo_material_id')
        ->join('colores','colores.id','=','materiales.color_id')
        ->select('materiales.id','tipos_materiales.nombre as tipo','unidad_medida_por_bulto','codigo','colores.nombre as color')
        ->orderBy('rubro_id','asc')
        ->orderBy('tipos_materiales.nombre','asc')
        ->orderBy('colores.nombre','asc')
        ->get();
        return view('registro.compras.crear', compact('proveedores','materiales'));
    }

    public function almacenar(Request $request){
        /* Validaciones */
        $request->validate([
            'fecha'             => 'required|date',
            'nro_comprobante'   => ['required', Rule::unique('compras')->where(function ($query) use ($request) {
                return $query->whereProveedor_id($request->proveedor_id);
            }), 'regex:/[0-9]{4}[-][0-9]{8}/', 'size:13',],
            'proveedor_id'      => 'required',
            'cantidades'        => 'required',
        ], [
            'required'     => 'Campo obligatorio',
            'digits'       => 'Valor inválido',
            'date'         => 'Ingrese una fecha válida',
            'unique'       => 'Compra ya ingresada',
            'size'         => 'Longitud inválida',
            'regex'        => 'Formato incorrecto',
        ]);

        $reglas = array();

        foreach ($request->cantidades as $i => $value){
            $reglas['cantidades.' . $i] = 'required|numeric';
            $reglas['materiales.' . $i] = 'required';
            $reglas['costos.' . $i] = 'required|numeric';
        }

        $messages = [
            'required'     => 'Campo obligatorio',
            'numeric'      => 'Campo numérico',
        ];

        $request->validate($reglas, $messages);

        /* Calcula el total y lo aniade al request */
        $total = 0;

        foreach ($request->cantidades as $i => $cant){
            $total += $request->costos[$i] * $request->cantidades[$i]; 
        }

        $request->merge(['total' => $total]);
        
        /* Crea la compra */
        $c = Compra::create($request->all());

        foreach ($request->cantidades as $i => $cant){
            /* Crea el detalle y asocia a la compra */
            DetalleCompra::create([
                'cantidad' => $cant,
                'precio_unitario' => $request->costos[$i],
                'material_id' => $request->materiales[$i],
                'compra_id' => $c->id,
            ]);
            /* busca el material */
            $m = Material::find($request->materiales[$i]);
            /* convierte el ingreso a unidad de medida en stock*/
            $ingreso = $m->tipo_material->convertir($request->cantidades[$i],$m->tipo_material->unidad_medida_en_stock);
            /* suma el ingreso al stock*/
            $m->update([
                'stock' => $m->stock + $ingreso,
            ]);
            /* si hay faltante y el ingreso lo cubre, se reserva el faltante*/
            if($m->faltante > 0 && $ingreso >= $m->faltante){
                $m->update([
                    'reservado' => $m->reservado + $m->faltante,
                    'faltante'  => 0,
                ]);
            }
            /* si hay faltante y el ingreso no lo cubre, se reserva el ingreso y se resta el faltante */
            if($m->faltante > 0 && $ingreso < $m->faltante){
                $m->update([
                    'reservado' => $m->reservado + $ingreso,
                    'faltante'  => $m->faltante - $ingreso,
                ]);
            }

            /* Actualiza el costo del tipo de material */
            $tm = $m->tipo_material;
            $costo = FuncionesTrait::pesoDolar($request->costos[$i]);
            
            $tm->update([
                'costo' => $costo,
            ]);

        }

        Artisan::call('tarea:planificacion');
        return redirect()->route('compras')->with('message', 'Registro almacenado exitosamente.');
        
    }

    public function descargar($compras, $desde, $hasta, $p){
        $proveedor = Proveedor::find($p);
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $data = [
            'compras'     => $compras,
            'desde'       => $desde,
            'hasta'       => $hasta,
            'proveedor'   => $proveedor,
        ];
        $pdf->loadView('registro.compras.reporte', $data);
        return $pdf->stream();
    }

}
