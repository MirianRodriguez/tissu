<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pedido;
use App\Asignacion;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rol = auth()->user()->rol_id;

        switch ($rol) {
            //confeccionista
            case 2:
                $asignaciones = Asignacion::where('usuario_id','=',auth()->user()->id)->where(function($query) {
                    $query->where('fecha','=', date('Y-m-d'))
                        ->orWhere('esta_cumplido',false);
                })
                ->with(['confeccion_tarea','confeccion_tarea.tarea','confeccion_tarea.confeccion','confeccion_tarea.confeccion.medidas'])->get();
                return view('confeccionistas.asignaciones.index', compact('asignaciones'));
                break;
            //auditor
            case 3:
                $tablas = DB::select("SHOW TABLES");

                $request = [
                    'tabla'          => null,
                    'rid'            => null,
                    'clave'          => null,
                    'accion'         => null,
                    'valor_anterior' => null,
                    'valor_nuevo'    => null,
                    'user_id'        => null,
                    'desde'          => null,
                    'hasta'          => null,
                ];
        
                return view('auditor.todos', compact('request', 'tablas'));
                break;
            //Registrador
            case 4:
                $pedidos = Pedido::with('cliente')->orderBy('id','desc')->get(); 
                return view('registro.pedidos.index', compact('pedidos'));
                break;
            default:
                return view('home');
        }
    }
}
