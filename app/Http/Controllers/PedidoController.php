<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
use App\Cliente;
use App\Producto;
use App\Confeccion;
use App\MedidaConfeccion;
use App\ConfeccionTarea;
use App\User;
use App\TareaProducto;
use Carbon\Carbon;
use App\Http\Requests\PedidoRequest;
use Illuminate\Support\Facades\DB;
use App\Mail\ComprobantePedido;
use App\Mail\AvisoPedidoProveedor;
use App\Mail\AvisoPedidoTerminado;
use Illuminate\Support\Facades\Mail;
use App\FuncionesTrait;
use Barryvdh\DomPDF\Facade as PDF;
use App\Parametro;
use App\PagoRequerido;
use DateTime;


class PedidoController extends Controller
{
    public function index(){
        $pedidos = Pedido::with('cliente')->orderBy('id','desc')->get(); 
        return view('registro.pedidos.index', compact('pedidos'));
    }

    public function crear(){
        $clientes= Cliente::orderBy('apellidos','asc')->orderBy('nombres','asc')->get();
        $productos= Producto::whereEsta_activo(true)->orderBy('nombre')->get();
        return view('registro.pedidos.crear-pedido', compact('clientes','productos'));
    }

    public function almacenar(PedidoRequest $request){

        /* VALIDACION */

        $reglas = array();

        foreach ($request->cantidades as $i => $value){
            $reglas['cantidades.' . $i] = 'digits_between:0,999999999999';
        }

        $messages = [
            'digits_between' => 'Campo numérico',
        ];

        $request->validate($reglas, $messages);

        $request->merge(['fecha' => now('America/Argentina/Buenos_Aires')]);
       
        /* Crea el pedido, crea las confecciones y las asocia al pedido */
        DB::transaction(function () use ($request) {
        
            $pedido = Pedido::create($request->all());

            foreach($request->cantidades as $i => $cant){
                for($j=0; $j<$cant; $j++){
                    Confeccion::create([
                        'producto_id' => $request->productos[$i],
                        'pedido_id'   => $pedido->id,
                    ]);
                }
            }
            /* para cada confeccion creo las tareas */
            foreach ($pedido->confecciones as $c){
                foreach($c->producto->tareas_producto as $tp){
                    ConfeccionTarea::create([
                        'estado_tarea_id'   => '1',
                        'confeccion_id'     =>  $c->id,
                        'tarea_id'          =>  $tp->tarea_id,
                    ]);
                }
            }
            /* genera el cronograma de pagos */
            $this->generar_cronograma_pagos($pedido);

            /* recopila los datos necesarios para el email */

            $datos['titulo'] = 'Comprobante de pedido';
            $datos['nombre'] = $pedido->cliente->nombres;

            /*genera el pdf para mandar por mail */
            $detalle = DB::table('confecciones')->join('productos', 'confecciones.producto_id', '=', 'productos.id')
                ->select(DB::raw('count(*) as cant, productos.nombre'))
                ->where('pedido_id', '=', $pedido->id)
                ->groupBy('productos.nombre')
                ->get();
        
            $pdf = \App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $data = [
                'pedido'     => $pedido,
                'detalle'    => $detalle,
            ];
            $pdf->loadView('registro.pedidos.reporte', $data);

            /* envia el email */
            Mail::to($pedido->cliente)->send(new ComprobantePedido($datos, $pdf->output()));

        });

        return redirect()->route('pedidos')->with('message', 'Pedido almacenado exitosamente.');


    }


    public function verDetalles(Pedido $pedido){      
        return view('registro.pedidos.ver-detalles', compact('pedido'));
    }

    public function generar(Request $request){

        $fechaEntrega = $this->calcularFechaEntrega($request);
        $presupuesto = number_format($this->calcularPresupuesto($request), 2, ',', '');
        $cotizacion = Parametro::whereClave('valor_dolar')->first();
        $fechaDolar = $cotizacion->updated_at;
        
        return response()->json([
            'fechaEntrega' => $fechaEntrega,
            'presupuesto'=> $presupuesto,
            'fechaDolar' => $fechaDolar,
        ]);
    }

    public function calcularFechaEntrega(Request $request){

        /*Obtiene los dias de demora en caso que no haya stock de los materiales necesarios*/
        $demoras = array();
        foreach($request->productos as $i => $p){
            $p = Producto::find($p);
            foreach($p->tipos_materiales_producto as $j => $tmp){
                $demoras[$i . '.' . $j] = $tmp->tipo_material->proveedor->demora;
            }
        }
        $diasDemoraProveedor = max($demoras);

        /*Obtiene la lista de usuarios confeccionistas*/
        $confeccionistas = User::whereRol_id('2')->get();

        /* crea un array con tantas posiciones como confeccionistas 
        tenga el taller para hacer asignaciones virtuales de tareas*/
        $asignaciones = array();
        foreach ($confeccionistas as $c){
            $asignaciones[$c->id] = 0;
        } 

        /*asigna virtualmente a cada confeccionista las tareas pendientes de pedidos registrados previamente*/ 
        $confeccionesTareasPendientes = ConfeccionTarea::whereEstado_tarea_id(1)->with('confeccion')->get();
        foreach($confeccionesTareasPendientes as $ctp){
            $tp = TareaProducto::whereTarea_id($ctp->tarea_id)->whereProducto_id($ctp->confeccion->producto_id)->first();
            $i = array_search(min($asignaciones), $asignaciones);
            $asignaciones[$i] += $tp->dificultad->tiempo_ejecucion;
        }

        /* obtiene las jornadas de trabajo pendientes */
        $jornadasLaboralesPendientes = ceil(max($asignaciones)/8);
        

        if($jornadasLaboralesPendientes > $diasDemoraProveedor){
           
            /*asigna virtualmente a cada confeccionista las tareas a realizar para el nuevo pedido*/
            foreach($request->productos as $j => $p){
                $producto = Producto::find($p);
                for($cant=0; $cant<$request->cantidades[$j]; $cant++){
                    foreach($producto->tareas_producto as $tp){
                        $i = array_search(min($asignaciones), $asignaciones);
                        $asignaciones[$i] += $tp->dificultad->tiempo_ejecucion;                                                 
                    }
                }     
            }

            /* calcula el numero de jornadas laborales necesarias para terminar los pedidos pendientes 
            y el pedido actual */
            $jornadasLaboralesNecesarias = ceil(max($asignaciones)/8);

        }else{
            /* reinicia el array de asignaciones */
            foreach ($asignaciones as $i => $a){
                $asignaciones[$i] = 0;
            } 
            
            /*asigna virtualmente a cada confeccionista las tareas a realizar para el nuevo pedido*/
            foreach($request->productos as $j => $p){
                $producto = Producto::find($p);
                for($cant=0; $cant<$request->cantidades[$j]; $cant++){
                    foreach($producto->tareas_producto as $tp){
                        $i = array_search(min($asignaciones), $asignaciones);
                        $asignaciones[$i] += $tp->dificultad->tiempo_ejecucion;                                                 
                    }
                }     
            }
            /* calcula el numero de jornadas laborales necesarias para confeccionar el pedido actual 
            sumado al numero de dias de demora del proveeder*/
            $jornadasLaboralesNecesarias = ceil(max($asignaciones)/8) + $diasDemoraProveedor;
        }        

        /* suma la cantidad de jornadas laborales necesarias a la fecha actual */
        $fechaActual = Carbon::now();
        $fechaEntrega = $fechaActual->addWeekdays($jornadasLaboralesNecesarias);
       
        
        /* devuelve la fecha de entrega */
        return $fechaEntrega;
        
    }

    public function calcularPresupuesto(Request $request){
        $precioPedido = 0;
        foreach ($request->productos as $i => $p){
            $p = Producto::find($p);
            $costoProducto = 0;
            $precioProducto = 0;
            foreach ($p->tipos_materiales_producto as $tmp){
                $costoMaterial = FuncionesTrait::dolarPeso($tmp->tipo_material->costo);
                $cantidadMaterial = $tmp->tipo_material->convertir($tmp->cantidad_necesaria, $tmp->tipo_material->unidad_medida_por_bulto);
                $costoProducto += $costoMaterial * $cantidadMaterial;
            }
            $precioProducto = $costoProducto + $costoProducto * ($p->complejidad->porcentaje_ganancia/100);
            
            $precioPedido += $precioProducto * $request->cantidades[$i];
        }
        return $precioPedido;
    }

    public function descargar(Pedido $pedido){
        $detalle = DB::table('confecciones')->join('productos', 'confecciones.producto_id', '=', 'productos.id')
            ->select(DB::raw('count(*) as cant, productos.nombre'))
            ->where('pedido_id', '=', $pedido->id)
            ->groupBy('productos.nombre')
            ->get();
       
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $data = [
            'pedido'     => $pedido,
            'detalle'    => $detalle,
        ];
        $pdf->loadView('registro.pedidos.reporte', $data);
        return $pdf->stream();
    }

 
    
    public function generar_cronograma_pagos(Pedido $pedido){
        /* Para el primer pago requerido el monto es el costo de materiales, y la fecha el dia */
        $costoMateriales = 0;
        foreach ($pedido->confecciones as $c){
            foreach($c->producto->tipos_materiales_producto as $tmp){
                $costoMateriales += FuncionesTrait::dolarPeso($tmp->tipo_material->costo);
            }
        }

        $primerPago = PagoRequerido::create([
            'fecha'         => date('Y-m-d'),
            'monto'         => ceil($costoMateriales * 100) / 100,
            'esta_pagado'   => false,
            'pedido_id'     => $pedido->id,
        ]);
        /* Para el 2do pago, el monto es la mitad de lo que falta y la fecha es la 
        media entre el dia actual y la fecha de entrega */
        $montoSegundoPago = ceil(($pedido->presupuesto - $primerPago->monto) * 50) / 100;
        $fechaEntrega = Carbon::create($pedido->fecha_pactada);
        $fechaPrimerPago = Carbon::create($primerPago->fecha);
        $diferenciaDias = $fechaEntrega->diffInDays($fechaPrimerPago);
        $fechaSegundoPago = $fechaPrimerPago->addDays(round($diferenciaDias/2));
        if($fechaSegundoPago->dayOfWeek == 6){
            $fechaSegundoPago->subDays(1);
        }
        if($fechaSegundoPago->dayOfWeek == 0){
            $fechaSegundoPago->addDays(1);
        }
        $segundoPago = PagoRequerido::create([
            'fecha'         => $fechaSegundoPago->format('Y-m-d'),
            'monto'         => $montoSegundoPago,
            'esta_pagado'   => false,
            'pedido_id'     => $pedido->id,
        ]);
        /* Para el tercer pago, el monto es lo que resta para completar el pago, 
        y la fecha es la fecha de entrega */
        $montoTercerPago = $pedido->presupuesto - $primerPago->monto - $segundoPago->monto;
        $tercerPago = PagoRequerido::create([
            'fecha'         => $pedido->fecha_pactada,
            'monto'         => $montoTercerPago,
            'esta_pagado'   => false,
            'pedido_id'     => $pedido->id,
        ]);
    }

    public function entregar(Pedido $pedido){
        $alDia = true;
        foreach($pedido->pagos_requeridos as $pr){
            if(!$pr->esta_pagado){
                $alDia = false;
            }
        }
        if($pedido->porcentaje_avance == 100){
            if($alDia){
                $pedido->update([
                    'entregado' => true,
                ]);
                return redirect()->route('pedidos')->with('message', 'Se ha registrado la entrega del pedido');
            }else{
                return redirect()->route('pedidos')
                            ->withErrors('El pedido tiene pagos pendientes.');
            }
        }else{
            return redirect()->route('pedidos')
                            ->withErrors('El pedido aún está en proceso de confección.');
        }
    }
}
