<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asignacion;
use App\ConfeccionTarea;
use App\TareaProducto;
use Artisan;

class AsignacionController extends Controller
{
    public function index(){
        $asignaciones = Asignacion::whereUsuario_id(auth()->user()->id)->whereFecha(date('Y-m-d'))
            ->orWhere('usuario_id', auth()->user()->id)->whereFecha_fin(date('Y-m-d'))
            ->orWhere('usuario_id', auth()->user()->id)->whereFecha_fin(null)
        ->with(['confeccion_tarea','confeccion_tarea.tarea','confeccion_tarea.confeccion','confeccion_tarea.confeccion.medidas'])->get();
        //dd($asignaciones);
        return view('confeccionistas.asignaciones.index', compact('asignaciones'));
    }

    public function verDetalles(Asignacion $asignacion){
        return view('confeccionistas.asignaciones.ver-detalles', compact('asignacion'));
    }

    public function finalizar(Asignacion $asignacion){
        /* marco la tarea como cumplida */
        $asignacion->update([
            'esta_cumplido' => true,
            'fecha_fin'     => date('Y-m-d'),
        ]);
        /* busco la confeccion tarea asociada y cambio el estado a finalizado */
        $confeccionTarea = ConfeccionTarea::whereAsignacion_id($asignacion->id);
        $confeccionTarea->update([
            'estado_tarea_id' => 3,
        ]);
        /* actualizo el stock de los materiales de la confeccion 
        si la asignacion corresponde a la ultima tarea */
        $ultimaTareaConfeccion = $asignacion->confeccion_tarea->confeccion->confecciones_tareas->last();
        if($ultimaTareaConfeccion->tarea_id == $asignacion->confeccion_tarea->tarea_id){
            $materialesConfeccion = $asignacion->confeccion_tarea->confeccion->materiales;
            $tiposMaterialesProducto = $asignacion->confeccion_tarea->confeccion->producto->tipos_materiales_producto;
            foreach ($materialesConfeccion as $i => $mc){
                $mc->update([
                    'stock'     => $mc->stock - $tiposMaterialesProducto[$i]->cantidad_necesaria,
                    'reservado' => $mc->reservado > 0 ? $mc->reservado - $tiposMaterialesProducto[$i]->cantidad_necesaria : $mc->reservado,
                ]);
            }
            $asignacion->confeccion_tarea->confeccion->avance();
        }

        /*vuelve a correr el proceso de asignacion de tareas */
        Artisan::call('tarea:planificacion');

        return redirect()->route('asignaciones')->with('message', 'La tarea se ha marcado como finalizada.');
    }

    public function historial(){
        $historial = Asignacion::whereUsuario_id(auth()->user()->id)
        ->with(['confeccion_tarea','confeccion_tarea.tarea','confeccion_tarea.confeccion','confeccion_tarea.confeccion.medidas'])->orderBy('fecha','desc')->get();
        return view('confeccionistas.asignaciones.historial', compact('historial'));
    }
}
