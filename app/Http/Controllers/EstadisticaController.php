<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EstadisticaController extends Controller
{
    public function productosMasPedidos(Request $request){
        //dd('hola');
        $request->validate([
            'desde' => 'nullable|date|before_or_equal:now',
            'hasta' => 'nullable|date|after_or_equal:desde|before_or_equal:now',
            'limite' => 'nullable|numeric|gt:0',
        ],[
            'date'  => 'Ingrese una fecha válida',
            'desde.before_or_equal' => 'Fecha no válida',
            'hasta.before_or_equal' => 'Fecha no válida',
            'after_or_equal' => 'Ingrese una fecha posterior a "desde"',
            'numeric' => 'Campo numérico',
            'gt' => 'Valor no válido',
        ]);

        $desde= $request->desde;
        $hasta= $request->hasta;
        $limite= $request->limite;

        $sql =  DB::table('confecciones')
        ->join('productos','confecciones.producto_id','=', 'productos.id')
        ->join('pedidos','confecciones.pedido_id','=', 'pedidos.id')
        ->select(DB::raw('count(*) as pedidos, productos.nombre as producto'));
        
        if(!is_null($desde)){
            $sql= $sql->where('fecha', '>=', $desde);
        }

        if(!is_null($hasta)){
            $sql= $sql->where('fecha', '<=', $hasta);
        }

        $sql = $sql->groupBy('productos.nombre')->orderBy('pedidos', 'DESC');

        if(!is_null($limite)){
            $sql= $sql->limit($limite);
        }

        $productos = $sql->get();

        //dd($productos);
                
        return view('estadisticas.productos', compact('productos','desde','hasta','limite'));
    }

    public function ingresos(){
        $inicio = date('Y-m-01',strtotime('-1 year'));
        $fin = date('Y-m-01', strtotime('now'));
        //dd($fin);
        $ingresos = DB::table('movimientos')
        ->select(DB::raw('sum(monto) as total, date_format(fecha, "%m/%y") as mes'))
        ->where('fecha', '>=', $inicio)
        ->where('fecha', '<', $fin)
        ->groupBy('mes')
        ->orderBy('fecha')
        ->get();
        //dd($ingresos);
        foreach($ingresos as $i){
            $i->importe = '$ ' . number_format($i->total, 2, ',', '.');
        }
        //dd($ingresos);
        return view('estadisticas.ingresos', compact('ingresos'));
    }
}
