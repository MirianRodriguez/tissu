<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\User;
use App\Rol;
use App\Http\Requests\UsuarioRequest;

class UsuarioController extends Controller
{
    public function index(){
        $usuarios = User::all(); 
        return view('administracion.usuarios.index', compact('usuarios'));
    }

    public function crear(){
        $roles = Rol::all();
        return view('administracion.usuarios.crear', compact('roles'));
    }

    public function almacenar(UsuarioRequest $request){
        User::updateOrCreate(['email'=>$request->email],$request->all());
        return redirect()->route('usuarios')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(User $usuario){
        $roles = Rol::all();
        return view('administracion.usuarios.editar', compact('usuario', 'roles'));
    }

    public function actualizar(UsuarioRequest $request, User $usuario){
        $usuario->update($request->all());
        return redirect()->route('usuarios')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(User $usuario){
        $usuario->delete();
        return redirect()->route('usuarios')->with('message', 'Registro eliminado exitosamente.');
    }
}
