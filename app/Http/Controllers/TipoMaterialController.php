<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\TipoMaterial;
use App\Material;
use App\Http\Requests\TipoMaterialRequest;
use App\UnidadMedida;
use App\Proveedor;
use App\FuncionesTrait;
use Illuminate\Support\Facades\DB;

class TipoMaterialController extends Controller
{
    public function index(){
        $tiposMateriales = TipoMaterial::all(); 
        return view('administracion.tipos-materiales.index', compact('tiposMateriales'));
    }

    public function crear(){
        $unidades = UnidadMedida::all();
        $proveedores = Proveedor::orderBy('nombre')->get();
        return view('administracion.tipos-materiales.crear', compact('unidades','proveedores'));
    }

    public function almacenar(TipoMaterialRequest $request){
        $tipoMaterial = TipoMaterial::create($request->all());
        $tipoMaterial->update(['costo' => funcionesTrait::pesoDolar($tipoMaterial->costo)]);
        return redirect()->route('tipos-materiales')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(TipoMaterial $tipoMaterial){
        $unidades = UnidadMedida::all();
        $proveedores = Proveedor::orderBy('nombre')->get();
        return view('administracion.tipos-materiales.editar', compact('tipoMaterial','unidades','proveedores'));
    }

    public function actualizar(TipoMaterialRequest $request, TipoMaterial $tipoMaterial){
        $tipoMaterial->update($request->all());
        return redirect()->route('tipos-materiales')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(TipoMaterial $tipoMaterial){
        $referencias = Material::whereTipo_material_id($tipoMaterial->id)->count(); 
        if($referencias>0){
            return redirect()->route('tipos-materiales')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $referencias = DB::table('tipos_materiales_productos')->where('tipo_material_id', '=', $tipoMaterial->id)->count();
        if($referencias>0){
            return redirect()->route('tipos-materiales')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $tipoMaterial->delete();
        return redirect()->route('tipos-materiales')->with('message', 'Registro eliminado exitosamente.');
    }
}
