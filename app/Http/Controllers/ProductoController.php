<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\TipoProducto;
use App\Complejidad;
use App\TipoMaterial;
use App\TipoMaterialProducto;
use App\Tarea;
use App\DificultadTarea;
use App\TareaProducto;
use App\Confeccion;
use App\Http\Requests\ProductoRequest;
use Illuminate\Support\Facades\DB;

class ProductoController extends Controller
{
    public function index(){
        $productos = Producto::orderBy('nombre','asc')->get(); 
        return view('registro.productos.index', compact('productos'));
    }

    public function crear(){
        $tipos_productos= TipoProducto::orderBy('nombre')->get();
        $complejidades= Complejidad::all();
        $tipos_materiales= DB::table('tipos_materiales')
        ->join('materiales', 'tipos_materiales.id', '=', 'materiales.tipo_material_id')
        ->select('tipos_materiales.id','nombre','unidad_medida_en_stock')
        ->groupBy('nombre','tipos_materiales.id','unidad_medida_en_stock')
        ->orderBy('rubro_id','asc')
        ->orderBy('nombre','asc')
        ->get();
        return view('registro.productos.crear', compact('tipos_productos','complejidades','tipos_materiales'));
       // select nombre from tipos_materiales inner join materiales on tipos_materiales.id = materiales.tipo_material_id group by nombre order by rubro_id, nombre
    }

    public function almacenar(ProductoRequest $request){

        //creo el producto
        $producto = Producto::updateOrCreate(['nombre'=>$request->nombre],$request->all());

        //creo los tipos materiales productos (request->cantidades y request->tipos_materiales son los arrays que vienen de la vista)
        foreach($request->cantidades as $i => $cant){
            TipoMaterialProducto::create([
                'cantidad_necesaria'=> $cant,
                'tipo_material_id'  => $request->tipos_materiales[$i],
                'producto_id'       => $producto->id,
            ]);
        }

        return redirect()->route('productos.cargar-tareas',$producto);
    }

    public function editar(Producto $producto){  
        $confecciones = Confeccion::whereProducto_id($producto->id)->count(); 
        if($confecciones>0){
            return redirect()->route('productos')
                            ->withErrors('No se puede modificar el registro porque existen confecciones en proceso.');
        }  
        $tipos_productos= TipoProducto::orderBy('nombre')->get();
        $complejidades= Complejidad::all();
        $tipos_materiales= DB::table('tipos_materiales')
        ->join('materiales', 'tipos_materiales.id', '=', 'materiales.tipo_material_id')
        ->select('tipos_materiales.id','nombre','unidad_medida_en_stock')
        ->groupBy('nombre','tipos_materiales.id','unidad_medida_en_stock')
        ->orderBy('rubro_id','asc')
        ->orderBy('nombre','asc')
        ->get();
        return view('registro.productos.editar', compact('producto','tipos_productos', 'complejidades','tipos_materiales'));
    }

    public function actualizar(ProductoRequest $request, Producto $producto){  
        $producto->update($request->all());
        foreach( $producto->tipos_materiales_producto as $tmp){
            if (!in_array($tmp->tipo_material_id, $request->tipos_materiales)){
                $tmp->delete();
            }
        }
        foreach( $request->tipos_materiales as $i => $tm ){
            TipoMaterialProducto::updateOrCreate([
                'tipo_material_id'  => $tm,
                'producto_id'       => $producto->id,
            ],[
                'cantidad_necesaria'=> $request->cantidades[$i],
            ]);
        }

        return redirect()->route('productos')->with('message', 'Registro modificado exitosamente.');
    }

    //toggle
    public function habilitar(Producto $producto, $status){
        if(($status==1 && count($producto->tareas_producto)>0) || $status==0){
            $producto->update(['esta_activo' => (bool) $status]);  
            return response()->json([
                'cambio'=>true,
                'mensaje'=>'Se ha cambiado el estado del producto.',
            ]);
        }else{
            return response()->json([
                'cambio' => false,
                'mensaje'=>'No se puede habilitar el producto. Debe cargar sus tareas asociadas.',
            ]);
        }
    }

    public function cargarTareas(Producto $producto){
        $confecciones = Confeccion::whereProducto_id($producto->id)->count(); 
        if($confecciones>0){
            return redirect()->route('productos')
                            ->withErrors('No se pueden modificar las tareas del producto porque existen confecciones en proceso.');
        } 
        $tareas = Tarea::All();
        $dificultades_tareas = DificultadTarea::orderBy('tiempo_ejecucion')->get();
        return view('registro.productos.cargar-tareas', compact('producto','tareas', 'dificultades_tareas'));
    }

    public function guardarTareas(Request $request, Producto $producto){
        $request->validate([
            'tareas'        => 'required|array',
        ],[
            'required'      => 'Debe cargar al menos una tarea',
            'array'         => 'Dato no váido',
        ]);

        foreach( $producto->tareas_producto as $tp){
            if (!in_array($tp->tarea_id, $request->tareas)){
                $tp->delete();
            }
        }
        foreach( $request->tareas as $i => $t ){
            TareaProducto::updateOrCreate([
                'tarea_id'          => $t,
                'producto_id'       => $producto->id,
            ],[
                //'predecesora_id'    => $request->predecesoras[$i],
                'predecesora_id'    => $i == 0 ? null : $request->tareas[$i-1],
                'dificultad_id'     => $request->dificultades[$i],
            ]);
        }

        $producto->update(['esta_activo' => 1]);

        return redirect()->route('productos')->with('message', 'Datos almacenados exitosamente. Revíselos. Sólo podrá modificarlos mientras no se registren pedidos sobre el producto');
    }

    public function verDetalles(Producto $producto){
        return view('registro.productos.ver-detalles', compact('producto'));
    }

}
