<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
use App\Producto;
use App\Confeccion;
use App\MedidaConfeccion;
use App\ConfeccionTarea;
use App\User;
use App\TareaProducto;
use Illuminate\Support\Facades\DB;
use Artisan;

class ConfeccionController extends Controller
{
    public function cargarMateriales(Confeccion $confeccion){
        return view('registro.confecciones.definir-material', compact('confeccion'));
    }

    public function guardarMateriales(Confeccion $confeccion, Request $request){

        /* ALMACENAMIENTO */
    
        $confeccion->update([
            'comentario'    => $request->comentario,
        ]);
        $confeccion->materiales()->sync(array_filter($request->materiales));
        /* Verifica la disponibilidad de materiales*/
        $this->verificarDisponibilidadMaterialesConfeccion($confeccion);  
        Artisan::call('tarea:planificacion');
        return redirect()->route('confecciones.cargar-medidas', $confeccion)->with('message', 'Materiales guardados.');
    }

    public function cargarMedidas(Confeccion $confeccion){
        return view('registro.confecciones.cargar-medidas', compact('confeccion'));
    }

    public function guardarMedidas(Confeccion $confeccion, Request $request){
        /* VALIDACION */
        $reglas = array();

        foreach ($request->valores as $i => $value){
            $reglas['valores.'.$i] = 'digits_between:0,999999999999';
        }

        $messages = [
            'digits_between' => 'Campo numérico',
        ];

        $request->validate($reglas, $messages);

        
        foreach($request->valores as $i => $v){
            MedidaConfeccion::updateOrCreate([
                'confeccion_id' => $confeccion->id,
                'medida_requerida_id' => $i,
            ],[
                'valor' => $v,
            ]);
        }
        Artisan::call('tarea:planificacion');
        return redirect()->route('pedidos.ver-detalles', $confeccion->pedido)->with('message', 'Medidas registradas exitosamente.');
    }

    public function verificarDisponibilidadMaterialesConfeccion(Confeccion $confeccion){
        $producto = $confeccion->producto;
        foreach ($producto->tipos_materiales_producto as $tmp){
            $materiales[$tmp->tipo_material_id] = $tmp->cantidad_necesaria;
        }
        /* para cada material de la confeccion */
        foreach ($confeccion->materiales as $m){
            /* si lo disponible es menos que lo necesario */
            if(($m->stock - $m->reservado) < $materiales[$m->tipo_material_id]){
                /* guardo el faltante y la reserva*/
                $m->update([
                    'reservado' => $m->stock,
                    'faltante' => $m->faltante + ($materiales[$m->tipo_material_id] - ($m->stock - $m->reservado)),
                ]);
            }
            /* si lo disponible cubre lo necesario, reservo lo necesario*/
            if(($m->stock - $m->reservado) >= $materiales[$m->tipo_material_id]){
                $m->update([
                    'reservado' => $m->reservado + $materiales[$m->tipo_material_id],
                ]);
            }
        }
    }
}
