<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movimiento;
use App\Cliente;
use App\Pedido;
use App\PagoRequerido;
use App\Mail\ComprobantePago;
use App\Mail\AvisoActualizacionCronograma;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Artisan;

class MovimientoController extends Controller
{
    public function index(){
        $movimientos = Movimiento::orderBy('id','desc')->get(); 
        return view('registro.movimientos.index', compact('movimientos'));
    }

    public function crear(){
        $clientes= Cliente::orderBy('apellidos','asc')->orderBy('nombres','asc')->get();
        return view('registro.movimientos.crear', compact('clientes'));
    }

    public function almacenar(Request $request){
        /* Valido */
//dd($request->all());
        $request->validate([
            'monto'         => 'required|numeric|between:1,999999999999',
            'cliente_id'    => 'required',
            'descripcion'   => 'max:250',
        ],[
            'required'      => 'Campo obligatorio',
            'between'       => 'Ingrese un número entre 1 y 999999999999',
            'max'           => 'Longitud excedida',
            'numeric'       => 'Ingrese un número',
        ]);
        $fecha = date('Y-m-d');
        $request->merge(['fecha' => $fecha]);
        $cliente = Cliente::find($request->cliente_id);
        /* registro el movimiento */
        DB::transaction(function () use ($request, $cliente) {
            $movimiento = Movimiento::create($request->all());
            /* sumo al saldo del cliente */
            $cliente->update([
                'saldo' => $cliente->saldo + $request->monto,
            ]);
            /* Actualizo el cronograma de ser necesario, 
            cancelo los pagos y resto al saldo del cliente  */
            foreach ($request->pagos as $p){
                $pago = PagoRequerido::find($p);
                if ($pago->fecha < today('America/Argentina/Buenos_Aires')->format('Y-m-d')){
                    $fechaOriginal = Carbon::create($pago->fecha);
                    $hoy = Carbon::now();
                    $diferencia = $hoy->diffInDays($fechaOriginal);
                    $siguientes = PagoRequerido::where('fecha', '>', $pago->fecha)->wherePedido_id($pago->pedido_id)->get();
                    foreach ($siguientes as $s){
                        $fechaNueva = Carbon::create($s->fecha)->addDays($diferencia);
                        $s->update(['fecha' => $fechaNueva]);
                    }
                    $fechaEntregaNueva = Carbon::create($pago->pedido->fecha_pactada)->addDays($diferencia);
                    $pago->pedido->update(['fecha_pactada' => $fechaEntregaNueva]);
                    /* mail con aviso del nuevo cronograma al cliente */
                    $datos['titulo'] = 'Actualización cronograma de pagos';
                    $datos['nombre'] = $pago->pedido->cliente->nombres;
                    $datos['pagos'] = $pago->pedido->pagos_requeridos;
                    $datos['fecha'] = $pago->pedido->fecha_pactada();
                    /* envia el email */
                    Mail::to($pago->pedido->cliente)->send(new AvisoActualizacionCronograma($datos));
                }
                $pago->update([
                    'esta_pagado' => true,
                ]);
                $saldoEnCentavos = $cliente->saldo*100;
                $montoEnCentavos = $pago->monto*100;
                $nuevoSaldoEnCentavos = intval("$saldoEnCentavos") - intval("$montoEnCentavos");
                $cliente->update([
                    'saldo' => number_format($nuevoSaldoEnCentavos/100, 2, '.', ''),
                ]);
            }
            /* mail con comprobante al cliente */
            $datos['titulo'] = 'Comprobante de pago';
            $datos['nombre'] = $movimiento->cliente->nombres;
                
            $pdf = \App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $data = [
                'movimiento'     => $movimiento,
            ];
            $pdf->loadView('registro.movimientos.reporte', $data);

            /* envia el email */
            Mail::to($movimiento->cliente)->send(new ComprobantePago($datos, $pdf->output()));
            Artisan::call('tarea:planificacion');
        });

        return redirect()->route('movimientos')->with('message', 'Registro creado exitosamente.');
    }

    public function cargarPagos(Request $request){
        $pedidos = Pedido::whereCliente_id($request->clienteId)->get();
        $cliente = Cliente::find($request->clienteId);
        $retorno = array();
        foreach($pedidos as $p){
            $pr = PagoRequerido::wherePedido_id($p->id)->whereEsta_pagado(false)->get();
            foreach ($pr as $pago){
                $valor = [
                    'texto' => '$ ' . $pago->monto() . ' - ' . $pago->fecha() . ' - Pedido ' . $pago->pedido->id,
                    'id'    => $pago->id,
                    'monto' => $pago->monto,
                    'fecha' => $pago->fecha,
                    'pedido_id' => $pago->pedido->id,
                ];
                $retorno[] = $valor;
            }
        }
        return response()->json([
            'retorno' => $retorno,
            'saldoCliente' => $cliente->saldo,
        ]);
    }

    public function descargar(Movimiento $movimiento){       
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $data = [
            'movimiento'     => $movimiento,
        ];
        $pdf->loadView('registro.movimientos.reporte', $data);
        return $pdf->stream();
    }
}
