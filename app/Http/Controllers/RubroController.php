<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rubro;

use App\Material;

use App\Http\Requests\RubroRequest;

class RubroController extends Controller
{
    public function index(){
        $rubros = Rubro::all(); /*Recupera todos los datos de la bd*/ 
        return view('administracion.rubros.index', compact('rubros'));/* llama a la vista y le envia la variable que contiene los datos */
    }

    /* muestra el formulario */
    public function crear(){
        return view('administracion.rubros.crear');
    }

    /* crea el objeto nuevo y lo guarda en la BD. Vuelve a la vista index */
    public function almacenar(RubroRequest $request){
        Rubro::create($request->all());
        return redirect()->route('rubros')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Rubro $rubro){
        //dd($rubro);   para ver el contenido de la variable
        return view('administracion.rubros.editar', compact('rubro'));
    }

    public function actualizar(RubroRequest $request, Rubro $rubro){
        $rubro->update($request->all());
        return redirect()->route('rubros')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(Rubro $rubro){
        $referencias = Material::whereRubro_id($rubro->id)->count(); /* cuenta las referencias en la tabla relacionada */
        if($referencias>0){
            return redirect()->route('rubros')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $rubro->delete();/* borra el registro */
        return redirect()->route('rubros')->with('message', 'Registro eliminado exitosamente.');
    }
}
