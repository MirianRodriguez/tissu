<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarea;
use App\DificultadTarea;
use App\EstadoTarea;
use App\Http\Requests\TareaRequest;
use App\TareaProducto;
use App\ConfeccionTarea;

class TareaController extends Controller
{
    public function index(){
        $tareas = Tarea::all(); 
        return view('administracion.tareas.index', compact('tareas'));
    }

    public function crear(){
        return view('administracion.tareas.crear');
    }

    public function almacenar(TareaRequest $request){
        Tarea::updateOrCreate(['nombre'=>$request->nombre],[]);
        return redirect()->route('tareas')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Tarea $tarea){
        return view('administracion.tareas.editar', compact('tarea'));
    }

    public function actualizar(TareaRequest $request, Tarea $tarea){
        $tarea->update($request->all());
        return redirect()->route('tareas')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(Tarea $tarea){
        $referencias = TareaProducto::whereTarea_id($tarea->id)->count(); 
        if($referencias>0){
            return redirect()->route('tareas')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }
        $referencias = ConfeccionTarea::whereTarea_id($tarea->id)->count(); 
        if($referencias>0){
            return redirect()->route('tareas')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $tarea->delete();
        return redirect()->route('tareas')->with('message', 'Registro eliminado exitosamente.');
    }
}
