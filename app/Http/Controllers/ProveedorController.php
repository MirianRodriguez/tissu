<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proveedor;
use App\Compra;
use App\Http\Requests\ProveedorRequest;
use App\TipoMaterial;

class ProveedorController extends Controller
{
    public function index(){
        $proveedores = Proveedor::all(); /*Recupera todos los datos de la bd*/ 
        return view('registro.proveedores.index', compact('proveedores'));/* llama a la vista y le envia la variable que contiene los datos */
    }

    /* muestra el formulario */
    public function crear(){
        return view('registro.proveedores.crear');
    }

    /* crea el objeto nuevo y lo guarda en la BD. Vuelve a la vista index */
    public function almacenar(ProveedorRequest $request){
        Proveedor::updateOrCreate(['nombre'=>$request->nombre],$request->all());
        return redirect()->route('proveedores')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Proveedor $proveedor){
        //dd($proveedor);   para ver el contenido de la variable
        return view('registro.proveedores.editar', compact('proveedor'));
    }

    public function actualizar(ProveedorRequest $request, Proveedor $proveedor){
        $proveedor->update($request->all());
        return redirect()->route('proveedores')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(Proveedor $proveedor){
        $referencias = Compra::whereProveedor_id($proveedor->id)->count(); 
        if($referencias>0){
            return redirect()->route('proveedores')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }
        
        $referencias = TipoMaterial::whereProveedor_id($proveedor->id)->count(); 
        if($referencias>0){
            return redirect()->route('proveedores')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $proveedor->delete();
        return redirect()->route('proveedores')->with('message', 'Registro eliminado exitosamente.');
    }
}
