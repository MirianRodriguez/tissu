<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DificultadTarea;
use App\Tarea;
use App\Http\Requests\DificultadTareaRequest;
use App\TareaProducto;

class DificultadTareaController extends Controller
{
    public function index(){
        $dificultades_tareas = DificultadTarea::all(); 
        return view('administracion.dificultades-tareas.index', compact('dificultades_tareas'));
    }

    public function crear(){
        return view('administracion.dificultades-tareas.crear');
    }

    public function almacenar(DificultadTareaRequest $request){
        DificultadTarea::updateOrCreate(['nombre'=>$request->nombre],$request->all());
        return redirect()->route('dificultades-tareas')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(DificultadTarea $dificultadTarea){
        return view('administracion.dificultades-tareas.editar', compact('dificultadTarea'));
    }

    public function actualizar(DificultadTareaRequest $request, DificultadTarea $dificultadTarea){
        $dificultadTarea->update($request->all());
        return redirect()->route('dificultades-tareas')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(DificultadTarea $dificultadTarea){
        $referencias = TareaProducto::whereDificultad_id($dificultadTarea->id)->count(); 
        if($referencias>0){
            return redirect()->route('dificultades-tareas')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $dificultadTarea->delete();
        return redirect()->route('dificultades-tareas')->with('message', 'Registro eliminado exitosamente.');
    }
}
