<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EstadoTarea;
use App\ConfeccionTarea;
use App\Http\Requests\EstadoTareaRequest;


class EstadoTareaController extends Controller
{
    public function index(){
        $estados_tareas = EstadoTarea::all(); 
        return view('administracion.estados-tareas.index', compact('estados_tareas'));
    }

    public function crear(){
        return view('administracion.estados-tareas.crear');
    }

    public function almacenar(EstadoTareaRequest $request){
        EstadoTarea::updateOrCreate(['nombre'=>$request->nombre],$request->all());
        return redirect()->route('estados-tareas')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(EstadoTarea $estadoTarea){
        return view('administracion.estados-tareas.editar', compact('estadoTarea'));
    }

    public function actualizar(EstadoTareaRequest $request, EstadoTarea $estadoTarea){
        $estadoTarea->update($request->all());
        return redirect()->route('estados-tareas')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(EstadoTarea $estadoTarea){
        $referencias = ConfeccionTarea::whereEstado_tarea_id($estadoTarea->id)->count(); 
        if($referencias>0){
            return redirect()->route('estados-tareas')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        if($estadoTarea->id == 1 || $estadoTarea->id == 2 || $estadoTarea->id == 3){
            return redirect()->route('estados-tareas')
                            ->withErrors('No se puede eliminar este registro.');
        }

        $estadoTarea->delete();
        return redirect()->route('estados-tareas')->with('message', 'Registro eliminado exitosamente.');
    }
}
