<?php

namespace App;

class MedidaRequerida extends Auditable
{
    public $timestamps = false;

    protected $table = 'medidas_requeridas'; 

    protected $fillable = ['nombre','descripcion',];

    public function tipos_productos(){
        return $this->belongsToMany('App\TipoProducto', 'medida_requerida_tipo_producto', 'medida_requerida_id', 'tipo_producto_id');
    }
}
