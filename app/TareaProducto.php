<?php

namespace App;

class TareaProducto extends Auditable
{
    public $timestamps = false;

    protected $table = 'tareas_productos'; 

    protected $fillable = ['predecesora_id','dificultad_id','producto_id','tarea_id'];

    public function tarea(){
        return $this->belongsTo('App\Tarea');
    }

    public function predecesora(){
        return $this->belongsTo('App\Tarea');
    }

    public function dificultad(){
        return $this->belongsTo('App\DificultadTarea');
    }

    public function producto(){
        return $this->belongsTo('App\Producto');
    }
}
