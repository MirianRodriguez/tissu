<?php

namespace App;

class TipoMaterialProducto extends Auditable
{
    public $timestamps = false;

    protected $table = 'tipos_materiales_productos'; 

    protected $fillable = ['cantidad_necesaria','tipo_material_id','producto_id',];

    public function tipo_material(){
        return $this->belongsTo('App\TipoMaterial');
    }
}
