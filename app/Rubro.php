<?php

namespace App;

class Rubro extends Auditable
{
    public $timestamps = false;

    /*protected $table = 'rubroSSS'; (solo cuando el plural es especial)*/

    protected $fillable = ['nombre', ];
}
