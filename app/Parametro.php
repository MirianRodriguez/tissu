<?php

namespace App;

class Parametro extends Auditable
{

    protected $table = 'parametros'; 

    protected $fillable = ['clave','valor',];

    
}
