<?php

namespace App;

trait FuncionesTrait
{
    private static function cotizacion(){
        $retorno = Parametro::whereClave('valor_dolar')->first();
        return $retorno->valor;
    }

    static function dolarPeso($dolares){
        $pesos = $dolares * self::cotizacion();
        return $pesos;
    }

    static function pesoDolar($pesos){
        $dolares = $pesos / self::cotizacion();
        return $dolares;
    }
    
}
