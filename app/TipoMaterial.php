<?php

namespace App;

class TipoMaterial extends Auditable
{
    public $timestamps = false;

    protected $table = 'tipos_materiales'; 

    protected $fillable = ['nombre','unidad_medida_por_bulto', 'unidad_medida_en_stock','proveedor_id','costo'];

    public function convertir($numero, $unidad){
        switch ($unidad) {
            case "Mt.":
                return ($numero/100);
                break;
            case "Cm.":
                return ($numero*100);
                break;
            case "Kg.":
                return ($numero/1000);
                break;
            case "Gr.":
                return ($numero*1000);
                break;
            default:
                return $numero;
        }
    }

    function conversor_monedas($amount,$from_currency,$to_currency){
        $apikey = '49449f8b555906c5b55f';
      
        $from_Currency = urlencode($from_currency);
        $to_Currency = urlencode($to_currency);
        $query =  "{$from_Currency}_{$to_Currency}";
      
        // URL para solicitar los datos
        $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
        $obj = json_decode($json, true);
      
        $val = floatval($obj["$query"]);
      
      
        $total = $val * $amount;
        return number_format($total, 2, '.', '');
      }

    /* Relaciones */

    public function materiales()
    {
        return $this->hasMany('App\Material');
    }

    public function proveedor(){
        return $this->belongsTo('App\Proveedor');
    }

    public function materialesOrdenados()
    {
        return Material::select('materiales.*', 'c.nombre as color')
        ->whereTipo_material_id($this->id)
        ->join('colores as c', 'c.id', 'materiales.color_id')
        ->orderBy('color', 'asc')
        ->get();
    }

    /* FORMATOS */

    public function costo(){
        return number_format(FuncionesTrait::dolarPeso($this->costo), 2, ',','.');
    }
}
