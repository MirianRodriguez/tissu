<?php

namespace App;

class Proveedor extends Auditable
{
    public $timestamps = false;

    protected $table = 'proveedores'; 

    protected $fillable = ['nombre', 'telefono', 'email', 'costo_envio', 'demora',];

    public function costo_envio(){
        return number_format($this->costo_envio, 2, ',', '.');
    }
}
