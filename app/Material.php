<?php

namespace App;

class Material extends Auditable
{
    public $timestamps = false;

    protected $table = 'materiales'; /*(solo cuando el plural es especial)*/

    protected $fillable = ['codigo','stock','reservado','faltante','es_liso',
                    'cantidad_por_bulto','descripcion','tipo_material_id','rubro_id','color_id',];

    /* RELACIONES */

    public function rubro(){
        return $this->belongsTo('App\Rubro');
    }

    public function color(){
        return $this->belongsTo('App\Color');
    }

    public function tipo_material(){
        return $this->belongsTo('App\TipoMaterial');
    }

    public function confecciones(){
        return $this->belongsToMany('App\Confeccion', 'material_confeccion', 'material_id', 'confeccion_id');
    }

    public function no_hay_suficiente(Producto $producto){
        $necesario = TipoMaterialProducto::whereProducto_id($producto->id)
                                        ->whereTipo_material_id($this->tipo_material_id)->first()->cantidad_necesaria;
        return $necesario > $this->stock;
    }
    
}
