<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComprobantePedido extends Mailable
{
    use Queueable, SerializesModels;

    public $datos;
    public $pdf;

    public function __construct($datos, $pdf)
    {
        $this->datos = $datos;
        $this->pdf = $pdf;
    }

    public function build()
    {
        return $this->view('mails.comprobante-pedido')->attachData($this->pdf,'comprobante.pdf')->subject('Comprobante de pedido');
    }
}
