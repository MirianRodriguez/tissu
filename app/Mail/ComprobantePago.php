<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComprobantePago extends Mailable
{
    use Queueable, SerializesModels;

    public $datos;
    public $pdf;

    public function __construct($datos, $pdf)
    {
        $this->datos = $datos;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.comprobante-pago')->attachData($this->pdf,'comprobante.pdf')->subject('Comprobante de pago');
    }
}
