<?php

namespace App;

class Producto extends Auditable
{
    public $timestamps = false;

    protected $table = 'productos'; 

    protected $fillable = ['nombre', 'descripcion', 'esta_activo','tipo_producto_id', 'complejidad_id',];

    /* RELACIONES */

    public function tipos_materiales_producto(){
        return $this->hasMany('App\TipoMaterialProducto');
    }

    public function tipo_producto(){
        return $this->belongsTo('App\TipoProducto');
    }

    public function complejidad(){
        return $this->belongsTo('App\Complejidad');
    }

    public function tareas_producto(){
        return $this->hasMany('App\TareaProducto');
    }

    public function confecciones(){
        return $this->hasMany('App\Confeccion');
    }
}
