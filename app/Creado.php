<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creado extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'tabla', 'id_creado', 'clave', 'valor',
                            'fecha', 'user_id' ];
}
