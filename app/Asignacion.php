<?php

namespace App;

class Asignacion extends Auditable
{
    public $timestamps = false;

    protected $table = 'asignaciones'; 

    protected $fillable = ['fecha','esta_cumplido', 'usuario_id','confeccion_tarea_id','fecha_fin'];

    public function confeccion_tarea(){
        return $this->hasOne('App\ConfeccionTarea');
    }

    public function fecha(){
        return date("d/m/Y", strtotime($this->fecha));
    }

    public function fecha_fin(){
        return date("d/m/Y", strtotime($this->fecha_fin));
    }
}
