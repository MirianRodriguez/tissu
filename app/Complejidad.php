<?php

namespace App;

class Complejidad extends Auditable
{
    public $timestamps = false;

    protected $table = 'complejidades'; 

    protected $fillable = ['tipo', 'porcentaje_ganancia',];
    
}
