<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ConfeccionTarea;
use App\Asignacion;
use App\User;
use App\TareaProducto;
use App\Tarea;
use App\Material;
use App\TipoMaterialProducto;
use Illuminate\Support\Facades\Mail;
use App\Mail\AvisoRevisarFaltantes;
use App\PagoRequerido;
use App\Confeccion;
use App\Parametro;

class PlanificacionDiariaTarea extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tarea:planificacion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Asigna tareas pendientes a las confeccionistas para el dia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $avisarFaltante = false;
        /* obtiene pedidos con pagos atrasados */
        $pagosAtrasados = PagoRequerido::where('fecha', '<=', today('America/Argentina/Buenos_Aires'))->whereEsta_pagado(false)->pluck('pedido_id');
        /* obtiene las confecciones de pedidos con pagos atrasados*/
        $confeccionesAtrasadas = Confeccion::whereIn('pedido_id', $pagosAtrasados)->pluck('id');
        /*Obtiene las confecciones tareas pendientes de confecciones sin pagos atrasados*/
        $confeccionesTareasPendientes = ConfeccionTarea::whereEstado_tarea_id(1)->whereNotIn('confeccion_id',$confeccionesAtrasadas)->with('confeccion')->get();
        /*Obtiene usuarios confeccionistas*/
        $confeccionistas = User::whereRol_id('2')->get();
        /* crea un array de asignaciones con tantas posiciones como confeccionistas */
        $asignaciones = array();
        foreach ($confeccionistas as $c){
            /* para cada confeccionista obtengo sus asignaciones pendientes*/
            $pendientes = Asignacion::whereUsuario_id($c->id)->whereFecha(date('Y-m-d'))
            ->orWhere('usuario_id', $c->id)->whereFecha_fin(date('Y-m-d'))
            ->orWhere('usuario_id', $c->id)->whereFecha_fin(null)->get();
            /* si no tiene ninguna, digo que tiene 0 hs asignadas */
            if (count($pendientes)==0){
                $asignaciones[$c->id] = 0;
            /* si no, obtengo la cantidad de hs asignadas que tiene */
            }else{
                $aux = 0;
                foreach($pendientes as $p){
                    $tp = TareaProducto::whereTarea_id($p->confeccion_tarea->tarea_id)->whereProducto_id($p->confeccion_tarea->confeccion->producto_id)->first();
                    $aux += $tp->dificultad->tiempo_ejecucion;
                }
                $asignaciones[$c->id] = $aux;
                
            }
        }      

        /* para cada ctp */
        foreach ($confeccionesTareasPendientes as $ctp){
            /*define si hay stock de los materiales para la confeccion*/
            $hayFaltante = false;
            $hayStock = true;
            foreach($ctp->confeccion->materiales as $m){   
                $tipoMaterialProducto = TipoMaterialProducto::whereTipo_material_id($m->tipo_material_id)->whereProducto_id($ctp->confeccion->producto->id)->first();
                if($tipoMaterialProducto->cantidad_necesaria > $m->stock){
                    $hayStock = false;
                    $hayFaltante = true;
                    $avisarFaltante = true;
                }
            }
            /*revisa que la confeccion tenga materiales y medidas definidas, y si hay stock*/
            if (count($ctp->confeccion->materiales)>0 && count($ctp->confeccion->medidas)>0 && $hayStock){
                /* obtengo la tarea producto asociada */
                $tp = TareaProducto::whereTarea_id($ctp->tarea_id)->whereProducto_id($ctp->confeccion->producto_id)->first();
                /* si no tiene predecesora, asigno */
                if(is_null($tp->predecesora_id)){
                    /* busco el confeccionista con menos asignaciones */
                    $i = array_search(min($asignaciones), $asignaciones);
                    /* si tiene menos de 8 hs de tareas asignadas */
                    if($asignaciones[$i]<8){
                         /* le sumo las hs de trabajo de la tarea producto */
                        $asignaciones[$i] += $tp->dificultad->tiempo_ejecucion;
                        /* creo una asignacion */
                        $nuevaAsignacion = Asignacion::create([
                            'fecha'                 => date('Y-m-d'),
                            'usuario_id'            => $i,
                            'confeccion_tarea_id'   => $ctp->id,
                        ]);
                        /* cambio el estado de la confeccion tarea a asignado */
                        $ctp->update([
                            'estado_tarea_id' => 2,
                            'asignacion_id'   => $nuevaAsignacion->id,
                        ]);
                    }
                /* si tiene predecesora */
                }else{
                    /* busco la confeccion tarea anterior */
                    $confeccionTareaAnterior = ConfeccionTarea::find(($ctp->id-1));
                    if(!is_null($confeccionTareaAnterior)){
                        /* busco la asignacion */
                        $asignacionTareaAnterior = $confeccionTareaAnterior->asignacion;
                        if(!is_null($asignacionTareaAnterior)){
                            /* obtengo el estado */
                            $cumplido = $asignacionTareaAnterior->esta_cumplido;
                        }
                    }
                    /* si ya esta cumplido, asigno */
                    if(!is_null($cumplido) && $cumplido == 1){
                        /* busco el confeccionista con menos asignaciones */
                        $i = array_search(min($asignaciones), $asignaciones);
                        /* si tiene menos de 8 hs de tareas asignadas */
                        if($asignaciones[$i]<8){
                            /* le sumo las hs de trabajo de la tarea producto */
                            $asignaciones[$i] += $tp->dificultad->tiempo_ejecucion;
                            /* creo una asignacion */
                            $nuevaAsignacion = Asignacion::create([
                                'fecha'                 => date('Y-m-d'),
                                'usuario_id'            => $i,
                                'confeccion_tarea_id'   => $ctp->id,
                            ]);
                            /* cambio el estado de la confeccion tarea a asignado */
                            $ctp->update([
                                'estado_tarea_id' => 2,
                                'asignacion_id' => $nuevaAsignacion->id,
                            ]);
                        }
                    }
                }
            }             
        }

        if ($avisarFaltante){
            $p = Parametro::whereClave('ultimo_aviso_faltante')->first();
            if ($p->valor < today('America/Argentina/Buenos_Aires')->format('Y-m-d')){
                $p->update([
                    'valor' => today('America/Argentina/Buenos_Aires')->format('Y-m-d'),
                ]);
                $datos['titulo'] = 'Faltante de materiales';
                Mail::to(User::whereRol_id(1)->first())->send(new AvisoRevisarFaltantes($datos));
            }
        }
    }
}
