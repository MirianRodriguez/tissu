<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Parametro;


class ActualizacionDiariaCotizacion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dolar:actualizacion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtine la cotizacion actual del dolar estadounidense y la almacena para utilizarla en las operaciones del dia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $apikey = '49449f8b555906c5b55f';
      
        $from_Currency = 'USD';
        $to_Currency = 'ARS';

        $query =  "{$from_Currency}_{$to_Currency}";
      
        // URL para solicitar los datos
        $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
        $obj = json_decode($json, true);
      
        $val = floatval($obj["$query"]);
      

        $cotizacion = Parametro::whereClave('valor_dolar')->first();

        $cotizacion->update(['valor' => $val]);

    }
}
