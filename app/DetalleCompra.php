<?php

namespace App;

class DetalleCompra extends Auditable
{
    public $timestamps = false;

    protected $table = 'detalles_compra'; 

    protected $fillable = ['cantidad','precio_unitario', 'material_id','compra_id',];

    public function material(){
        return $this->belongsTo('App\Material');
    }

    /* Formatos */
    public function precio_unitario(){
        return number_format($this->precio_unitario, 2, ',', '.');
    }

}
