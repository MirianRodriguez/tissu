<?php

namespace App;

class EstadoTarea extends Auditable
{
    public $timestamps = false;

    protected $table = 'estados_tareas'; 

    protected $fillable = ['nombre',];
}
