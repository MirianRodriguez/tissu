<?php

namespace App;

class Tarea extends Auditable
{
    public $timestamps = false;

    protected $table = 'tareas'; 

    protected $fillable = ['nombre'];

}
