<?php

namespace App;

class Movimiento extends Auditable
{
    public $timestamps = false;

    protected $fillable = ['fecha', 'monto','descripcion', 'cliente_id',];

    /* RELACIONES */

    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }

    /* FORMATOS */

    public function fecha(){
        return date("d/m/Y", strtotime($this->fecha));
    }

    public function monto(){
        return number_format($this->monto, 2, ',', '.');
    }

    public function fechaTextual(){
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $f = strtotime($this->fecha);

        return date('d', $f) . ' de ' . $meses[date('m', $f) - 1] . ' de ' . date('Y', $f);
    }
}
