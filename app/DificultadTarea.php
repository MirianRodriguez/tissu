<?php

namespace App;

class DificultadTarea extends Auditable
{
    public $timestamps = false;

    protected $table = 'dificultades_tareas'; 

    protected $fillable = ['nombre', 'tiempo_ejecucion',];
}
