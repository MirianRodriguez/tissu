<?php

namespace App;

use App\TipoMaterialProducto;
use App\Medida;
use App\Mail\AvisoPedidoTerminado;
use Illuminate\Support\Facades\Mail;

class Confeccion extends Auditable
{
    public $timestamps = false;

    protected $table = 'confecciones'; 

    protected $fillable = ['comentario','cantidad','producto_id','pedido_id',];

    public function pedido(){
        return $this->belongsTo('App\Pedido');
    }

    public function producto(){
        return $this->belongsTo('App\Producto');
    }

    public function materiales(){
        return $this->belongsToMany('App\Material', 'material_confeccion', 'confeccion_id', 'material_id');
    }

    public function medidas(){
        return $this->hasMany('App\MedidaConfeccion');
    }

    public function confecciones_tareas(){
        return $this->hasMany('App\ConfeccionTarea');
    }

    /* FUNCIONES */
    public function material_elegido(TipoMaterialProducto $tmp){
        $retorno = null;
        foreach($tmp->tipo_material->materiales as $m){
            foreach($this->materiales as $cm){
                if ($m->id == $cm->id){
                    $retorno = $m->id;
                }
            }
        }
        return $retorno;
    }

    public function valor_medida(MedidaRequerida $mr){
        $retorno = null;
        $mc = MedidaConfeccion::whereConfeccion_id($this->id)->whereMedida_requerida_id($mr->id)->first();
        if (!is_null($mc)){
            $retorno = $mc->valor;
        }
        return $retorno;
    }

    public function asignaciones(){
        $tareas = ConfeccionTarea::whereConfeccion_id($this->id)->get();
        //dd($tareas);
        $asignaciones = array();
        foreach($tareas as $t){
            if(!is_null($t->asignacion)){
                $asignaciones[] = $t->asignacion;
            }
        }
        return $asignaciones;
    }

    public function avance(){
        $pedido = $this->pedido;
        $confecciones = $pedido->confecciones;
        $cantidadConfecciones = count($confecciones);
        $porcentajeConfeccion = 100 / $cantidadConfecciones;
        $porcentaje = 0;
        $todas = true;

        foreach($confecciones as $c){
            if($c->confecciones_tareas->last()->estado_tarea_id == 3){
                $porcentaje += $porcentajeConfeccion;
            }else{
                $todas = false;
            }
        }

        if($todas){
            $pedido->update([
                'porcentaje_avance' => 100,
            ]);
            $datos['titulo'] = 'Pedido listo para retirar';
            $datos['nombre'] = $pedido->cliente->nombres;
            Mail::to($pedido->cliente)->send(new AvisoPedidoTerminado($datos));
        }else{
            $pedido->update([
                'porcentaje_avance' => $porcentaje,
            ]);
        }
    } 
}
