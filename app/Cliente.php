<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Cliente extends Auditable
{
    use Notifiable;

    public $timestamps = false;

    protected $fillable = ['apellidos','nombres', 'telefono','email','saldo',];

    public function saldo(){
        return number_format($this->saldo, 2, ',', '.');
    }

    public function pedidos(){
        return $this->hasMany('App\Pedido');
    }
}
