<?php

namespace App;

class PagoRequerido extends Auditable
{
    public $timestamps = false;

    protected $table = 'pagos_requeridos'; 

    protected $fillable = ['fecha','monto','esta_pagado', 'pedido_id'];

    /* RELACIONES */

    public function pedido(){
        return $this->belongsTo('App\Pedido');
    }

    /* FORMATOS */

    public function fecha(){
        return date("d/m/Y", strtotime($this->fecha));
    }

    public function monto(){
        return number_format($this->monto, 2, ',', '.');
    }
}
