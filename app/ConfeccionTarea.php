<?php

namespace App;

class ConfeccionTarea extends Auditable
{
    public $timestamps = false;

    protected $table = 'confecciones_tareas'; 

    protected $fillable = ['estado_tarea_id','confeccion_id','tarea_id','asignacion_id'];

    public function tarea(){
        return $this->belongsTo('App\Tarea');
    }

    public function confeccion(){
        return $this->belongsTo('App\Confeccion');
    }

    public function estado_tarea(){
        return $this->belongsTo('App\EstadoTarea');
    }

    public function asignacion(){
        return $this->hasOne('App\Asignacion');
    }
}
