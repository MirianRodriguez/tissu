<?php

namespace App;

class Color extends Auditable
{
    public $timestamps = false;

    protected $table = 'colores'; 

    protected $fillable = ['nombre',];
    
}
