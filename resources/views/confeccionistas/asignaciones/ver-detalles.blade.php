@extends('layouts.app')

@section('titulo', 'Detalles de la tarea asignadas')

@section('contenido')

<div class="container">
<div class="row">
<div class="col-8 offset-2">
    <div class="card mb-3">
        <div class="card-header">
            Detalles de la tarea asignada
        </div>
        <div class="card-body">
            <h5 class="card-title">{{$asignacion->confeccion_tarea->tarea->nombre}} {{$asignacion->confeccion_tarea->confeccion->producto->nombre}}</h5>
            <p>{{$asignacion->confeccion_tarea->confeccion->producto->descripcion}}</p>
            <p>Tiempo estimado: 
            @php
                $tarea = $asignacion->confeccion_tarea->confeccion->producto->tareas_producto->whereIn('tarea_id', $asignacion->confeccion_tarea->tarea_id)->first();
                print($tarea->dificultad->tiempo_ejecucion);
            @endphp
            Hs. </p>
            @if($asignacion->confeccion_tarea->confeccion->comentario != null)
                <p>Comentarios particulares de la confeccion: {{$asignacion->confeccion_tarea->confeccion->comentario}}</p>
            @endif
            <div>
                <h6 style= "font-weight: bold;">Materiales</h6>
                <ul>
                    @foreach($asignacion->confeccion_tarea->confeccion->materiales as $m)
                        <li>{{$m->tipo_material->nombre}} {{$m->color->nombre}}</li>
                    @endforeach
                </ul>
            </div>
                
            <div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Medida</th>
                            <th scope="col">Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($asignacion->confeccion_tarea->confeccion->medidas as $m)
                        <tr>
                            <td>{{$m->medida_requerida->nombre}}</td>
                            <td>{{$m->valor}}</</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> 
                       
        </div>
        <div class="card-footer d-flex justify-content-center mt-4 ">
            @if($asignacion->esta_cumplido)
                <span class="badge badge-info d-inline mx-1 my-auto">Finalizada</span>
            @else
                <a href="{{route('asignaciones.finalizar', $asignacion->id)}}" class="btn btn-success mx-2">Finalizar</a>
            @endif    
            <a href="{{route('asignaciones')}}" class="btn btn-secondary mx-2">Volver</a>
        </div>
    </div>
    </div>
</div>
</div>

@endsection