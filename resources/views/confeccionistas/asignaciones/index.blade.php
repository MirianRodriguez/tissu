@extends('layouts.app')

@section('titulo', 'Asignaciones')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            /*tabla a la que se le va a aplicar los cambios*/
            $('#asignaciones').DataTable({

                aaSorting: [], /*para que se muestre en el mismo orden que viene del controlador*/

                /* para alinear el texto de las columnas (se numeran desde 0) */
                columnDefs: [
                    {
                        targets: 4, /* centra el contenido de la columna 1 */
                        className: 'dt-center',
                    }
                ],

                /* traducciones */
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "No tiene tareas asignadas",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
@endsection

@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Tareas asignadas</div>
        </div>
    </div>
    <div class="container-fluid">
        @include ('layouts.mensaje')
        <div class="container-fluid mt-3">
            <table id="asignaciones" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Tarea</th>
                        <th>Producto</th>
                        <th>Confección</th>
                        <th>Pedido</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($asignaciones as $a)
                    <tr>
                        <td>{{$a->confeccion_tarea->tarea->nombre}}</td>
                        <td>{{$a->confeccion_tarea->confeccion->producto->nombre}}</td>
                        <td>{{$a->confeccion_tarea->confeccion->id}} 
                        @if ($a->confeccion_tarea->confeccion->comentario!=null)
                        ({{$a->confeccion_tarea->confeccion->comentario}})
                        @endif
                        </td>
                        <td>{{$a->confeccion_tarea->confeccion->pedido->id}} - 
                        {{$a->confeccion_tarea->confeccion->pedido->cliente->apellidos}}, 
                        {{$a->confeccion_tarea->confeccion->pedido->cliente->nombres}}</td>
                        <td>
                            <div class="row ml-2">
                                <a href="{{route('asignaciones.ver-detalles', $a->id)}}" class="btn btn-primary m-1" data-toggle="tooltip" data-placement="bottom" title="Detalles"><i class="fas fa-eye fa-fw"></i></a>
                                
                                @if($a->esta_cumplido)
                                    <span class="badge badge-info d-inline mx-1 my-auto">Finalizada</span>
                                @else
                                    <a href="{{route('asignaciones.finalizar', $a->id)}}" class="btn btn-success m-1">Finalizar</a>
                                @endif
                            </div>                    
                        </td>            
                    </tr>
                    @endforeach           
                </tbody>
            </table>
        </div>
    </div>
@endsection
