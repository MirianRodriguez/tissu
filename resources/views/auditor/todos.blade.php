@extends('layouts.app')

@section('titulo', 'Logs de auditoría')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                allowClear: true,
                theme: 'classic',
                width: '100%',
            });

            $('#registros').DataTable({
                serverSide: true,
                ajax: {
                    url: "{{ url('api/datatables/logs') }}",

                    dataSrc: function (json) {
                        var return_data = [];

                        for (var i = 0; i < json.data.length; i++) {
                            d = new Date(json.data[i].fecha);

                            return_data.push({
                                accion: json.data[i].accion === 'C' ? '<span class="badge badge-success">CREACIÓN</span>' : (json.data[i].accion === 'M' ? '<span class="badge badge-warning">MODIFICACIÓN</span>' : '<span class="badge badge-danger">BORRADO</span>'),
                                user_id: json.data[i].user_id,
                                fecha: `<span class="d-none">${json.data[i].fecha}</span>${("0" + d.getDate()).slice(-2)}/${("0" + (d.getMonth() + 1)).slice(-2)}/${("0" + d.getYear()).slice(-2)} <div class="text-muted">${(json.data[i].fecha).slice(-8)}</div>`,
                                tabla: json.data[i].tabla,
                                rid: json.data[i].rid,
                                clave: json.data[i].clave,
                                anterior: json.data[i].valor_anterior === null ? '' : `<span style="white-space: normal;">${json.data[i].valor_anterior}</span>`,
                                nuevo: json.data[i].valor_nuevo === null ? '' : `<span style="white-space: normal;">${json.data[i].valor_nuevo}</span>`,
                            });
                        }

                        return return_data;
                    }
                },

                columns: [
                    { data: 'accion' },
                    { data: 'user_id' },
                    { data: 'fecha' },
                    { data: 'tabla' },
                    { data: 'rid' },
                    { data: 'clave' },
                    { data: 'anterior' },
                    { data: 'nuevo' },
                ],

                aaSorting: [],

                searching: false,

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script>
@endsection

@section('contenido')
    <div class="p-3" style="min-height: calc(100vh - 144px);">
        <div class="d-block d-md-none mb-4 text-center h2 my-2">
            @yield('titulo')
        </div>

        <div class="d-none d-md-block mb-3 h1">
            @yield('titulo')
        </div>

        @include('auditor.filtros')

        <div class="card mt-4 p-3">
            <table id="registros" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Acción</th>
                        <th>Usuario</th>
                        <th>Fecha</th>
                        <th>Tabla</th>
                        <th>ID</th>
                        <th>Clave</th>
                        <th>Anterior</th>
                        <th>Nuevo</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
