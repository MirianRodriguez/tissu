<form method="POST" action="{{ route('logs.filtrados') }}" class="container-fluid bg-light border rounded border-dark py-3 mt-4 mb-3">
    @csrf

    <div class="row pb-3 text-center text-uppercase font-weight-bold">
        <div class="col">
            Filtrar registros
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 form-group">
            <label for="tabla">Tabla</label>
            <select id="tabla" class="select2 form-control" name="tabla">
                <option></option>

                @foreach ($tablas as $t)
                    @if ( ! in_array($t->Tables_in_tissu, ['borrados', 'creados', 'modificados','failed_jobs']) )
                        @if ( old('tabla', $request['tabla']) == $t->Tables_in_tissu )
                            <option value="{{ $t->Tables_in_tissu }}" selected>{{ $t->Tables_in_tissu }}</option>
                        @else
                            <option value="{{ $t->Tables_in_tissu }}">{{ $t->Tables_in_tissu }}</option>
                        @endif
                    @endif
                @endforeach
            </select>
        </div>

        <div class="col-4 col-md-2 form-group pr-0 pr-md-3">
            <label for="rid">ID</label>
            <input type="number" class="form-control text-right" id="rid" name="rid" value="{{ old('rid', $request['rid']) }}" min="1">
        </div>

        <div class="col-8 col-md-4 form-group">
            <label for="clave">Clave</label>
            <input type="text" class="form-control" id="clave" name="clave" value="{{ old('clave', $request['clave']) }}">
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 form-group">
            <label for="accion">Acción</label>
            <select id="accion" class="select2 form-control" name="accion">
                <option></option>

                @if ( old('accion', $request['accion']) == 'C' )
                    <option value="C" selected>Creación</option>
                @else
                    <option value="C">Creación</option>
                @endif

                @if ( old('accion', $request['accion']) == 'M' )
                    <option value="M" selected>Modificación</option>
                @else
                    <option value="M">Modificación</option>
                @endif

                @if ( old('accion', $request['accion']) == 'B' )
                    <option value="B" selected>Borrado</option>
                @else
                    <option value="B">Borrado</option>
                @endif
            </select>
        </div>

        <div class="col-md-4 form-group">
            <label for="valor_anterior">Anterior</label>
            <input type="text" class="form-control" id="valor_anterior" name="valor_anterior" value="{{ old('valor_anterior', $request['valor_anterior']) }}">
            <small id="valor_anteriorHelp" class="form-text text-muted">Cuando se filtra por CREACIÓN, este campo se ignora.</small>
        </div>

        <div class="col-md-4 form-group">
            <label for="valor_nuevo">Nuevo</label>
            <input type="text" class="form-control" id="valor_nuevo" name="valor_nuevo" value="{{ old('valor_nuevo', $request['valor_nuevo']) }}">
            <small id="valor_nuevoHelp" class="form-text text-muted">Cuando se filtra por BORRADO, este campo se ignora.</small>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 form-group pr-0 pr-md-3">
            <label for="user_id">ID del usuario</label>
            <input type="number" class="form-control text-right" id="user_id" name="user_id" value="{{ old('user_id', $request['user_id']) }}" min="1">
        </div>

        <div class="col-md-5 form-group">
            <label for="desde">Desde</label>
            <input type="date" class="form-control @error('desde') is-invalid @enderror" min="2018-07-04" max="{{ now()->format('Y-m-d') }}" id="desde" name="desde" value="{{ old('desde', $request['desde']) }}">

            @error('desde')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-md-5 form-group">
            <label for="hasta">Hasta</label>
            <input type="date" class="form-control @error('hasta') is-invalid @enderror" min="2018-07-04" max="{{ now()->format('Y-m-d') }}" id="hasta" name="hasta" value="{{ old('hasta', $request['hasta']) }}">

            @error('hasta')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-auto">
            <button type="submit" class="btn btn-primary">
                Filtrar
            </button>
        </div>

        <div class="col-auto">
            <a href="{{ route('logs.todos') }}" class="btn btn-secondary">
                Borrar filtros
            </a>
        </div>
    </div>
</form>
