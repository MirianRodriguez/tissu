@extends('layouts.app')

@section('titulo', 'Crear tipo de material')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Crear tipo de material

                  </div>
                  <form class="card-body" action="{{route('tipos-materiales.crear')}}" method="POST">
                        @csrf
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                        <div class="form-group">
                            <label for="nombre">Nombre <span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('nombre') is-invalid @enderror" 
                            id="nombre" name="nombre" value="{{old('nombre')}}" maxlength="50">
                               
                            @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="unidad_medida_por_bulto"> Unidad de medida por bulto <span class="text-danger">*</span></label>
                                <select id="unidad_medida_por_bulto" class="select2 form-control @error('unidad_medida_por_bulto') is-invalid @enderror" name="unidad_medida_por_bulto">
                                        <option></option>

                                        @foreach ($unidades as $u)
                                        @if ( old('unidad_medida_por_bulto') == $u->id )
                                            <option value="{{ $u->nombre }}" selected>{{ $u->nombre }}</option>
                                        @else
                                            <option value="{{ $u->nombre }}">{{ $u->nombre }}</option>
                                        @endif
                                        @endforeach
                                </select>
                                
                                @error('unidad_medida_por_bulto')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="unidad_medida_en_stock"> Unidad de medida en stock <span class="text-danger">*</span></label>
                                <select id="unidad_medida_en_stock" class="select2 form-control @error('unidad_medida_en_stock') is-invalid @enderror" name="unidad_medida_en_stock">
                                        <option></option>

                                        @foreach ($unidades as $u)
                                        @if ( old('unidad_medida_en_stock') == $u->id )
                                            <option value="{{ $u->nombre }}" selected>{{ $u->nombre }}</option>
                                        @else
                                            <option value="{{ $u->nombre }}">{{ $u->nombre }}</option>
                                        @endif
                                        @endforeach
                                </select>
                                
                                @error('unidad_medida_en_stock')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                </span>
                                @enderror
                            </div>
                        </div> 
                        <div class="row">
                            <div class="form-group col">
                                <label for="proveedor_id"> Proveedor <span class="text-danger">*</span></label>
                                <select id="proveedor_id" class="select2 form-control @error('proveedor_id') is-invalid @enderror" name="proveedor_id">
                                    <option></option>
                                    @foreach ($proveedores as $p)
                                    @if ( old('proveedor_id') == $p->id )
                                        <option value="{{ $p->id }}" selected>{{ $p->nombre }}</option>
                                    @else
                                        <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                                    @endif
                                    @endforeach
                                </select>
                                @error('proveedor_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col">
                                    <label for="costo"> Costo <span class="text-danger">*</span></label>
                                    <input type="number" step="0.01" class="form-control @error('costo') is-invalid @enderror" 
                                    id="costo" name="costo" value="{{old('costo')}}" min="0"
                                    max="999999999">
                                    
                                    @error('costo')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                              </div>
                        </div>

                          <div class="d-flex justify-content-center mt-4 ">
                              <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                              <a href="{{route('tipos-materiales')}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>
                  </form>
          </div>

    </div>
@endsection
