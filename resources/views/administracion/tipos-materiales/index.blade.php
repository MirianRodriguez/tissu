@extends('layouts.app')

@section('titulo', 'Tipos de materiales')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            $('#tipos').DataTable({

                aaSorting: [], /*para que se muestre en el mismo orden que viene del controlador*/

                columnDefs: [
                    {
                        targets: 5,
                        className: 'dt-center',
                    }
                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
@endsection

@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Gestión de tipos de materiales</div>
            <div class="col-auto">
                <a href="{{route('tipos-materiales.crear')}}" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="bottom" title="Nuevo"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @include ('layouts.mensaje')
        <div class="container-fluid mt-3">
            <table id="tipos" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Unidad de medida por bulto</th>
                        <th>Unidad de medida en stock</th>
                        <th>Proveedor</th>
                        <th>Costo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tiposMateriales as $tm)
                    <tr>
                        <td>{{$tm->nombre}}</td>
                        <td>{{$tm->unidad_medida_por_bulto}}</td>
                        <td>{{$tm->unidad_medida_en_stock}}</td>
                        <td>{{$tm->proveedor->nombre}}</td>
                        <td>$ {{$tm->costo()}}</td>
                        <td>
                            <a href="{{route('tipos-materiales.editar', $tm->id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fas fa-pencil-alt fa-fw"></i></a>
                            <form class="d-inline" action="{{route('tipos-materiales.eliminar', $tm->id)}}" method="POST">
                                @csrf
                                @method ('DELETE')
                                <button type="submit" class="btn btn-danger m-1" data-toggle="tooltip" data-placement="bottom" title="Eliminar" onclick="return confirm ('¿Está seguro de que desea eliminar el registro?')">
                                <i class="fas fa-trash-alt fa-fw"></i>
                                </button>
                            </form>
                        </td>            
                    </tr>
                    @endforeach           
                </tbody>
            </table>
        </div>
    </div>
@endsection
