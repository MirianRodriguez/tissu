@extends('layouts.app')

@section('titulo', 'Crear tipo de producto')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Crear tipo de producto

                  </div>
                  <form class="card-body" action="{{route('tipos-productos.crear')}}" method="POST">
                        @csrf
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                        <div class="border-bottom">
                        <div class="form-group">
                            <label for="nombre">Nombre <span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('nombre') is-invalid @enderror" 
                            id="nombre" name="nombre" value="{{old('nombre')}}" maxlength="50">
                            
                            @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="descripcion">Descripción</label>
                            <textarea class="form-control @error('descripcion') is-invalid @enderror" 
                            id="descripcion" name="descripcion" rows="3" value="{{old('descripcion')}}" maxlength="250"></textarea>

                            @error('descripcion')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                        </div>

                        <div class="py-3">
                            <h6>Medidas requeridas <span class="text-danger">*</span></h6>
                        </div>

                        <div class="container-fluid">   
                            <select name="medidas[]" id="medidas[]" placeholder="Seleccione las medidas" class="select2 js-example-basic-multiple form-control @error('medidas') is-invalid @enderror" multiple="multiple" required>
                                <option></option>
                                @foreach ($medidas as $m)
                                    @if (is_array(old('medidas')))
                                        @if ( in_array($m->id, old('medidas')))
                                            <option value="{{ $m->id }}" selected>{{ $m->nombre}}</option>
                                        @else
                                            <option value="{{ $m->id }}">{{ $m->nombre }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $m->id }}">{{ $m->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('medidas')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="d-flex justify-content-center mt-4 ">
                            <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                            <a href="{{route('tipos-productos')}}" class="btn btn-secondary mx-2">Volver</a>
                        </div>
                  </form>
          </div>
    </div>
@endsection
