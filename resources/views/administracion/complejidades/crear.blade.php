@extends('layouts.app')

@section('titulo', 'Crear complejidad')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
        <div class="card mx-1 my-3 ">
            <div class="card-header">
                Crear tipo de complejidad
            </div>
            <form class="card-body" action="{{route('complejidades.crear')}}" method="POST">
                @csrf
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                <div class="form-group">
                    <label for="tipo">Tipo <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('tipo') is-invalid @enderror" 
                    id="tipo" name="tipo" value="{{old('tipo')}}" maxlength="50">
                    
                    @error('tipo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="porcentaje_ganancia">Porcentaje de ganancia<span class="text-danger">*</span></label>
                    <input type="number" class="form-control @error('porcentaje_ganancia') is-invalid @enderror" 
                    id="porcentaje_ganancia" name="porcentaje_ganancia" value="{{old('porcentaje_ganancia')}}"
                    max="500" min="0">
                        
                    @error('porcentaje_ganancia')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                    <a href="{{route('complejidades')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </form>
        </div>
    </div>
@endsection
