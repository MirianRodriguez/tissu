@extends('layouts.app')

@section('titulo', 'Ajuste de stock')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
        <div class="card mx-1 my-3 ">
            <div class="card-header">
                Ajuste de stock
            </div>
            <form class="card-body" action="{{route('ajustes.crear')}}" method="POST">
                @csrf
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                <div class="form-group">
                    <label for="material_id"> Material <span class="text-danger">*</span></label>
                    <select id="material_id" class="select2 form-control @error('material_id') is-invalid @enderror" name="material_id">
                        <option></option>
                        @foreach ($materiales as $m)
                        @if ( old('material_id') == $m->id )
                            <option value="{{ $m->id }}" selected>{{ $m->codigo }} {{ $m->tipo_material->nombre }} {{ $m->color->nombre }}</option>
                        @else
                            <option value="{{ $m->id }}">{{ $m->codigo }} {{ $m->tipo_material->nombre }} {{ $m->color->nombre }}</option>
                        @endif
                        @endforeach
                    </select>             
                    @error('material_id')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="cantidad"> Stock actual <span class="text-danger">*</span></label>
                    <input type="number" class="form-control @error('cantidad') is-invalid @enderror" 
                    id="cantidad" name="cantidad" value="{{old('cantidad')}}"
                    max="999999999999" min="1" step="1">   
                    @error('cantidad')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                    <a href="{{route('ajustes')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </form>
        </div>
    </div>
@endsection
