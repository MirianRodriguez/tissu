@extends('layouts.app')

@section('titulo', 'Editar tarea')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Editar tarea
                  </div>
                  <form class="card-body" action="{{route('tareas.editar', $tarea)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                        <div class="form-group">
                            <label for="nombre">Nombre <span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('nombre') is-invalid @enderror" 
                            id="nombre" name="nombre" value="{{old('nombre', $tarea->nombre)}}" maxlength="50">
                            
                            @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>

                    {{--    <div class="form-group ">
                            <label for="dificultad_id"> Dificultad de la tarea <span class="text-danger">*</span></label>
                            <select id="dificultad_id" class="select2 form-control @error('dificultad_id') is-invalid @enderror" name="dificultad_id">
                                <option></option>

                                @foreach ($dificultades_tareas as $dt)
                                @if ( old('dificultad_id',$tarea->dificultad_id) == $dt->id )
                                    <option value="{{ $dt->id }}" selected>{{ $dt->nombre }} / {{ $dt->tiempo_ejecucion }} Hs.</option>
                                @else
                                    <option value="{{ $dt->id }}">{{ $dt->nombre }} / {{ $dt->tiempo_ejecucion }} Hs.</option>
                                @endif
                                @endforeach
                            </select>
                            
                            @error('dificultad_id')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>--}}
                        <div class="d-flex justify-content-center mt-4 ">
                            <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                            <a href="{{route('tareas')}}" class="btn btn-secondary mx-2">Volver</a>
                        </div>
                  </form>
          </div>
    </div>
@endsection
