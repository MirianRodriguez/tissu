@extends('layouts.app')

@section('titulo', 'Alta usuario')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
        <div class="card mx-1 my-3 ">
            <div class="card-header">
                Alta usuario
            </div>
            <form class="card-body" action="{{route('usuarios.crear')}}" method="POST">
                @csrf
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                <div class="form-group">
                    <label for="name">Nombre de usuario <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" 
                    id="name" name="name" value="{{old('name')}}" maxlength="50">
                    
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Correo electrónico <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('email') is-invalid @enderror" 
                    id="email" name="email" value="{{old('email')}}" maxlength="50">
                    
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Contraseña <span class="text-danger">*</span></label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" 
                    id="password" name="password" value="{{old('password')}}" maxlength="50" minlength="8">
                    
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="rol_id"> Rol <span class="text-danger">*</span></label>
                    <select id="rol_id" class="select2 form-control @error('rol_id') is-invalid @enderror" name="rol_id">
                        <option></option>
                        @foreach ($roles as $r)
                        @if ( old('rol_id') == $r->id )
                            <option value="{{ $r->id }}" selected>{{ $r->nombre }}</option>
                        @else
                            <option value="{{ $r->id }}">{{ $r->nombre }}</option>
                        @endif
                        @endforeach
                    </select>             
                    @error('rol_id')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>             

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                    <a href="{{route('usuarios')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </form>
        </div>
    </div>
@endsection
