@extends('layouts.app')

@section('titulo', 'Editar dificultad')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
        <div class="card mx-1 my-3 ">
            <div class="card-header">
                Editar dificultad de tarea
            </div>
            <form class="card-body" action="{{route('dificultades-tareas.editar', $dificultadTarea)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                <div class="form-group">
                    <label for="nombre">Nombre <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('nombre') is-invalid @enderror" 
                    id="nombre" name="nombre" value="{{old('nombre', $dificultadTarea->nombre)}}" maxlength="50">
                    
                    @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tiempo_ejecucion">Tiempo estimado de ejecución <span class="text-danger">*</span></label>
                    <input type="number" class="form-control @error('tiempo_ejecucion') is-invalid @enderror" 
                    id="tiempo_ejecucion" name="tiempo_ejecucion" value="{{old('tiempo_ejecucion', $dificultadTarea->tiempo_ejecucion)}}"
                    max="500" min="0" step="0.5">
                        
                    @error('tiempo_ejecucion')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                    <a href="{{route('dificultades-tareas')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </form>
        </div>
    </div>
@endsection
