@extends('layouts.app')

@section('titulo', 'Crear estado')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
        <div class="card mx-1 my-3 ">
            <div class="card-header">
                Crear estado de tarea
            </div>
            <form class="card-body" action="{{route('estados-tareas.crear')}}" method="POST">
                @csrf
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                <div class="form-group">
                    <label for="nombre"> Nombre <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('nombre') is-invalid @enderror" 
                    id="nombre" name="nombre" value="{{old('nombre')}}" maxlength="50">
                    
                    @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                    <a href="{{route('estados-tareas')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </form>
        </div>
    </div>
@endsection
