@extends('layouts.app')

@section('titulo', 'Cargar compra')

@section('contenido')

    <div class="container p-0">
        <form class="card mx-1 my-3" action="{{route('compras.cargar')}}" method="POST">
            @csrf

            <div class="card-header">
                Cargar compra
            </div>

            <div class="card-body">
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                <div class="container-fluid">
                    <div class="row border-bottom">
                        <div class="form-group col-md-3">
                            <label for="fecha"> Fecha <span class="text-danger">*</span></label>
                            <input type="date" class="form-control @error('fecha') is-invalid @enderror" 
                            id="fecha" name="fecha" value="{{old('fecha')}}" max="{{now()->format('Y-m-d')}}">

                            @error('fecha')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-4">
                            <label for="nro_comprobante"> Nro. de comprobante <span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('nro_comprobante') is-invalid @enderror" 
                            id="nro_comprobante" name="nro_comprobante" value="{{old('nro_comprobante')}}" pattern="[0-9]{4}[-][0-9]{8}" 
                            placeholder="0000-00000000" maxlength="13" minlength="13">
                                
                            @error('nro_comprobante')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>  
                        <div class="form-group col-md-5">
                            <label for="proveedor_id"> Proveedor <span class="text-danger">*</span></label>
                            <select id="proveedor_id" class="select2 form-control @error('proveedor_id') is-invalid @enderror" name="proveedor_id">
                                <option></option>
                                @foreach ($proveedores as $p)
                                @if ( old('proveedor_id') == $p->id )
                                    <option value="{{ $p->id }}" selected>{{ $p->nombre }}</option>
                                @else
                                    <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                                @endif
                                @endforeach
                            </select>
                            @error('proveedor_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="py-3">
                        <h6 class="@error('cantidades') is-invalid @enderror">Detalle de compra</h6>
                        @error('cantidades')
                        <span id="error" class="invalid-feedback" role="alert">
                            <strong>Debe cargar al menos un detalle de compra.</strong>
                        </span>
                        @enderror

                    </div>
                </div>


                <div class="container-fluid">
                    <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
                        <thead style="background-color: #feeae6;">
                            <th style="width:30%">Cantidad <span class="text-danger">*</span></th>
                            <th style="width:30%">Material <span class="text-danger">*</span></th>
                            <th style="width:30%">Precio unitario <span class="text-danger">*</span></th>
                            <th style="width:10%" class="justify-content-center">Acciones</th>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
                                    <input type="number" class="form-control @error('cantidad') is-invalid @enderror" 
                                    id="cantidad" name="cantidad" value="{{old('cantidad')}}"
                                    max="999999999999" min="0.01" step="0.01">
                                        
                                    @error('cantidad')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                                </td>
                                <td>
                                    <select id="material_id" class="select2 form-control @error('material_id') is-invalid @enderror" name="material_id">
                                        <option></option>
                                        @foreach ($materiales as $m)
                                        @if ( old('material_id') == $m->id )
                                            <option value="{{ $m->id }}" selected>{{$m->unidad_medida_por_bulto}} {{$m->tipo}} {{$m->color}} ({{ $m->codigo}})</option>
                                        @else
                                            <option value="{{ $m->id }}">{{$m->unidad_medida_por_bulto}} {{$m->tipo}} {{$m->color}} ({{ $m->codigo}})</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('material_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                                </td>
                                <td>
                                    <input type="number" step="0.01" class="form-control @error('costo') is-invalid @enderror" 
                                    id="costo" name="costo" value="{{old('costo')}}"
                                    max="999999999999" min="0.01">
                                        
                                    @error('costo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                                </td>
                                <td>
                                    <button type="button" id="btn_add" class="btn btn-primary"><i class="fas fa-plus fa-fw"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <th>TOTAL</th>
                                <th></th>
                                <th></th>
                                <th><h4 id="total">$ 0,00</h4></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-center" id="guardar">
                <button type="submit" class="btn btn-primary mx-2">Guardar</button>
                <a href="{{route('compras')}}" class="btn btn-secondary mx-2">Volver</a>
            </div>
        </form>
    </div>
@endsection
@section('js')
<script>
        $(document).ready(function(){
            $("#btn_add").click(function(){
            agregar();
            $("#error").remove();
            });
            $('#nro_comprobante').mask('0000-00000000');
        });

        var i=0;
        total=0;

        function agregar(){
            cantidad=$("#cantidad").val();
            material=$("#material_id option:selected").text();
            material_id=$("#material_id option:selected").val();
            costo=$("#costo").val();

            if (cantidad!="" && material!="" && costo!="" && cantidad>0 && costo>0 && !esta(material_id)){
                var fila = "<tr class='selected' id='fila"+i+"'><td><input type='hidden' name='cantidades[]' id='cantidades[]' value="+cantidad+">"+cantidad+"</td><td><input type='hidden' name='materiales[]' value="+material_id+">"+material+"</td><td><input type='hidden' name='costos[]' id='costos[]' value="+costo+">"+new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(costo)+"</td><td><button type='button' class='btn btn-danger' onclick='eliminar("+i+");'><i class='fas fa-trash-alt fa-fw'></i></button></td></tr>";
                i++;
                limpiar(); 
                $('#detalles').append(fila);
                calcular();
            }else{
                alert("No se puede agregar el detalle, revise los datos cargados.");
            }
        }

        function limpiar(){
            $("#cantidad").val("");
            $("#material_id").val(null).trigger('change');
            $("#costo").val("");
        }

        function eliminar(index){
            $("#fila"+index).remove();
            calcular();
        }

        function calcular(){
            var cantidades=document.getElementsByName('cantidades[]');
            var costos=document.getElementsByName('costos[]');
            total=0;
            for (var j=0; j<cantidades.length; j++){
                total+= cantidades[j].attributes[3].nodeValue * costos[j].attributes[3].nodeValue;
            } 
            $("#total").html(new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(total));   
        }

        function esta(material) {       
            var retorno = false;
            var materiales = document.getElementsByName('materiales[]'); 
            for (var i = 0; i < materiales.length; i++) { 
                if(materiales[i].value == material){
                    retorno = true;
                }
            } 
            return retorno;
        } 
    </script>
@endsection
