@extends('layouts.app')

@section('titulo', 'Compras')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            $('#compras').DataTable({

                aaSorting: [], /*para que se muestre en el mismo orden que viene del controlador*/

                columnDefs: [
                    {
                        targets: 5,
                        className: 'dt-center',
                    },
                    {
                        targets: 4,
                        className: 'dt-right',
                    }
                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
@endsection

@section('contenido')
    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Gestión de compras</div>
            <div class="col-auto">
                <a href="{{route('compras.cargar')}}" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="bottom" title="Nuevo"><i class="fas fa-plus"></i></a>
            </div>
        </div>   
    </div>

    {{--Filtros--}}

    <div ad="filtro" class="container border mx-auto p-3 m-3 bg-light">
        <form action="{{route('compras')}}" method="POST">
            @csrf
            <div class="row">
                <div class="form-group col">
                    <label for="desde"> Desde</label>
                    <input type="date" class="form-control @error('desde') is-invalid @enderror" 
                    id="desde" name="desde" value="{{old('desde',$desde)}}" max="{{now()->format('Y-m-d')}}">

                    @error('desde')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col">
                    <label for="hasta"> Hasta </label>
                    <input type="date" class="form-control @error('hasta') is-invalid @enderror" 
                    id="hasta" name="hasta" value="{{old('hasta', $hasta)}}" max="{{now()->format('Y-m-d')}}">

                    @error('hasta')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col">
                    <label for="proveedor_id"> Proveedor </label>
                    <select id="proveedor_id" class="select2 form-control" name="proveedor_id">
                        <option></option>
                        @foreach ($proveedores as $p)
                        @if ( old('proveedor_id', $proveedor) == $p->id )
                            <option value="{{ $p->id }}" selected>{{ $p->nombre }}</option>
                        @else
                            <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-auto pt-4">
                    <button name="filtrar" id="filtrar" type="submit" class="btn btn-success mx-2" data-toggle="tooltip" data-placement="bottom" title="Filtrar" ><i class="fas fa-filter"></i></button>
                    <button name="descargar" id="descargar" type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Exportar" ><i class="far fa-file-pdf"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid">
        @include ('layouts.mensaje')
        <div class="container-fluid mt-3">
            <table id="compras" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Nro. de registro</th>
                        <th>Fecha</th>
                        <th>Nro. de comprobante</th>
                        <th>Proveedor</th>
                        <th>Total</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($compras as $c)
                    <tr>
                        <td>{{$c->id}}</td>
                        <td>
                            <span class="d-none">{{$c->fecha}}</span>
                            {{$c->fecha()}}
                        </td>
                        <td>{{$c->nro_comprobante}}</td>
                        <td>{{$c->proveedor->nombre}}</td>
                        <td>$ {{$c->total()}}</td>  
                        <td>
                            <span data-toggle="tooltip" data-placement="bottom" title="Detalle">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalle{{$c->id}}">
                                <i class="fas fa-eye"></i>
                            </button>
                            </span>
                            <div class="modal fade" id="detalle{{$c->id}}" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Detalle de compra</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-fluid mt-3">
                                            <table id="detalles" class="display responsive nowrap w-100">
                                                <thead>
                                                    <tr>
                                                        <th>Cantidad</th>
                                                        <th>Material</th>
                                                        <th>Precio Unitario</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($c->detalles_compra as $dc)
                                                    <tr>
                                                        <td>{{$dc->cantidad}} {{$dc->material->tipo_material->unidad_medida_por_bulto}}</td>
                                                        <td>{{$dc->material->tipo_material->nombre}} {{$dc->material->color->nombre}} ({{$dc->material->codigo}})</td>
                                                        <td>$ {{$dc->precio_unitario()}}</td>  
                                                    </tr>
                                                    @endforeach           
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </td> 
                    </tr>
                    @endforeach           
                </tbody>
            </table>
        </div>
    </div>
@endsection



