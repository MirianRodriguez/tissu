@extends('layouts.app')

@section('titulo', 'Detalles de pedido')

@section('contenido')

<div class="container">
    @include('layouts.mensaje')
    <div class="card my-3">
        <div class="card-header">
            Pedido {{$pedido->id}} 
        </div>
        <div class="card-body">
            <h5 class="card-title">Cliente: {{$pedido->cliente->apellidos. ', '. $pedido->cliente->nombres}}</h5>
            <p>{{$pedido->comentario}}</p>
            <p class="card-text">Pedido para: {{$pedido->fecha_pactada()}}</p>
            <p class="card-text">Presupuesto: ${{$pedido->presupuesto()}}</p>
        </div>
        <div class="card-footer">
            @foreach($pedido->confecciones as $i => $c)
                @if (count($c->medidas) > 0 && count($c->materiales) >0)
                    <i class="fas fa-check-circle text-success mr-1"></i>
                @else
                    <i class="fas fa-times-circle text-danger mr-1"></i>
                @endif
                <a href="#{{$c->id}}">{{($i+1). ' - '. $c->producto->nombre}} 
                @if(!is_null($c->comentario))
                   - {{$c->comentario}}
                @endif
                </a><br>
            @endforeach
        </div>
    </div>
    @foreach($pedido->confecciones as $i => $c)
        <a id="{{$c->id}}">
            <div class="card mb-3">
                <div class="card-header">
                {{($i+1). ' - '. $c->producto->nombre}} 
                @if(!is_null($c->comentario))
                   - {{$c->comentario}}
                @endif
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <p>Materiales</p>                           
                            <ul>
                                @foreach($c->materiales as $m)
                                    <li>{{$m->tipo_material->nombre}} {{$m->color->nombre}} {{$m->descripcion}}</li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-8 container-fluid">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Medida</th>
                                        <th scope="col">Valor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($c->medidas as $m)
                                    <tr>
                                        <td>{{$m->medida_requerida->nombre}}</td>
                                        <td>{{$m->valor}}</</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    @if(count($c->materiales)==0)
                        <a href="{{route('confecciones.definir-material', $c->id)}}" class="btn btn-success mr-1" data-toggle="tooltip" data-placement="bottom" title="Materiales"><i class="fas fa-puzzle-piece fa-fw"></i></a>
                    @else
                        <a href="{{route('confecciones.definir-material', $c->id)}}" class="btn btn-success mr-1 disabled"><i class="fas fa-puzzle-piece fa-fw"></i></a>
                    @endif
                    @if (count($c->asignaciones())==0)
                        <a href="{{route('confecciones.cargar-medidas',$c->id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Medidas"><i class="fas fa-ruler fa-fw"></i></a>
                    @else
                        <a href="{{route('confecciones.cargar-medidas',$c->id)}}" class="btn btn-primary disabled"><i class="fas fa-ruler fa-fw"></i></a>
                    @endif
                    <a href="#"><i class="fas fa-chevron-circle-up fa-fw"></i></a>
                </div>
            </div>
        </a>
    @endforeach
</div>

@endsection