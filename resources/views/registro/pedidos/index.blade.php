@extends('layouts.app')

@section('titulo', 'Pedidos')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            $('#pedidos').DataTable({

                aaSorting: [], /*para que se muestre en el mismo orden que viene del controlador*/

                columnDefs: [
                    {
                        targets: 5,
                        className: 'dt-center',
                    },
                    {
                        targets: [3,4],
                        className: 'dt-right',
                    }
                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });

        function cambiar(toggle){
            
            var status = toggle.checked ? 1 : 0;
            var producto = toggle.id; 
            
            $.ajax({
                type: "GET",
                dataType: "json",
                url: `/administracion/productos/habilitar/${producto}/${status}`,
                data: {},
                success: function(data){
                alert(data.success)
                }
            });
        }

        $(function() {
        $('.custom-control-input').change(function() {
    })
  })

    </script>
@endsection

@section('contenido')
    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Gestión de pedidos</div>
            <div class="col-auto">
                <a href="{{route('pedidos.crear')}}" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="bottom" title="Nuevo"><i class="fas fa-plus"></i></a>
            </div>
        </div>   
    </div>
    <div class="container-fluid">
        @include ('layouts.mensaje')
        <div class="container-fluid mt-3">
            <table id="pedidos" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Nro.</th>
                        <th>Cliente</th>
                        <th>Fecha de entrega</th>
                        <th>Presupuesto</th>
                        <th>Estado de avance</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pedidos as $p)
                    <tr>
                        <td>
                        {{$p->id}}
                        </td>
                        <td>{{$p->cliente->apellidos}}, {{$p->cliente->nombres}}</td>
                        <td>{{$p->fecha_pactada()}}</td>
                        <td>${{$p->presupuesto()}}</td>
                        <td>
                            @if($p->porcentaje_avance()==100)
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: {{$p->porcentaje_avance()}}%" aria-valuenow="{{$p->porcentaje_avance()}}" aria-valuemin="0" aria-valuemax="100">{{$p->porcentaje_avance()}}%</div>
                                </div>
                            @else
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: {{$p->porcentaje_avance()}}%" aria-valuenow="{{$p->porcentaje_avance()}}" aria-valuemin="0" aria-valuemax="100">{{$p->porcentaje_avance()}}%</div>
                                </div>
                            @endif
                        </td>
                        <td>
                            <div class="row ml-2" style="width: 250px;">
                                <a href="{{route('pedidos.descargar', $p->id)}}" class="btn btn-danger m-1" data-toggle="tooltip" data-placement="bottom" title="Comprobante"><i class="far fa-file-pdf fa-fw"></i></a>
                                <a href="{{route('pedidos.ver-detalles', $p->id)}}" class="btn btn-primary m-1" data-toggle="tooltip" data-placement="bottom" title="Detalles de confecciones"><i class="far fa-clipboard fa-fw"></i></a>
                                <span data-toggle="tooltip" data-placement="bottom" title="Cronograma de pagos"><button type="button" class="btn btn-warning m-1" data-toggle="modal" data-target="#cronograma{{$p->id}}">
                                    <i class="fas fa-calendar-alt fa-fw"></i>
                                </button>
                                </span>
                                @if($p->entregado)
                                    <span class="badge badge-info d-inline mx-1 my-auto">Entregado</span>
                                @elseif($p->porcentaje_avance == 100)
                                    @if($p->esta_cancelado())
                                        <a href="{{route('pedidos.entregar', $p->id)}}" class="btn btn-success m-1">
                                        Entregar
                                        </a>
                                    @else
                                        <span data-toggle="tooltip" data-placement="bottom" title="Pagos pendientes" style="cursor: not-allowed;">
                                            <a class="btn btn-success m-1 disabled">
                                            Entregar
                                            </a>
                                        </span>
                                    @endif
                                @endif                              
                                <div class="modal fade" id="cronograma{{$p->id}}" tabindex="-1" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Cronograma de pagos</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid mt-3">
                                                <table id="cronograma" class="display responsive nowrap w-100">
                                                    <thead>
                                                        <tr>
                                                            <th>Fecha</th>
                                                            <th class="text-right">Monto</th>
                                                            <th>Estado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($p->pagos_requeridos as $pr)
                                                        <tr>
                                                            <td>{{$pr->fecha()}}</td>
                                                            <td class="text-right">$ {{$pr->monto()}}</td>
                                                           
                                                            <td>
                                                                @if($pr->esta_pagado)
                                                                    <span class="badge badge-success">Pagado</span>
                                                                @else
                                                                    @if($pr->fecha <= today('America/Argentina/Buenos_Aires')->format('Y-m-d'))
                                                                        <span class="badge badge-danger">Atrasado</span>
                                                                    @else
                                                                        <span class="badge badge-warning">Pendiente</span>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endforeach           
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                                   
                        </td> 
                    </tr>
                    @endforeach           
                </tbody>
            </table>
        </div>
    </div>
@endsection
