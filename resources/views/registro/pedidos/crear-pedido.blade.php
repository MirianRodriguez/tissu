@extends('layouts.app')

@section('titulo', 'Cargar pedido')

@section('contenido')


<div class="container col-9 p-0">
    @include('layouts.mensaje')
    <form class="card mx-1 my-3" action="{{route('pedidos.crear')}}" method="POST">
        @csrf
        <div class="card-header">
            Cargar pedido
        </div>
        <div class="card-body">
            <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
            <div class="container-fluid">         
                <div class="row">
                    <div class="form-group col">
                        <label for="cliente_id"> Cliente <span class="text-danger">*</span></label>
                        <select id="cliente_id" class="select2 form-control @error('cliente_id') is-invalid @enderror" name="cliente_id">
                            <option></option>
                            @foreach ($clientes as $c)
                            @if ( old('cliente_id') == $c->id )
                                <option value="{{ $c->id }}" selected>{{ $c->apellidos }}, {{ $c->nombres }}</option>
                            @else
                                <option value="{{ $c->id }}">{{ $c->apellidos }}, {{ $c->nombres }}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('cliente_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>

                </div>
                <div class="row border-bottom">
                    <div class="form-group col">
                        <label for="comentario">Notas particulares</label>
                        <textarea class="form-control @error('comentario') is-invalid @enderror" 
                        id="comentario" name="comentario" rows="3" maxlength="250">{{old('comentario')}}</textarea>
                        @error('comentario')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                
                <div class="py-3">
                    <h6 class="@error('cantidades') is-invalid @enderror">Detalle del pedido</h6>
                    @error('cantidades')
                    <span id="error" class="invalid-feedback" role="alert">
                        <strong>Debe cargar al menos un producto.</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="container-fluid">
                <table id="productos" class="table table-striped table-bordered table-condensed table-hover">
                    <thead style="background-color: #feeae6;">
                        <th style="width:30%">Cantidad <span class="text-danger">*</span></th>
                        <th style="width:30%">Producto <span class="text-danger">*</span></th>
                        <th style="width:10%" class="justify-content-center">Acciones</th>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <input type="number" class="form-control" 
                                id="cantidad" name="cantidad" value="{{old('cantidad')}}" min="1"
                                max="999999999999" >
                            </td>
                            <td>
                                <select id="producto_id" class="select2 form-control" name="producto_id">
                                    <option></option>
                                    @foreach ($productos as $p)
                                        @if ( old('producto_id') == $p->id )
                                            <option value="{{ $p->id }}" selected> {{ $p->nombre}} </option>
                                        @else
                                            <option value="{{ $p->id }}"> {{ $p->nombre }} </option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <button type="button" id="btn_add" class="btn btn-primary"><i class="fas fa-plus fa-fw"></i></button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="container-fluid">
                <div id="alerta" class="row">
                </div>
                <div id="datos" class="row">
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
            <button id="FP" type="button" class="btn btn-success mx-2" onclick="generar();">Ver fecha de entrega y presupuesto</button>
            <button id="guardar" type="submit" class="btn btn-primary mx-2">Guardar</button>
            <a href="{{route('pedidos')}}" class="btn btn-secondary mx-2">Volver</a>
        </div>
    </form>
</div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script>
        $(document).ready(function(){
            $("#guardar").prop('disabled', true);
            $("#FP").prop('disabled', true);
            $("#btn_add").click(function(){
            agregar();
            $("#error").remove();
            });
        });

        var i=0;

        function agregar(){
            cantidad=$("#cantidad").val();
            producto=$("#producto_id option:selected").text();
            producto_id=$("#producto_id option:selected").val();

            if (cantidad!="" && producto!="" && cantidad>0 && !esta(producto_id)){
                var fila = "<tr class='selected' id='fila"+i+"'><td><input type='hidden' name='cantidades[]' id='cantidades[]' value="+cantidad+">"+cantidad+"</td><td><input type='hidden' name='productos[]' value="+producto_id+">"+producto+"</td><td><button type='button' class='btn btn-danger' onclick='eliminar("+i+");'><i class='fas fa-trash-alt fa-fw'></i></button></td></tr>";
                i++;
                limpiar(); 
                $('#productos').append(fila);
                $("#FP").prop('disabled', false);
            }else{
                alert("No se puede agregar el detalle, revise los datos cargados.");
            }
        }

        function limpiar(){
            $("#cantidad").val("");
            $("#producto_id").val(null).trigger('change');
        }

        function eliminar(index){
            $("#fila"+index).remove();
        }

        function esta(producto) {       
            var retorno = false;
            var productos = document.getElementsByName('productos[]'); 
            for (var i = 0; i < productos.length; i++) { 
                if(productos[i].value == producto){
                    retorno = true;
                }
            } 
            return retorno;
        } 

        function generar(){
            token = document.getElementsByName('_token')[0].value;
            /* crea un string con los id de productos */
            var productosp='';
            $('input[name=productos\\[\\]]').each(function(index, element) {
                productosp=productosp+this.value+',';
            });
            /* crea un string con las cantidades */
            var cantidadesp='';
            $('input[name=cantidades\\[\\]]').each(function(index, element) {
                cantidadesp=cantidadesp+this.value+',';
            });
            /* saca las comas que sobran al final de string */
            productosp=productosp.slice(0,productosp.length-1);
            cantidadesp=cantidadesp.slice(0,cantidadesp.length-1);
            /* crea un array a partir de cada cadena */
            var cantidades = cantidadesp.split(',');
            var productos = productosp.split(',');

            $.ajax({
                type: "POST",
                dataType: "json",
                url:"{{ route('pedidos.generar') }}",
                data: {'_token' : token, cantidades : cantidades, productos : productos},
                success: function(data){
                    var fechaEntrega = data.fechaEntrega.slice(0,10);
                    var fechaDolar = new Date(data.fechaDolar);
                    var datos = "<div class='form-group col-6'>"+
                    "<label> Fecha de entrega <span class='text-danger'>*</span></label>"+
                    "<input type='date' class= 'form-control' name='fecha_pactada' id='fecha_pactada' value="+fechaEntrega+" min="+fechaEntrega+"></div>"+
                    "<div class='form-group col-6'><label> Presupuesto $ <span class='text-danger'>*</span></label><input type='number' step='0.01' class='form-control col' name='presupuesto' value="+data.presupuesto+" min='1'></div>";
                    var info = "<div class='col-12 alert alert-info' role='alert'>Fecha de entrega y presupuesto generados automáticamente. <br> Presupuesto en base a la cotización del dólar del día: "+fechaDolar.toLocaleDateString('es-AR')+".</div>";
                    $('#alerta').append(info);
                    $('#datos').append(datos);
                    $('#FP').prop('disabled', true);
                    $('#guardar').prop('disabled', false);
                    $('.btn-danger').prop('disabled', true);
                    $('#btn_add').prop('disabled', true);

                }
            });
        }

    </script>
@endsection
