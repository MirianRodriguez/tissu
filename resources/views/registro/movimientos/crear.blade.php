@extends('layouts.app')

@section('titulo', 'Registrar movimiento')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Registrar movimiento

                  </div>
                  <form class="card-body" action="{{route('movimientos.crear')}}" method="POST">
                        @csrf
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                        <div class="form-group">
                            <label for="cliente_id"> Cliente <span class="text-danger">*</span></label>
                            <select id="cliente_id" class="select2 form-control @error('cliente_id') is-invalid @enderror" name="cliente_id">
                                <option></option>
                                @foreach ($clientes as $c)
                                @if ( old('cliente_id') == $c->id )
                                    <option value="{{ $c->id }}" selected>{{ $c->apellidos }}, {{ $c->nombres }}</option>
                                @else
                                    <option value="{{ $c->id }}">{{ $c->apellidos }}, {{ $c->nombres }}</option>
                                @endif
                                @endforeach
                            </select>
                            @error('cliente_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                        <div id="saldo">
                        </div>
                        <div class="form-group">   
                            <label for="descripcion">Pago/s a cubrir <span class="text-danger">*</span></label>
                            <select name="pagos[]" id="pagos" class="select2 js-example-basic-multiple form-control @error('pagos') is-invalid @enderror" multiple="multiple" required disabled>
                                <option></option>
                            </select>
                            @error('pagos')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="monto"> Monto <span class="text-danger">*</span></label>
                            <input type="number" step="0.01" class="form-control @error('monto') is-invalid @enderror" 
                            id="monto" name="monto" value="{{old('monto')}}"
                            max="999999999999" min="0.00">
                                
                            @error('monto')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripción</label>
                            <textarea class="form-control @error('descripcion') is-invalid @enderror" 
                            id="descripcion" name="descripcion" rows="3" maxlength="250">{{old('descripcion')}}</textarea>
                            @error('descripcion')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                        
                        <div class="d-flex justify-content-center mt-4 ">
                            <button id="guardar" type="submit" class="btn btn-primary mx-2" onclick="validar();">Guardar</button>
                            <a href="{{route('movimientos')}}" class="btn btn-secondary mx-2">Volver</a>
                        </div>
                  </form>
          </div>

    </div>
@endsection
@section('js')
<script>
$('#pagos').on('select2:select', function (e) {
    calculo();
});
$('#pagos').on('select2:unselect', function (e) {
    calculo();
});
function calculo(){
    pagos=$('#pagos').select2('data');
    suma = 0;
    $(datos.retorno).each(function(i,v){
        if(pagos.find( a => a.id == v.id) != undefined){
            suma += v.monto;
        }
    })

    if (suma > datos.saldoCliente){
        suma -= datos.saldoCliente;
    }

    document.getElementById("monto").value = suma.toFixed(2).replace('.', ',');
}
$(document).ready(function(){
    $("#cliente_id").change(function(){
        token = document.getElementsByName('_token')[0].value;
        clienteId = $("#cliente_id option:selected").val();
        $.ajax({
                type: "POST",
                dataType: "json",
                url:"{{ route('movimientos.cargarPagos') }}",
                data: {'_token' : token, clienteId : clienteId},
                success: function(data){
                    datos = data;
                    deuda = 0;
                    $('#pagos').val(null).trigger('change');
                    $('#pagos').empty();
                    $(data.retorno).each(function(i, v){ // indice, valor
                        var newOption = new Option(v.texto, v.id, false, false);
                        $('#pagos').append(newOption).trigger('change');
                        $('#pagos').prop("disabled", false);
                        deuda += v.monto;
                    });
                    var saldo = "<div id= 'old' class='col-12 alert alert-info' role='alert'> Saldo del cliente: "+ new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(data.saldoCliente) +". <br> Deuda total: "+ new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(deuda) +".</div>";
                    $('#old').remove();
                    $('#saldo').append(saldo);
                }
            });
    })   
})
function validar(){
    if(validarPagos()){
        validarMonto();
    }    
}
function validarMonto(){
    monto = 1 * $('#monto').val();
    suma = 0;
    pagos=$('#pagos').select2('data');
    if (pagos.length > 0 && monto>=0){
        $(datos.retorno).each(function(i,v){
            if(pagos.find( a => a.id == v.id) != undefined){
                suma += v.monto;
            }
        })

        if(suma > 1 * (monto + datos.saldoCliente).toFixed(2) ){
            event.preventDefault();
            alert('El monto ingresado no es suficiente para cubrir los pagos seleccionados. Revise los datos e intente nuevamente.');
        }
    }else{
        event.preventDefault();
        alert('Debe ingresar un monto y seleccionar el/los pago/s a cubrir.');
    }
}
function validarPagos(){
    pagos=$('#pagos').select2('data');
    retorno = true;
    if (pagos.length > 0){
        $(pagos).each(function(i,v){
            p = datos.retorno.find( a => a.id == v.id);
            if(p != undefined){
                for (let v of anteriores(p)){
                    if (!estaSeleccionado(v)){
                        event.preventDefault();
                        alert('Uno de los pagos seleccionados corresponde a un pedido que tiene pagos anteriores sin cubrir.');
                        retorno = false;
                        break;
                    }
                }
            }
        })
    } 
    return retorno;
}
function estaSeleccionado(pago){
    pagos=$('#pagos').select2('data');
    return pagos.find( x => x.id == pago.id) != undefined;
}
function anteriores(pago){
    retorno = [];
    $(datos.retorno).each(function(i,v){
        if(v.pedido_id == pago.pedido_id && v.fecha < pago.fecha){
            retorno.push(v);
        }
    })
    return retorno;
}
</script>
@endsection
