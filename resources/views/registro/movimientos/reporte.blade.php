<!DOCTYPE html>
<html>
<head>
    <style>
        @page {
            margin: 0cm 0cm;
            font-family: Arial;
        }

        body {
            margin: 3cm 2cm 2cm;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2.5cm;
            background-color: #fedbd0;
            color: #a54a58;
            text-align: right;
            line-height: 30px;
            padding-right: 2cm;
            padding-top:1cm;
            font-family: "Hatton";
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 3cm;
            color: black;
            text-align: center;
            line-height: 35px;
        }

        thead{
            background-color: #fedbd0;
        }

        main{
            margin-top: 2cm;
        }

        table {
            table-layout: fixed;
            width: 100%;
            border-collapse: collapse;
        }

        thead th:nth-child(1) {
            width: 25%;
        }

        thead th:nth-child(2) {
            width: 25%;
        }

        thead th:nth-child(3) {
            width: 25%;
        }

        thead th:nth-child(4) {
            width: 25%;
        }

        th, td {
            padding: 20px;
            border-collapse: collapse;
        } 
        
        table td:nth-child(4) {
            text-align: right;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
    <body>
        <header>
            <div class="container-fluid">
                <img  
                    src="http://localhost/tissu/public/images/entretelas.png"
                    alt="Logo"
                    width="200px"
                >    
            </div>
        </header>

        <main>
            <table>
                <tbody>
                    <tr>
                        <td style="text-align: center; font-weight: bold; font-size: 16pt;">
                            Comprobante de pago
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            {{$movimiento->fechaTextual()}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i>Recibimos de</i>
                            <b>{{$movimiento->cliente->apellidos}}, {{$movimiento->cliente->nombres}}</b>
                            <i>la cantidad de</i>
                            <b>$&nbsp;{{$movimiento->monto()}}</b>
                            <i>en concepto de pago por confecciones.</i>
                            <br><br>
                            <i>El saldo de su cuenta es</i> <b>$&nbsp;{{$movimiento->cliente->saldo()}}</b>
                        </td>
                    </tr>
                </tbody>         
            </table>
        </main>
        <script type="text/php">
            if (isset($pdf)) {
                $text = "página {PAGE_NUM} de {PAGE_COUNT}";
                $size = 10;
                $font = $fontMetrics->getFont("Arial");
                $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                $x = ($pdf->get_width() - $width);
                $y = $pdf->get_height() - 35;
                $pdf->page_text($x, $y, $text, $font, $size);
                $pdf->page_text(40, $y, "Generado por: ".Auth::user()->name." - ".now()->format('d/m/Y H:i:s'), $font, $size);
            }
        </script>
    </body>
</html>
