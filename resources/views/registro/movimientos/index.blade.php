@extends('layouts.app')

@section('titulo', 'Pagos')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            $('#movimientos').DataTable({

                aaSorting: [], 

                columnDefs: [
                    {
                        targets: 5,
                        className: 'dt-center',
                    },{
                        targets: 2,
                        className: 'dt-right',
                    }
                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
@endsection

@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Gestión de pagos</div>
            <div class="col-auto">
                <a href="{{route('movimientos.crear')}}" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="bottom" title="Nuevo"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @include ('layouts.mensaje')
        <div class="container-fluid mt-3">
            <table id="movimientos" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Nro.</th>
                        <th>Fecha</th>
                        <th>Monto</th>
                        <th>Cliente</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($movimientos as $m)
                    <tr>
                        <td>{{$m->id}}</td>
                        <td>{{$m->fecha()}}</td>
                        <td>${{$m->monto()}}</td> 
                        <td>{{$m->cliente->apellidos}}, {{$m->cliente->nombres}}</td>
                        <td>{{$m->descripcion}}</td> 
                        <td> 
                            <a href="{{route('movimientos.descargar', $m->id)}}" class="btn btn-danger m-1" data-toggle="tooltip" data-placement="bottom" title="Exportar"><i class="far fa-file-pdf fa-fw"></i></a>
                        </td>          
                    </tr>
                    @endforeach           
                </tbody>
            </table>
        </div>
    </div>
@endsection
