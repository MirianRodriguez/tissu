@extends('layouts.app')

@section('titulo', 'Materiales')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            $('#materiales').DataTable({

                aaSorting: [], /*para que se muestre en el mismo orden que viene del controlador*/

                columnDefs: [
                    {
                        targets: 8,
                        className: 'dt-center',
                    },

                    {
                        targets: [3,4,5],
                        className: 'dt-right',
                    }
                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script>
@endsection

@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Gestión de materiales</div>
            <div class="col-auto">
                <a href="{{route('materiales.crear')}}" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="bottom" title="Nuevo"><i class="fas fa-plus fa-fw"></i></a>
                <a href="{{route('materiales.faltante')}}" class="btn btn-success float-right mr-1">Consultar faltante <i class="fas fa-boxes fa-fw"></i></a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @include ('layouts.mensaje')
        <div class="container-fluid mt-3">
            <table id="materiales" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Tipo de material</th>
                        <th>Color</th>
                        <th>Stock</th>
                        <th>Reservado</th>
                        <th>Faltante</th>
                        <th>Liso</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($materiales as $m)
                    <tr>
                        <td>
                        @if($m->faltante > 0)
                            <i class="fas fa-exclamation-triangle text-danger" data-toggle="tooltip" data-placement="bottom" title="Faltante"></i>
                        @endif
                        {{$m->codigo}}
                        </td>
                        <td>
                        @if (is_null($m->tipo_material))
                            —
                        @else
                            {{$m->tipo_material->nombre}}
                        @endif
                        </td>
                        <td>
                        @if (is_null($m->color))
                            —
                        @else
                            {{$m->color->nombre}}
                        @endif
                        </td>
                        
                        <td>{{$m->tipo_material->convertir($m->stock, $m->tipo_material->unidad_medida_por_bulto)}} {{$m->tipo_material->unidad_medida_por_bulto}}</td>
                        <td>
                        {{$m->tipo_material->convertir($m->reservado, $m->tipo_material->unidad_medida_por_bulto)}} {{$m->tipo_material->unidad_medida_por_bulto}}
                        </td>
                        <td>
                        {{$m->tipo_material->convertir($m->faltante, $m->tipo_material->unidad_medida_por_bulto)}} {{$m->tipo_material->unidad_medida_por_bulto}}
                        </td>
                        @if ($m->es_liso)
                            <td>Si</td>
                        @else
                            <td>No</td>
                        @endif
                        <td>
                        @if (is_null($m->descripcion)  || $m->descripcion == "")
                            -
                        @else
                            {{$m->descripcion}}
                        @endif
                        </td>
                        
                        <td>
                            <a href="{{route('materiales.editar', $m->id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fas fa-pencil-alt fa-fw"></i></a>
                            <form class="d-inline" action="{{route('materiales.eliminar', $m->id)}}" method="POST">
                                @csrf
                                @method ('DELETE')
                                <button type="submit" class="btn btn-danger m-1" data-toggle="tooltip" data-placement="bottom" title="Eliminar" onclick="return confirm ('¿Está seguro de que desea eliminar el registro?')"><i class="fas fa-trash-alt fa-fw"></i></button>
                            </form>
                        </td>            
                    </tr>
                    @endforeach           
                </tbody>
            </table>
        </div>
    </div>
@endsection
