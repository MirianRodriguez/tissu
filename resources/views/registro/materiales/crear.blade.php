@extends('layouts.app')

@section('titulo', 'Crear material')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Crear material
                  </div>

                  <form class="card-body" action="{{route('materiales.crear')}}" method="POST">
                        @csrf

                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                        <div class="row">

                              <div class="form-group col-md-6">
                                    <label for="codigo"> Código <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('codigo') is-invalid @enderror" 
                                    id="codigo" name="codigo" value="{{old('codigo')}}"
                                    maxlength="20">
                                    
                                    @error('codigo')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                              </div>

                              <div class="form-group col-md-6">
                                    <label for="rubro_id"> Rubro </label>
                                    <select id="rubro_id" class="select2 form-control @error('rubro_id') is-invalid @enderror" name="rubro_id">
                                          <option></option>

                                          @foreach ($rubros as $r)
                                          @if ( old('rubro_id') == $r->id )
                                                <option value="{{ $r->id }}" selected>{{ $r->nombre }}</option>
                                          @else
                                                <option value="{{ $r->id }}">{{ $r->nombre }}</option>
                                          @endif
                                          @endforeach
                                    </select>
                                    
                                    @error('rubro_id')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                              </div>
                        </div>

                        <div class="row">
                              <div class="form-group col-md-6">
                                    <label for="tipo_material_id"> Tipo de material <span class="text-danger">*</span></label>
                                    <select id="tipo_material_id" class="select2 form-control @error('tipo_material_id') is-invalid @enderror" name="tipo_material_id">
                                          <option></option>

                                          @foreach ($tiposMateriales as $tm)
                                          @if ( old('tipo_material_id') == $tm->id )
                                                <option value="{{ $tm->id }}" selected>{{ $tm->nombre }}</option>
                                          @else
                                                <option value="{{ $tm->id }}">{{ $tm->nombre }}</option>
                                          @endif
                                          @endforeach
                                    </select>
                                    
                                    @error('tipo_material_id')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                              </div>

                              <div class="form-group col-md-6">
                                    <label for="color_id"> Color <span class="text-danger">*</span></label>
                                    <select id="color_id" class="select2 form-control @error('color_id') is-invalid @enderror" name="color_id">
                                          <option></option>

                                          @foreach ($colores as $c)
                                          @if ( old('color_id') == $c->id )
                                                <option value="{{ $c->id }}" selected>{{ $c->nombre }}</option>
                                          @else
                                                <option value="{{ $c->id }}">{{ $c->nombre }}</option>
                                          @endif
                                          @endforeach
                                    </select>
                                    
                                    @error('color_id')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                              </div>
                        </div> 

                        <div class="row">

                              

                              <div class="form-group col-md-6">
                                    <label for="stock"> Stock </label>
                                    <input type="number"  class="form-control @error('stock') is-invalid @enderror" 
                                    id="stock" name="stock" value="{{old('stock')}}" min="0"
                                    max="999999999">
                                    
                                    @error('stock')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                              </div>
                              <div class="col-md-6 form-group">
                                    <div class="d-flex justify-content-center h-100">
                                          <div class="form-check text-center my-auto m-md-auto">
                                                <input type="checkbox" class="form-check-input" value="1" id="es_liso"
                                                name="es_liso" @if( old('es_liso')) checked @endif>
                                                <label for="es_liso" class="form-check-label">
                                                      Color liso
                                                </label>
                                          </div>
                                    </div>
                              </div>
                        </div>

                        
                        
                        <div class="form-group">
                              <label for="descripcion"> Descripción </label>
                              <input type="text" class="form-control @error('descripcion') is-invalid @enderror" 
                              id="descripcion" name="descripcion" value="{{old('descripcion')}}" maxlength="300">
                               
                              @error('descripcion')
                              <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                              </span>
                              @enderror
                        </div>
                        
                        <div class="d-flex justify-content-center mt-4 ">
                              <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                              <a href="{{route('materiales')}}" class="btn btn-secondary mx-2">Volver</a>
                        </div>
                  </form>
            </div>
      </div>
@endsection
