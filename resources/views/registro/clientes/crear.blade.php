@extends('layouts.app')

@section('titulo', 'Registrar cliente')

@section('contenido')

<div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
    <div class="card mx-1 my-3 ">
        <div class="card-header">
            Registrar cliente
        </div>
        <form class="card-body" action="{{route('clientes.crear')}}" method="POST">
            @csrf
            <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
            <div class="form-group">
                <label for="apellidos">Apellidos <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('apellidos') is-invalid @enderror" 
                id="apellidos" name="apellidos" value="{{old('apellidos')}}" maxlength="50">
                    
                @error('apellidos')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="nombres">Nombres <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('nombres') is-invalid @enderror" 
                id="nombres" name="nombres" value="{{old('nombres')}}" maxlength="50">
                    
                @error('nombres')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
                @enderror
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="telefono"> Teléfono celular<span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('telefono') is-invalid @enderror" 
                    id="telefono" name="telefono" value="{{old('telefono')}}" minlength="10" maxlength="10" 
                    pattern="[0-9]*">
                    
                    @error('telefono')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>   
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="email"> Correo electrónico <span class="text-danger">*</span></label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" 
                    id="email" name="email" value="{{old('email')}}" maxlength="50" >
                    
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>   
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="saldo"> Saldo inicial </label>
                    <input type="number" step="0.01" class="form-control @error('saldo') is-invalid @enderror" 
                    id="saldo" name="saldo" value="{{old('saldo')}}" min="0" max="999999999" >
                    
                    @error('saldo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="d-flex justify-content-center mt-4 ">
                <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                <a href="{{route('clientes')}}" class="btn btn-secondary mx-2">Volver</a>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
  $('#telefono').mask('0000000000');
});
</script>
@endsection
