@extends('layouts.app')

@section('titulo', 'Definir materiales')

@section('contenido')

<div class="container col-9 p-0">
    @include('layouts.mensaje')
    
    <form class="card mx-1 my-3" action="{{route('confecciones.definir-material', $confeccion)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-header">
            Definir materiales y notas particulares para cada confección del pedido
        </div>
        <div class="card-body pt-0"> 
                     
            @include('registro.confecciones.datos-confeccion')
        </div>
        <div class="card-footer d-flex justify-content-center" id="guardar">
            <button type="submit" class="btn btn-primary mx-2">Guardar</button>
            <a href="{{route('pedidos.ver-detalles', $confeccion->pedido->id)}}" class="btn btn-secondary mx-2">Volver</a>
        </div>
    </form>
</div>

@endsection