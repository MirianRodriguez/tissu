@extends('layouts.app')

@section('titulo', 'Cargar medidas')

@section('contenido')

<div class="container col-9 p-0">
    @include('layouts.mensaje')
    <form class="card mx-1 my-3" action="{{route('confecciones.cargar-medidas',$confeccion)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-header">
            Cargar medidas
        </div>
        <div class="card-body">
            <p>{{$confeccion->producto->nombre}}</p>
            <p>
                <span class="font-weight-bold"> Comentario: </span>
                @if (is_null($confeccion->comentario))
                    —
                @else
                    {{$confeccion->comentario}}
                @endif
            </p>
            <table id="productos" class="table table-striped table-bordered table-condensed table-hover">
                <thead style="background-color: #feeae6;">
                    <th style="width:30%">Medida </th>
                    <th style="width:30%">Descripción </th>
                    <th style="width:30%">Valor (en Cm)</th>
                </thead>
                <tbody>
                    @foreach ($confeccion->producto->tipo_producto->medidas_requeridas as $i => $mr)
                    <tr>
                        <td>{{$mr->nombre}}</td>
                        <td>
                        @if(!is_null($mr->descripcion))
                            {{$mr->descripcion}}
                        @else
                            —
                        @endif
                        </td>
                        <td class="form-group">
                            <input type="number" class="form-control @error('valores.'.$mr->id) is-invalid @enderror" 
                            id="valores[{{$mr->id}}]" name="valores[{{$mr->id}}]" value="{{old('valores.' . $mr->id, $confeccion->valor_medida($mr))}}"
                            max="999999999999" min="1">                
                            @error('valores.'.$mr->id)
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="card-footer d-flex justify-content-center" id="guardar">
            <button type="submit" class="btn btn-success mx-2">Guardar</button>
            <a href="{{route('pedidos.ver-detalles',$confeccion->pedido)}}" class="btn btn-secondary mx-2">Volver</a>
        </div>
    </form>
</div>
@endsection