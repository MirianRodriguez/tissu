<div class="row px-3 pt-3">
    <div class="alert alert-danger" role="alert">
        ¡Atención! Una vez que guarde los datos ingresados se reservarán los materiales para la confección y no podrá modificarlos. 
    </div>
    <div class="col-12 pl-0 font-weight-bold">
        {{$confeccion->producto->nombre}}
    </div>
    <div class="col-12 container-fluid p-0">
        <div class="row">
            <div class="col">
                @foreach ($confeccion->producto->tipos_materiales_producto as $tmp)
                <div class="form-group">
                    <label for="materiales[{{$tmp->id}}]">{{$tmp->tipo_material->nombre}}</label>
                    <select id="materiales[{{$tmp->id}}]" class="select2 form-control @error('materiales[{{$tmp->id}}]') is-invalid @enderror" name="materiales[{{$tmp->id}}]">
                        <option></option>                            
                            @foreach($tmp->tipo_material->materialesOrdenados() as $m)
                                @if ( old('materiales.'.$tmp->id, $confeccion->material_elegido($tmp)) == $m->id )
                                    <option value="{{ $m->id }}" selected>
                                        @if($m->no_hay_suficiente($confeccion->producto)) &#128711; @endif
                                        {{ $m->color }} {{ $m->descripcion }}
                                    </option>
                                @else
                                    <option value="{{ $m->id }}">
                                        @if($m->no_hay_suficiente($confeccion->producto)) &#128711; @endif
                                        {{ $m->color }} {{ $m->descripcion }}
                                    </option>
                                @endif
                            @endforeach
                    </select>
                    @error('materiales[{{$tmp->id}}]')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                @endforeach
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="comentario"> Comentario </label>
                    <input type="text" class="form-control @error('comentario') is-invalid @enderror" 
                    id="comentario" name="comentario" value="{{old('comentario',$confeccion->comentario)}}" maxlength="100">
                        
                    @error('comentario')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>