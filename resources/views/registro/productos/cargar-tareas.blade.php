@extends('layouts.app')

@section('titulo', 'Cargar medidas')

@section('contenido')

<div class="container col-9 p-0">
    <form class="card mx-1 my-3" action="{{route('productos.cargar-tareas',$producto)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="py-3 card-header">
            <h6>Tareas a realizar para su confección</h6>
        </div>
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                Por favor, cargue las tareas en el orden de ejecución.
            </div>
            <div class="font-weight-bold">
                {{$producto->nombre}}
            </div>
        </div>
        <div class="container-fluid">
            @include('layouts.mensaje')
            <table id="tareas" class="table table-striped table-bordered table-condensed table-hover mt-2">
                <thead style="background-color: #feeae6;">
                    <th style="width:30%">Tarea <span class="text-danger">*</span></th>
                {{--    <th style="width:30%">Predecesora <span class="text-danger">*</span></th>--}}
                    <th style="width:30%">Dificultad <span class="text-danger">*</span></th>
                    <th style="width:10%" class="justify-content-center">Acciones</th>
                </thead>
                <tbody>
                    @foreach($producto->tareas_producto as $i => $tp)
                        <tr id="fila{{$i}}">
                            <td><input type="hidden" name="tareas[]" id="tareas[]" value="{{$tp->tarea_id}}">{{$tp->tarea->nombre}} </td>
                        {{--    <td><input type="hidden" name="predecesoras[]" id="predecesoras[]" value="{{$tp->predecesora_id}}">{{$tp->predecesora->nombre}} </td> --}}
                            <td><input type="hidden" name="dificultades[]" id="dificultades[]" value="{{$tp->dificultad_id}}">{{$tp->dificultad->nombre}} / {{$tp->dificultad->tiempo_ejecucion}} Hs</td>
                            <td><button type="button" class="btn btn-danger" onclick="eliminar('{{$i}}');"><i class="fas fa-trash-alt fa-fw"></i></button></td> 
                        </tr>                                     
                    @endforeach 
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                        <div class="form-group ">
                            <select id="tarea_id" class="select2 form-control @error('tarea_id') is-invalid @enderror" name="tarea_id">
                                <option></option>

                                @foreach ($tareas as $t)
                                @if ( old('tarea_id') == $t->id )
                                    <option value="{{ $t->id }}" selected>{{ $t->nombre }}</option>
                                @else
                                    <option value="{{ $t->id }}">{{ $t->nombre }}</option>
                                @endif
                                @endforeach
                            </select>
                            
                            @error('tarea_id')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                        </td>
                        {{--    <td>
                        <div class="form-group ">
                            <select id="predecesora_id" class="select2 form-control @error('predecesora_id') is-invalid @enderror" name="predecesora_id">
                                <option></option>

                                @foreach ($tareas as $t)
                                @if ( old('predecesora_id') == $t->id )
                                    <option value="{{ $t->id }}" selected>{{ $t->nombre }} </option>
                                @else
                                    <option value="{{ $t->id }}">{{ $t->nombre }} </option>
                                @endif
                                @endforeach
                            </select>
                            
                            @error('predecesora_id')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                        </td>--}}
                        <td>
                        <div class="form-group ">
                            <select id="dificultad_id" class="select2 form-control @error('dificultad_id') is-invalid @enderror" name="dificultad_id">
                                <option></option>

                                @foreach ($dificultades_tareas as $dt)
                                @if ( old('dificultad_id') == $dt->id )
                                    <option value="{{ $dt->id }}" selected>{{ $dt->nombre }} / {{ $dt->tiempo_ejecucion }} Hs.</option>
                                @else
                                    <option value="{{ $dt->id }}">{{ $dt->nombre }} / {{ $dt->tiempo_ejecucion }} Hs.</option>
                                @endif
                                @endforeach
                            </select>
                            
                            @error('dificultad_id')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                        </td>
                        <td>
                            <button type="button" id="btn_add" class="btn btn-primary"><i class="fas fa-plus"></i></button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="card-footer d-flex justify-content-center" id="guardar">
            <button type="submit" class="btn btn-primary mx-2">Guardar</button>
            <a href="{{route('productos')}}" class="btn btn-secondary mx-2">Volver</a>
        </div>
    </form>
</div>
@endsection
@section('js')
<script>
        $(document).ready(function(){
            //cargarselect();
            $("#btn_add").click(function(){
            agregar();
            $("#error").remove();
            });
        });

        var i=0;

        function agregar(){
            tarea=$("#tarea_id option:selected").text();
            tarea_id=$("#tarea_id option:selected").val();
            //predecesora=$("#predecesora_id option:selected").text();
            //predecesora_id=$("#predecesora_id option:selected").val();
            dificultad=$("#dificultad_id option:selected").text();
            dificultad_id=$("#dificultad_id option:selected").val();

            if (tarea!=""  && dificultad!="" && !esta(tarea_id)){
                var fila = "<tr class='selected' id='fila"+i+"'><td><input type='hidden' name='tareas[]' value="+tarea_id+">"+tarea+"</td><td><input type='hidden' name='dificultades[]' value="+dificultad_id+">"+dificultad+"</td><td><button type='button' class='btn btn-danger' onclick='eliminar("+i+");'><i class='fas fa-trash-alt fa-fw'></i></button></td></tr>";
                i++;
                limpiar(); 
                $('#tareas').append(fila);
                //cargarselect();
            }else{
                alert("No se puede agregar la tarea, revise los datos cargados.");
            }
        }

        function limpiar(){
            $("#tarea_id").val(null).trigger('change');
            $("#predecesora_id").val(null).trigger('change');
            $("#dificultad_id").val(null).trigger('change');
        }

        function eliminar(index){
            $("#fila"+index).remove();
        }

        function esta(tarea) {       
            var retorno = false;
            var tareas = document.getElementsByName('tareas[]'); 
            for (var i = 0; i < tareas.length; i++) { 
                if(tareas[i].value == tarea){
                    retorno = true;
                }
            } 
            return retorno;
        }

        /*function cargarselect() {
            var tareas = document.getElementsByName('tareas[]'); //tareas ya seleccionadas
            var tareas_nombres = @json($tareas); //todas las tareas

            var select = document.getElementById("predecesora_id"); //Seleccionamos el select
            
            for(var i=0; i < tareas_nombres.length; i++){
                for(var j=0; j < tareas.length; j++){
                    if(tareas[j].value == tareas_nombres[i].id){
                        var option = document.createElement("option"); //Creamos la opcion
                        option.innerHTML = tareas_nombres[i].nombre; //Metemos el texto en la opción
                        select.appendChild(option); //Metemos la opción en el select
                    }
                }
            }
        }*/

    </script>
@endsection
