@extends('layouts.app')

@section('titulo', 'Detalles de producto')

@section('contenido')

<div class="container">
<div class="row">
<div class="col-8 offset-2">
    <div class="card mb-3">
        <div class="card-header">
            Detalles del producto
        </div>
        <div class="card-body">
            <h5 class="card-title">{{$producto->nombre}}</h5>
            <p>{{$producto->descripcion}}</p>
            <div>
                <h6 style= "font-weight: bold;">Materiales necesarios</h6>
                <table id="materiales" class="table table-striped table-bordered table-condensed table-hover">
                    <thead style="background-color: #feeae6;">
                        <tr>
                            <th>Cantidad</th>
                            <th>Tipo de material</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($producto->tipos_materiales_producto as $tmp)
                        <tr>
                            <td>{{$tmp->tipo_material->convertir($tmp->cantidad_necesaria, $tmp->tipo_material->unidad_medida_por_bulto)}} {{$tmp->tipo_material->unidad_medida_por_bulto}}</td>
                            <td>{{$tmp->tipo_material->nombre}}</td> 
                        </tr>
                        @endforeach           
                    </tbody>
                </table>
            </div>

            @if(count($producto->tareas_producto)>0)
            <div>
                <h6 style= "font-weight: bold;">Tareas a realizar para su confección</h6>
                <table id="tareas" class="table table-striped table-bordered table-condensed table-hover">
                    <thead style="background-color: #feeae6;">
                        <tr>
                            <th>Tarea</th>
                            <th>Predecesora</th>
                            <th>Dificultad</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($producto->tareas_producto as $tp)
                        <tr>
                            <td>{{$tp->tarea->nombre}}</td>
                            <td>
                            @if(!is_null($tp->predecesora))
                                {{$tp->predecesora->nombre}}</td>
                            @else
                                -
                            @endif
                            <td>{{$tp->dificultad->nombre}} / {{$tp->dificultad->tiempo_ejecucion}} Hs</td> 
                        </tr>
                        @endforeach           
                    </tbody>
                </table>
            </div> 
            @endif             
        </div>
        <div class="d-flex justify-content-center mt-4 mb-3">
            <a href="{{route('productos')}}" class="btn btn-secondary mx-2">Volver</a>
        </div>
    </div>
    </div>
</div>
</div>

@endsection