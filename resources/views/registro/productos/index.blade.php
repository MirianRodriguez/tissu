@extends('layouts.app')

@section('titulo', 'Productos')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            $('#productos').DataTable({

                aaSorting: [], 

                columnDefs: [
                    {
                        targets: 3,
                        className: 'dt-center',
                    },

                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });

        //js para el togle
        function cambiar(toggle){
            
            var status = toggle.checked ? 1 : 0;
            var producto = toggle.id; 

            $.ajax({
                type: "GET",
                dataType: "json",
                url: `/datos/productos/habilitar/${producto}/${status}`,
                data: {},
                success: function(data){
                    alert(data.mensaje);
                    if(!data.cambio)
                        //console.log(toggle);
                        toggle.checked = !toggle.checked;
                }
            });
        }

        $(function() {
            $('.custom-control-input').change(function() {
        })
  })

    </script>
@endsection

@section('contenido')
    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Gestión de productos</div>
            <div class="col-auto">
                <a href="{{route('productos.crear')}}" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="bottom" title="Nuevo"><i class="fas fa-plus"></i></a>
            </div>
        </div>   
    </div>
    <div class="container-fluid">
        @include ('layouts.mensaje')
        <div class="container-fluid mt-3">
            <table id="productos" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo de producto</th>
                        <th>Complejidad</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($productos as $p)
                    <tr>
                        <td>{{$p->nombre}}</td>
                        <td>{{$p->tipo_producto->nombre}}</td>
                        <td>{{$p->complejidad->tipo}}</td>
                        <td>
                            <div class="row justify-content-center">
                                <a href="{{route('productos.ver-detalles', $p->id)}}" class="btn btn-primary m-1" data-toggle="tooltip" data-placement="bottom" title="Detalles"><i class="fas fa-eye fa-fw"></i></a>
                                @if(count($p->confecciones)==0)
                                    <a href="{{route('productos.editar', $p->id)}}" class="btn btn-primary m-1" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fas fa-pencil-alt fa-fw"></i></a>
                                @else
                                    <span data-toggle="tooltip" data-placement="bottom" title="Editar" style="cursor: not-allowed;">
                                        <a class="btn btn-primary m-1 disabled"><i class="fas fa-pencil-alt fa-fw"></i></a>
                                    </span>
                                @endif
                                @if(count($p->confecciones)==0)
                                    <a href="{{route('productos.cargar-tareas', $p->id)}}" class="btn btn-success m-1" data-toggle="tooltip" data-placement="bottom" title="Tareas"><i class="fas fa-tasks fa-fw"></i></a>
                                @else
                                    <span data-toggle="tooltip" data-placement="bottom" title="Tareas" style="cursor: not-allowed;">
                                        <a class="btn btn-success m-1 disabled"><i class="fas fa-tasks fa-fw"></i></a>
                                    </span>
                                @endif
                                {{--disenio toggle--}}
                                <div class="custom-control custom-switch mx-1 my-auto" data-toggle="tooltip" data-placement="bottom" title="Habilitar">
                                    <input data-id="{{$p->id}}" type="checkbox" class="custom-control-input" id="{{$p->id}}" {{ $p->esta_activo ? 'checked' : '' }} onchange="cambiar(this);">
                                    <label class="custom-control-label" for="{{$p->id}}"></label>
                                </div>
                            </div>
                        </td> 
                    </tr>
                    @endforeach           
                </tbody>
            </table>
        </div>
    </div>
@endsection
