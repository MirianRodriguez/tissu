@extends('layouts.app')

@section('titulo', 'Crear producto')

@section('contenido')

<div class="container p-0">
    <form class="card mx-1 my-3" action="{{route('productos.crear')}}" method="POST">
        @csrf
        <div class="card-header">
            Crear producto
        </div>
        <div class="card-body">
            <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
            <div class="container-fluid">         
                <div class="row">
                    <div class="form-group col">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror" 
                        id="nombre" name="nombre" value="{{old('nombre')}}" maxlength="50" >
                        
                        @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col">
                        <label for="tipo_producto_id"> Tipo de producto <span class="text-danger">*</span></label>
                        <select id="tipo_producto_id" class="select2 form-control @error('tipo_producto_id') is-invalid @enderror" name="tipo_producto_id">
                            <option></option>
                            @foreach ($tipos_productos as $tp)
                            @if ( old('tipo_producto_id') == $tp->id )
                                <option value="{{ $tp->id }}" selected>{{ $tp->nombre }}</option>
                            @else
                                <option value="{{ $tp->id }}">{{ $tp->nombre }}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('tipo_producto_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col">
                        <label for="complejidad_id"> Complejidad <span class="text-danger">*</span></label>
                        <select id="complejidad_id" class="select2 form-control @error('complejidad_id') is-invalid @enderror" name="complejidad_id">
                            <option></option>
                            @foreach ($complejidades as $c)
                            @if ( old('complejidad_id') == $c->id )
                                <option value="{{ $c->id }}" selected>{{ $c->tipo }}</option>
                            @else
                                <option value="{{ $c->id }}">{{ $c->tipo }}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('complejidad_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row border-bottom">
                    <div class="form-group col">
                        <label for="descripcion">Descripción</label>
                        <textarea class="form-control @error('descripcion') is-invalid @enderror" 
                        id="descripcion" name="descripcion" rows="3" maxlength="250">{{old('descripcion')}}</textarea>
                        @error('descripcion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                
                <div class="py-3">
                    <h6 class="@error('cantidades') is-invalid @enderror">Materiales necesarios para su fabricación por unidad</h6>
                    @error('cantidades')
                    <span id="error" class="invalid-feedback" role="alert">
                        <strong>Debe cargar al menos un material.</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="container-fluid">
                <table id="materiales" class="table table-striped table-bordered table-condensed table-hover">
                    <thead style="background-color: #feeae6;">
                        <th style="width:30%">Cantidad <span class="text-danger">*</span></th>
                        <th style="width:30%">Tipo de material <span class="text-danger">*</span></th>
                        <th style="width:10%" class="justify-content-center">Acciones</th>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <input type="number" class="form-control @error('cantidad') is-invalid @enderror" 
                                id="cantidad" name="cantidad" value="{{old('cantidad')}}"
                                max="999999999999" min="1" step="1">
                                    
                                @error('cantidad')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                                @enderror
                            </td>
                            <td>
                                <select id="tipo_material_id" class="select2 form-control @error('tipo_material_id') is-invalid @enderror" name="tipo_material_id">
                                    <option></option>
                                    @foreach ($tipos_materiales as $tm)
                                        @if ( old('tipo_material_id') == $tm->id )
                                            <option value="{{ $tm->id }}" selected>{{$tm->unidad_medida_en_stock}} {{ $tm->nombre}} </option>
                                        @else
                                            <option value="{{ $tm->id }}">{{$tm->unidad_medida_en_stock}} {{ $tm->nombre }} </option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('tipo_material_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                                @enderror
                            </td>
                            <td>
                                <button type="button" id="btn_add" class="btn btn-primary"><i class="fas fa-plus"></i></button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center" id="guardar">
            <button type="submit" class="btn btn-primary mx-2">Guardar</button>
            <a href="{{route('productos')}}" class="btn btn-secondary mx-2">Volver</a>
        </div>
    </form>
</div>
@endsection
@section('js')
<script>
        $(document).ready(function(){
            $("#btn_add").click(function(){
            agregar();
            $("#error").remove();
            });
        });

        var i=0;

        function agregar(){
            cantidad=$("#cantidad").val();
            tipo_material=$("#tipo_material_id option:selected").text();
            tipo_material_id=$("#tipo_material_id option:selected").val();

            if (cantidad!="" && tipo_material!="" && cantidad>0 && !esta(tipo_material_id)){
                var fila = "<tr class='selected' id='fila"+i+"'><td><input type='hidden' name='cantidades[]' id='cantidades[]' value="+cantidad+">"+cantidad+"</td><td><input type='hidden' name='tipos_materiales[]' value="+tipo_material_id+">"+tipo_material+"</td><td><button type='button' class='btn btn-danger' onclick='eliminar("+i+");'><i class='fas fa-trash-alt fa-fw'></i></button></td></tr>";
                i++;
                limpiar(); 
                $('#materiales').append(fila);
            }else{
                alert("No se puede agregar el material, revise los datos cargados.");
            }
        }

        function limpiar(){
            $("#cantidad").val("");
            $("#tipo_material_id").val(null).trigger('change');
        }

        function eliminar(index){
            $("#fila"+index).remove();
        }

        function esta(tipo_material) {       
            var retorno = false;
            var tipos_materiales = document.getElementsByName('tipos_materiales[]'); 
            for (var i = 0; i < tipos_materiales.length; i++) { 
                if(tipos_materiales[i].value == tipo_material){
                    retorno = true;
                }
            } 
            return retorno;
        } 
    </script>
@endsection
