@extends('layouts.app')

@section('titulo', 'Crear proveedor')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Crear proveedor
                  </div>
                  <form class="card-body" action="{{route('proveedores.crear')}}" method="POST">
                        @csrf
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                        <div class="form-group">
                            <label for="nombre">Nombre <span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('nombre') is-invalid @enderror" 
                            id="nombre" name="nombre" value="{{old('nombre')}}" maxlength="50">
                               
                            @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="telefono"> Teléfono <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('telefono') is-invalid @enderror" 
                                id="telefono" name="telefono" value="{{old('telefono')}}" minlength="10" maxlength="10" 
                                pattern="[0-9]*">
                                
                                @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>   
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="email"> Correo electrónico <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('email') is-invalid @enderror" 
                                id="email" name="email" value="{{old('email')}}">
                                
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>   
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="costo_envio"> Costo de envío </label>
                                <input type="number" step="0.01" class="form-control @error('costo_envio') is-invalid @enderror" 
                                id="costo_envio" name="costo_envio" value="{{old('costo_envio')}}" min="0" max="999999999">
                                
                                @error('costo_envio')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="demora"> Días de demora </label>
                                <input type="number" class="form-control @error('demora') is-invalid @enderror" 
                                id="demora" name="demora" value="{{old('demora')}}" min= "0" max="365">
                                
                                @error('demora')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                                @enderror
                            </div>

                        </div>

                          <div class="d-flex justify-content-center mt-4 ">
                              <button type="submit" class="btn btn-primary mx-2" >Guardar</button>
                              <a href="{{route('proveedores')}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>
                  </form>
          </div>

    </div>
@endsection
@section('js')
<script>
$(doc ument).ready(function(){
  $('#telefono').mask('0000000000');
});
</script>
@endsection
