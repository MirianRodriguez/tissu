@extends('layouts.app')

@section('titulo', 'Ayuda')

@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Ayuda</div>
        </div>
    </div>
    <div class="container-fluid mt-3">
        {{-- Administrador --}}
        @if(auth()->user()->rol_id == 1)
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'rQICdaHRCcE') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Registrar ajuste de stock</a>
        @endif

        {{-- Confeccionista --}}
        @if(auth()->user()->rol_id == 2)
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'sARtBXb6FE0') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Consultar y finalizar tarea</a>
        @endif

        {{-- Registrador --}}
        @if(auth()->user()->rol_id == 4)
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'uYflvmDwFK0') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Registrar pedido</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', '4O4wvCqbR8k') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Cargar datos de confección</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'qoHweR7r7tI') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Crear producto</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'X_Ln6VVCEKI') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Gestionar productos</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'LpilJ1onQSQ') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Gestionar materiales</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'qbyzwhR6ILQ') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Consultar faltante de materiales</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'muPKRmxMvwQ') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Registrar compra</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'TC51fiIEikg') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Obtener reporte de compras</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'f4vm-sr2JCM') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Gestionar proveedores</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', 'ggA470j8Z7o') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Gestionar clientes</a></br>
            <a href="#" onClick="window.open('{{ route('ayuda.video', '8wlATjnQMPQ') }}', 'Ayuda', 'resizable,height=360,width=640'); return false;">Registrar pagos</a></br>

        @endif
    </div>
@endsection
