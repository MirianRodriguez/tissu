@extends('layouts.app')

@section ('titulo','Inicio')

@section('contenido')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">¡Ha iniciado sesión!</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Bienvenido/a {{Auth::user()->name}}   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
