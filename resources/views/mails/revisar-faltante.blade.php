@extends('layouts.mails')

@section('contenido')

    <p>  
    Buenos días, le informamos que hay tareas en espera debido a que se registra un faltante de materiales. <br>
    Le sugerimos <a href="{{route('materiales')}}" style="text-decoration: none; font-weight: bold;">verificar el faltante</a> y reponer el stock.
    </p>

@endsection