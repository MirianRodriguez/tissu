@extends('layouts.mails')

@section('contenido')
    <p>
    Hola, {{ $datos['nombre'] }}!
    <br>
    Tu pedido está listo para ser retirado en el taller. 
    <br>
    ¡Gracias por elegirnos!
    </p>
@endsection