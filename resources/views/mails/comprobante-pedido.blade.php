@extends('layouts.mails')

@section('contenido')
    <p>
    Hola, {{ $datos['nombre'] }}!
    <br>
    Te enviamos el comprobante del pedido realizado en el taller.
    <br>
    ¡Gracias por elegirnos!
    </p>
@endsection