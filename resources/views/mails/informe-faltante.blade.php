@extends('layouts.mails')

@section('contenido')

    <p>  
    Buenos días, le informamos que se registra un faltante de materiales en el taller. A continuación se detallan los materiales a comprar:
    </p>
    <ul>
    @foreach($datos['detalle'] as $faltante)
        <li>{{$faltante['cantidad']}} {{$faltante['unidadMedida']}} de {{$faltante['nombre']}}</li>
    @endforeach
    </ul>

@endsection