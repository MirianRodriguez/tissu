@extends('layouts.mails')

@section('contenido')
    <p>
    Hola, {{ $datos['nombre'] }}!
    <br>
    Te informamos que debido al retraso en los pagos previstos, el cronograma de pagos y la
    fecha de entrega de tu pedido se han modificado. A continuación podes consultar las nuevas fechas:
    <br>
    <p>Cronograma de pagos</p>
    <ul>
        @foreach($datos['pagos'] as $pr)
        <li>
            $ {{$pr->monto()}} - {{$pr->fecha()}}
        </li>
        @endforeach
    </ul>
    <br>
    <p>Fecha de entrega estimada: {{$datos['fecha']}}</p>
    <br>
    ¡Gracias por elegirnos!
    </p>
@endsection