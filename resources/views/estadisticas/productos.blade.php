@extends('layouts.app')

@section('titulo', 'Productos más vendidos')

@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Producto', 'Pedidos'],
      @foreach ($productos as $p)
        ['{{$p->producto}}' , {{$p->pedidos}}],
      @endforeach
    ]);

    var options = {
      title: 'Porcentaje de pedidos por producto',
      pieHole: 0.4,
      backgroundColor: '#fff7f6',
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }
</script>
@endsection
@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Pedidos por producto</div>
    </div>
</div>
<div ad="filtro" class="container border mx-auto p-3 m-3 bg-light">
    <form action="{{route('estadisticas.productos')}}" method="POST">
        @csrf
        <div class="row">
            <div class="form-group col">
                <label for="desde"> Desde</label>
                <input type="date" class="form-control @error('desde') is-invalid @enderror" 
                id="desde" name="desde" value="{{old('desde',$desde)}}" max="{{now()->format('Y-m-d')}}">

                @error('desde')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group col">
                <label for="hasta"> Hasta </label>
                <input type="date" class="form-control @error('hasta') is-invalid @enderror" 
                id="hasta" name="hasta" value="{{old('hasta', $hasta)}}" max="{{now()->format('Y-m-d')}}">

                @error('hasta')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group col">
                <label for="limite"> Cantidad de productos a considerar </label>
                <input type="number" class="form-control @error('limite') is-invalid @enderror" 
                id="limite" name="limite" value="{{old('limite', $limite)}}" min="1">

                @error('limite')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
                @enderror
            </div>
            <div class="col-auto pt-4">
                <button name="filtrar" id="filtrar" type="submit" class="btn btn-success mx-2" data-toggle="tooltip" data-placement="bottom" title="Filtrar"><i class="fas fa-filter"></i></button>
            </div>
        </div>
    </form>
</div>
@if(count($productos) == 0)
  <h3 class="text-center mt-5">No hay pedidos en el período seleccionado</h3>
@else
  <div class="container-fluid" id="donutchart" style="width: 900px; height: 500px;"></div>
@endif
@endsection