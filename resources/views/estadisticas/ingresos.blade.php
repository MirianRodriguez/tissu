@extends('layouts.app')

@section('titulo', 'Ingresos por mes')

@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart'], 'language': 'es-AR'});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Mes", "Ingresos" ],
        @foreach ($ingresos as $i)
            ['{{$i->mes}}' , {
                v: {{$i->total}},
                f: '{{$i->importe}}'
            }],
        @endforeach
      ]);

      var view = new google.visualization.DataView(data);

      var options = {
        title: "Ingresos de los últimos 12 meses",
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        backgroundColor: '#fff7f6',
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>
@endsection

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Ingresos</div>
    </div>
</div>
<div class="container-fluid" id="columnchart_values" style="width: 900px; height: 300px;"></div>
@endsection