<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Slabo+27px&display=swap" rel="stylesheet">
	    <title> {{ $datos['titulo'] }} </title>
	</head>
	<body>
		<div style="background-color:#FFFFFF; font-family: 'Nunito', sans-serif; font-size: 14pt;">
			
			<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
		    	<tr>
		    		<td valign="top" align="left">

		    			<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="margin: 0;">
							<tr style="background-color: #feeae6; text-align: center;">
							<td style="padding: 15px 5px;">
								<img  
									src="https://i.imgur.com/USQ8sWS.png"
									alt="Logo"
									width="200px"
								> 
							</td>   
							</tr>
							<tr>
								<td>
									<div style="margin: 20px 15%; padding: 10px; background-color: white; font-size: 14pt;">
										@yield('contenido')
									</div>
								</td>
							</tr>

							<tr style="text-align: center; color: white; background-color: #a54a58;">
								<td style="padding: 15px 5px;">
									<p>Tissu. &copy; 2020 - {{ date('Y') }}</p>
								</td>
							</tr>
						</table>
		    		</td>
		    	</tr>
		    </table>
		</div>

	</body>
</html>
