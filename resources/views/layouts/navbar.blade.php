<nav class="navbar fixed-top flex-md-nowrap p-0 shadow navbar-dark">
      <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="{{route('home')}}">Tissu</a>
      <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a href="{{route('logout')}}" class="text-white" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <li class="nav-item active ml-3 text-right">
              <i class="fas fa-sign-out-alt"></i> Cerrar sesión
            </li>
          </a>
          <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
            @csrf
          </form>
        </li>
      </ul>
</nav>