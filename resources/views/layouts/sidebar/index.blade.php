<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block sidebar collapse">
      <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
          @if(auth()->user()->rol_id == 4)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('pedidos')}}">
                <i class="fas fa-box-open fa-fw"></i>
                Pedidos
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('productos')}}">
                <i class="fas fa-tshirt fa-fw"></i>
                Productos
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('materiales')}}">
                <i class="fas fa-puzzle-piece fa-fw"></i>
                Materiales
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('compras')}}">
                <i class="fas fa-shopping-cart fa-fw"></i>
                Compras
              </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('proveedores')}}">
              <i class="fas fa-people-carry fa-fw"></i>
              Proveedores
            </a>
          </li> 
          <li class="nav-item">
            <a class="nav-link" href="{{ route('clientes')}}">
            <i class="fas fa-user-friends fa-fw"></i>
              Clientes
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('movimientos')}}">
            <i class="fas fa-money-check-alt fa-fw"></i>
              Pagos
            </a>
          </li>
          @endif
          @if(auth()->user()->rol_id == 1)
          <li class="nav-item">
            <a class="nav-link" href="{{ route('usuarios')}}">
            <i class="fas fa-user-friends fa-fw"></i>
              Usuarios
            </a>
          </li>   
          <li class="nav-item">
            <a class="nav-link" href="{{ route('ajustes')}}">
            <i class="fas fa-wrench fa-fw"></i>
              Ajustes de stock
            </a>
          </li>
          <li class="nav-item">
            <a href="#parametros" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                <i class="fas fa-sliders-h fa-fw"></i>
                Parámetros
            </a>
            <ul class="collapse list" id="parametros">
                <li>
                    <a href="{{ route('rubros')}}" class="nav-link">Rubros</a>
                </li>
                <li>
                    <a href="{{ route('colores')}}" class="nav-link">Colores</a>
                </li>
                <li>
                    <a href="{{ route('tipos-materiales')}}" class="nav-link">Tipos de materiales</a>
                </li>
                <li>
                    <a href="{{ route('tipos-productos')}}" class="nav-link">Tipos de productos</a>
                </li> 
                <li>
                    <a href="{{ route('complejidades')}}" class="nav-link">Tipos de complejidad</a>
                </li>
                <li>
                    <a href="{{ route('dificultades-tareas')}}" class="nav-link">Dificultades de tareas</a>
                </li> 
                {{--<li>
                    <a href="{{ route('estados-tareas')}}" class="nav-link">Estados de tareas</a>
                </li>--}}
                <li>
                    <a href="{{ route('tareas')}}" class="nav-link">Tareas</a>
                </li>   
            </ul>
          </li>
          <li class="nav-item">
            <a href="#estadisticas" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
            <i class="fas fa-chart-line fa-fw"></i>
                Estadísticas
            </a>
            <ul class="collapse list" id="estadisticas">
                <li>
                    <a href="{{ route('estadisticas.productos')}}" class="nav-link">Productos más vendidos</a>
                </li>
                <li>
                    <a href="{{ route('estadisticas.ingresos')}}" class="nav-link">Ingresos</a>
                </li>   
            </ul>
          </li> 
          @endif
          @if(auth()->user()->rol_id == 2)
          <li class="nav-item">
            <a class="nav-link" href="{{ route('asignaciones') }}">
              <i class="fas fa-cut fa-fw"></i>
              Tareas asignadas
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('asignaciones.historial') }}">
              <i class="fas fa-history fa-fw"></i>
              Historial de tareas asignadas
            </a>
          </li>
          @endif
          @if(auth()->user()->rol_id == 3)
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logs.todos') }}">
              <i class="fas fa-search"></i>
              Logs de auditoría
            </a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('ayuda')}}">
            <i class="far fa-question-circle fa-fw"></i>
              Ayuda
            </a>
          </li>  
          @endif
        </ul>
      </div>
    </nav>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
      <div class="pt-3 pb-2 mb-3">
        @yield ('contenido')
      </div>
    </main>
  </div>
</div>