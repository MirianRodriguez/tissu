<!doctype html>
<html lang="es" style="position:relative; min-height: 100%;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v4.1.1">
        <title>@yield('titulo') - Tissu </title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

        <!-- Bootstrap core CSS -->
        <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <!--  Fuente-->
        <link href="https://fonts.googleapis.com/css2?family=Slabo+27px&display=swap" rel="stylesheet">
        <style>
          .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
          }

          @media (min-width: 768px) {
            .bd-placeholder-img-lg {
              font-size: 3.5rem;
            }
          }
        </style>
        @yield('css')
        <!-- Custom styles for this template -->
         <link rel="stylesheet" href="{{asset('css/app.css') }}">
         <!-- Font Awesome JS -->
         <script defer src="{{ asset ('js/all.js')}}"></script>
         <!-- Favicon-->
         <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon"/>
         <!-- Select 2 -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    </head>
    <body>
        <div id="content" class="container-fluid">
            <!-- Page navbar  -->
            <div class="row ">
                <div class="col p-0">
                    @include('layouts.navbar')
                </div>
            </div>
            <!-- Page sidebar && content -->
            <div class="row">
                <div class="col p-0">
                    @include('layouts.sidebar.index')
                </div>
            </div>
            <!-- Page footer -->
            <div class="row">
                <footer class="container">
                    <ul class="nav justify-content-end">
                        <li class="nav-item mr-5">
                            <p>{{auth()->user()->rol->nombre}} {{auth()->user()->name}}</p>
                        </li>
                        <li class="nav-item mr-5">
                            <p>Tissu. &copy; 2020 - {{ date('Y') }}</p>
                        </li>
                        <li class="nav-item">
                            <p class="float-right"><a href="#">Ir arriba</a></p>
                        </li>
                    </ul>    
                </footer>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="{{ asset ('js/popper.min.js')}}"></script>
        <script src="/docs/4.5/dist/js/bootstrap.bundle.min.js" integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf" crossorigin="anonymous"></script>
        <script defer src="{{ asset ('js/bundle.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script defer src="{{ asset ('js/dashboard.js')}}"></script>
        <!--select 2-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.select2').select2({
                    placeholder: 'Seleccione una opción',
                    allowClear: true,
                    theme: 'classic',
                    width: '100%',
                });
                $('.js-example-basic-multiple').select2({
                    width: '100%',
                });
                $('body').tooltip({selector: '[data-toggle="tooltip"]'});
            });
        </script>
        <!--mascaras-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
        @yield('js')
    </body>
</html>
