<?php

Route::get('/', function () {
    return view('welcome');
});

/* Esta ruta está protegida por el middleware administrador, que solo acepta peticiones de usuarios con ese rol */


Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

//Administrador

Route::middleware(['auth','administrador'])->prefix('administracion')->group(function () {

    Route::prefix('rubros')->group(function() {

        Route::get('/', 'RubroController@index')->name('rubros');

        Route::get('crear', 'RubroController@crear')->name('rubros.crear');

        Route::post('crear', 'RubroController@almacenar'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

        Route::get('editar/{rubro}', 'RubroController@editar')->where('rubro', '[0-9]+')->name('rubros.editar');

        Route::put('editar/{rubro}', 'RubroController@actualizar')->where('rubro', '[0-9]+'); /* el where limita que el parametro sea solo nro */
        
        Route::delete('eliminar/{rubro}', 'RubroController@eliminar')->where('rubro', '[0-9]+')->name('rubros.eliminar');

    });

    Route::prefix('colores')->group(function() {

        Route::get('/', 'ColorController@index')->name('colores');

        Route::get('crear', 'ColorController@crear')->name('colores.crear');

        Route::post('crear', 'ColorController@almacenar'); 

        Route::get('editar/{color}', 'ColorController@editar')->where('color', '[0-9]+')->name('colores.editar');

        Route::put('editar/{color}', 'ColorController@actualizar')->where('color', '[0-9]+'); 
        
        Route::delete('eliminar/{color}', 'ColorController@eliminar')->where('color', '[0-9]+')->name('colores.eliminar');

    });

    Route::prefix('tipos-de-materiales')->group(function() {

        Route::get('/', 'TipoMaterialController@index')->name('tipos-materiales');

        Route::get('crear', 'TipoMaterialController@crear')->name('tipos-materiales.crear');

        Route::post('crear', 'TipoMaterialController@almacenar'); 

        Route::get('editar/{tipoMaterial}', 'TipoMaterialController@editar')->where('tipoMaterial', '[0-9]+')->name('tipos-materiales.editar');

        Route::put('editar/{tipoMaterial}', 'TipoMaterialController@actualizar')->where('tipoMaterial', '[0-9]+'); 
        
        Route::delete('eliminar/{tipoMaterial}', 'TipoMaterialController@eliminar')->where('tipoMaterial', '[0-9]+')->name('tipos-materiales.eliminar');

    });

    Route::prefix('tipos-de-productos')->group(function() {

        Route::get('/', 'TipoProductoController@index')->name('tipos-productos');

        Route::get('crear', 'TipoProductoController@crear')->name('tipos-productos.crear');

        Route::post('crear', 'TipoProductoController@almacenar'); 

        Route::get('editar/{tipoProducto}', 'TipoProductoController@editar')->where('tipoProducto', '[0-9]+')->name('tipos-productos.editar');

        Route::put('editar/{tipoProducto}', 'TipoProductoController@actualizar')->where('tipoProducto', '[0-9]+'); 
        
        Route::delete('eliminar/{tipoProducto}', 'TipoProductoController@eliminar')->where('tipoProducto', '[0-9]+')->name('tipos-productos.eliminar');

    });

    Route::prefix('complejidades')->group(function() {

        Route::get('/', 'ComplejidadController@index')->name('complejidades');

        Route::get('crear', 'ComplejidadController@crear')->name('complejidades.crear');

        Route::post('crear', 'ComplejidadController@almacenar'); 

        Route::get('editar/{complejidad}', 'ComplejidadController@editar')->where('complejidad', '[0-9]+')->name('complejidades.editar');

        Route::put('editar/{complejidad}', 'ComplejidadController@actualizar')->where('complejidad', '[0-9]+'); 
        
        Route::delete('eliminar/{complejidad}', 'ComplejidadController@eliminar')->where('complejidad', '[0-9]+')->name('complejidades.eliminar');

    });

    Route::prefix('usuarios')->group(function() {

        Route::get('/', 'UsuarioController@index')->name('usuarios');

        Route::get('crear', 'UsuarioController@crear')->name('usuarios.crear');

        Route::post('crear', 'UsuarioController@almacenar'); 

        Route::get('editar/{usuario}', 'UsuarioController@editar')->where('usuario', '[0-9]+')->name('usuarios.editar');

        Route::put('editar/{usuario}', 'UsuarioController@actualizar')->where('usuario', '[0-9]+'); 
        
        Route::delete('eliminar/{usuario}', 'UsuarioController@eliminar')->where('usuario', '[0-9]+')->name('usuarios.eliminar');

    });

    Route::prefix('dificultades-tareas')->group(function() {

        Route::get('/', 'DificultadTareaController@index')->name('dificultades-tareas');

        Route::get('crear', 'DificultadTareaController@crear')->name('dificultades-tareas.crear');

        Route::post('crear', 'DificultadTareaController@almacenar'); 

        Route::get('editar/{dificultadTarea}', 'DificultadTareaController@editar')->where('dificultadTarea', '[0-9]+')->name('dificultades-tareas.editar');

        Route::put('editar/{dificultadTarea}', 'DificultadTareaController@actualizar')->where('dificultadTarea', '[0-9]+'); 
        
        Route::delete('eliminar/{dificultadTarea}', 'DificultadTareaController@eliminar')->where('dificultadTarea', '[0-9]+')->name('dificultades-tareas.eliminar');

    });

    /*Route::prefix('estados-tareas')->group(function() {

        Route::get('/', 'EstadoTareaController@index')->name('estados-tareas');

        Route::get('crear', 'EstadoTareaController@crear')->name('estados-tareas.crear');

        Route::post('crear', 'EstadoTareaController@almacenar'); 

        Route::get('editar/{estadoTarea}', 'EstadoTareaController@editar')->where('estadoTarea', '[0-9]+')->name('estados-tareas.editar');

        Route::put('editar/{estadoTarea}', 'EstadoTareaController@actualizar')->where('estadoTarea', '[0-9]+'); 
        
        Route::delete('eliminar/{estadoTarea}', 'EstadoTareaController@eliminar')->where('estadoTarea', '[0-9]+')->name('estados-tareas.eliminar');

    });*/

    Route::prefix('tareas')->group(function() {

        Route::get('/', 'TareaController@index')->name('tareas');

        Route::get('crear', 'TareaController@crear')->name('tareas.crear');

        Route::post('crear', 'TareaController@almacenar'); 

        Route::get('editar/{tarea}', 'TareaController@editar')->where('tarea', '[0-9]+')->name('tareas.editar');

        Route::put('editar/{tarea}', 'TareaController@actualizar')->where('tarea', '[0-9]+'); 
        
        Route::delete('eliminar/{tarea}', 'TareaController@eliminar')->where('tarea', '[0-9]+')->name('tareas.eliminar');

    });

    Route::prefix('ajustes')->group(function() {

        Route::get('/', 'AjusteStockController@index')->name('ajustes');

        Route::get('crear', 'AjusteStockController@crear')->name('ajustes.crear');

        Route::post('crear', 'AjusteStockController@almacenar'); 

    });

    Route::prefix('estadisticas')->group(function() {

        Route::match(['get','post'],'productos', 'EstadisticaController@productosMasPedidos')->name('estadisticas.productos');

        Route::get('ingresos', 'EstadisticaController@ingresos')->name('estadisticas.ingresos');

    });


});

//Registrador

Route::middleware(['auth','registrador'])->prefix('datos')->group(function () {

    Route::prefix('pedidos')->group(function() {

        Route::get('/', 'PedidoController@index')->name('pedidos');

        Route::post('generar', 'PedidoController@generar')->name('pedidos.generar');

        Route::get('crear', 'PedidoController@crear')->name('pedidos.crear');

        Route::post('crear', 'PedidoController@almacenar'); 

        Route::get('ver-detalles/{pedido}', 'PedidoController@verDetalles')->where('pedido', '[0-9]+')->name('pedidos.ver-detalles');

        Route::get('descargar/{pedido}', 'PedidoController@descargar')->where('pedido', '[0-9]+')->name('pedidos.descargar');

        Route::get('finalizar/{pedido}', 'PedidoController@entregar')->where('pedido', '[0-9]+')->name('pedidos.entregar'); 

    });

    Route::prefix('confecciones')->group(function() {

        Route::get('definir-material/{confeccion}', 'ConfeccionController@cargarMateriales')->where('confeccion', '[0-9]+')->name('confecciones.definir-material');

        Route::put('definir-material/{confeccion}', 'ConfeccionController@guardarMateriales')->where('confeccion', '[0-9]+'); 

        Route::get('cargar-medidas/{confeccion}', 'ConfeccionController@cargarMedidas')->where('confeccion', '[0-9]+')->name('confecciones.cargar-medidas');

        Route::put('cargar-medidas/{confeccion}', 'ConfeccionController@guardarMedidas')->where('confeccion', '[0-9]+'); 

    });

    Route::prefix('compras')->group(function() {

        Route::match(['get','post'], '/', 'CompraController@index')->name('compras'); //usa metodos get y post porque tiene un formulario para filtros

        Route::get('cargar', 'CompraController@nueva')->name('compras.cargar');

        Route::post('cargar', 'CompraController@almacenar'); 

    });

    Route::prefix('materiales')->group(function() {

        Route::get('/', 'MaterialController@index')->name('materiales');

        Route::get('crear', 'MaterialController@crear')->name('materiales.crear');

        Route::post('crear', 'MaterialController@almacenar'); 

        Route::get('editar/{material}', 'MaterialController@editar')->where('material', '[0-9]+')->name('materiales.editar');

        Route::put('editar/{material}', 'MaterialController@actualizar')->where('material', '[0-9]+'); 
        
        Route::delete('eliminar/{material}', 'MaterialController@eliminar')->where('material', '[0-9]+')->name('materiales.eliminar');

        Route::get('faltante', 'MaterialController@verificarFaltanteMateriales')->name('materiales.faltante');

    });

    Route::prefix('productos')->group(function() {

        Route::get('/', 'ProductoController@index')->name('productos');

        Route::get('habilitar/{producto}/{status}', 'ProductoController@habilitar')->where('producto', '[0-9]+')->where('status', '[0,1]');

        Route::get('crear', 'ProductoController@crear')->name('productos.crear');

        Route::post('crear', 'ProductoController@almacenar'); 

        Route::get('editar/{producto}', 'ProductoController@editar')->where('producto', '[0-9]+')->name('productos.editar');

        Route::put('editar/{producto}', 'ProductoController@actualizar')->where('producto', '[0-9]+'); 
      
        Route::get('cargar-tareas/{producto}', 'ProductoController@cargarTareas')->where('producto', '[0-9]+')->name('productos.cargar-tareas');

        Route::put('cargar-tareas/{producto}', 'ProductoController@guardarTareas')->where('producto', '[0-9]+');

        Route::get('ver-detalles/{producto}', 'ProductoController@verDetalles')->where('producto', '[0-9]+')->name('productos.ver-detalles');


    });

    Route::prefix('clientes')->group(function() {

        Route::get('/', 'ClienteController@index')->name('clientes');

        Route::get('crear', 'ClienteController@crear')->name('clientes.crear');

        Route::post('crear', 'ClienteController@almacenar'); 

        Route::get('editar/{cliente}', 'ClienteController@editar')->where('cliente', '[0-9]+')->name('clientes.editar');

        Route::put('editar/{cliente}', 'ClienteController@actualizar')->where('cliente', '[0-9]+'); 

    });

    Route::prefix('proveedores')->group(function() {

        Route::get('/', 'ProveedorController@index')->name('proveedores');

        Route::get('crear', 'ProveedorController@crear')->name('proveedores.crear');

        Route::post('crear', 'ProveedorController@almacenar'); 

        Route::get('editar/{proveedor}', 'ProveedorController@editar')->where('proveedor', '[0-9]+')->name('proveedores.editar');

        Route::put('editar/{proveedor}', 'ProveedorController@actualizar')->where('proveedor', '[0-9]+'); 
        
        Route::delete('eliminar/{proveedor}', 'ProveedorController@eliminar')->where('proveedor', '[0-9]+')->name('proveedores.eliminar');

    });

    
    Route::prefix('movimientos')->group(function() {

        Route::get('/', 'MovimientoController@index')->name('movimientos');

        Route::get('crear', 'MovimientoController@crear')->name('movimientos.crear');

        Route::post('crear', 'MovimientoController@almacenar'); 

        Route::post('cargarPagos', 'MovimientoController@cargarPagos')->name('movimientos.cargarPagos');

        Route::get('descargar/{movimiento}', 'MovimientoController@descargar')->where('movimiento', '[0-9]+')->name('movimientos.descargar');

    });

});

//Confeccionista

Route::middleware(['auth','confeccionista'])->prefix('confecciones')->group(function () {

    Route::prefix('asignaciones')->group(function() {

        Route::get('/', 'AsignacionController@index')->name('asignaciones'); 

        Route::get('ver-detalles/{asignacion}', 'AsignacionController@verDetalles')->where('asignacion', '[0-9]+')->name('asignaciones.ver-detalles');

        Route::get('finalizar/{asignacion}', 'AsignacionController@finalizar')->where('asignacion', '[0-9]+')->name('asignaciones.finalizar'); 

        Route::get('historial', 'AsignacionController@historial')->name('asignaciones.historial'); 

    });

});

// AUDITOR
Route::middleware(['auth', 'auditor'])
    ->prefix('auditor')->group(function () {

    Route::get('logs', 'AuditoriaController@todos')->name('logs.todos');

    Route::post('logs', 'AuditoriaController@filtrados')->name('logs.filtrados');

});

//MANUAL DE USUARIO
Route::prefix('ayuda')->group(function() {

    Route::get('/', 'AyudaController@index')->name('ayuda'); 

    Route::get('video/{video}', 'AyudaController@video')->name('ayuda.video'); 

});
