<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
            'nombre' => 'Administrador',
        ]);

        DB::table('roles')->insert([
            'nombre' => 'Confeccionista',
        ]);

        DB::table('roles')->insert([
            'nombre' => 'Auditor',
        ]);

        DB::table('roles')->insert([
            'nombre' => 'Registrador',
        ]);
    }
}
