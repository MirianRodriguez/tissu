<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PedidoSeeder extends Seeder
{

    public function run()
    {
        DB::table('pedidos')->insert([
            'presupuesto'           => '2500',
            'fecha_pactada'         => '2020/12/25',
            'fecha'                 => '2020/11/14',
            'porcentaje_avance'     => '0',
            'comentario'            => 'Mandar a sublimar imágen de San Cayetano',
            'cliente_id'            => '1',
        ]);

        DB::table('pedidos')->insert([
            'presupuesto'           => '10000',
            'fecha_pactada'         => '2020/12/30',
            'fecha'                 => '2020/11/14',
            'porcentaje_avance'     => '0',
            'cliente_id'            => '2',
        ]);

        DB::table('pedidos')->insert([
            'presupuesto'           => '950',
            'fecha_pactada'         => '2020/12/23',
            'fecha'                 => '2020/11/29',
            'porcentaje_avance'     => '0',
            'comentario'            => 'Mandar a bordar el logo del ejército',
            'cliente_id'            => '3',
        ]);
    }
}
