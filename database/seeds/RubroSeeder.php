<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RubroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rubros')->insert([
            'nombre' => 'Telas',
        ]);

        DB::table('rubros')->insert([
            'nombre' => 'Merceria',
        ]);
    }
}
