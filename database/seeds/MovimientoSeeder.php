<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovimientoSeeder extends Seeder
{
    public function run()
    {
        DB::table('movimientos')->insert([
            'fecha'         => '2020/01/14',
            'monto'         => '1500',
            'cliente_id'    => '1',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/01/18',
            'monto'         => '2000',
            'cliente_id'    => '2',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/02/12',
            'monto'         => '2000',
            'cliente_id'    => '3',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/02/26',
            'monto'         => '2650.5',
            'cliente_id'    => '4',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/03/14',
            'monto'         => '2760',
            'cliente_id'    => '1',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/03/26',
            'monto'         => '3300',
            'cliente_id'    => '2',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/04/02',
            'monto'         => '3970.66',
            'cliente_id'    => '3',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/04/17',
            'monto'         => '4386.90',
            'cliente_id'    => '4',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/05/01',
            'monto'         => '4689.30',
            'cliente_id'    => '1',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/05/23',
            'monto'         => '4975',
            'cliente_id'    => '2',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/06/04',
            'monto'         => '5720',
            'cliente_id'    => '3',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/06/29',
            'monto'         => '5800',
            'cliente_id'    => '4',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/07/09',
            'monto'         => '6150.25',
            'cliente_id'    => '3',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/07/24',
            'monto'         => '6845',
            'cliente_id'    => '1',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/08/11',
            'monto'         => '7050',
            'cliente_id'    => '2',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/09/07',
            'monto'         => '8000',
            'cliente_id'    => '3',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/09/14',
            'monto'         => '3600',
            'cliente_id'    => '4',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/09/21',
            'monto'         => '4600',
            'cliente_id'    => '1',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/10/02',
            'monto'         => '8260',
            'cliente_id'    => '2',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/10/14',
            'monto'         => '8590',
            'cliente_id'    => '4',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/11/15',
            'monto'         => '8995',
            'cliente_id'    => '1',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/11/22',
            'monto'         => '9300',
            'cliente_id'    => '3',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/12/19',
            'monto'         => '9500',
            'cliente_id'    => '1',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2020/12/22',
            'monto'         => '9980',
            'cliente_id'    => '2',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2021/01/04',
            'monto'         => '10000',
            'cliente_id'    => '3',
        ]);

        DB::table('movimientos')->insert([
            'fecha'         => '2021/01/06',
            'monto'         => '10500',
            'cliente_id'    => '4',
        ]);
    }
}
