<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name'      => 'Mirian Rodriguez',
            'email'     => 'mirodriiguez8@gmail.com',
            'password'  => bcrypt('administrador'),
            'rol_id'    => 1,
        ]);

        
        User::create([
            'name'      => 'Rosa Rivas',
            'email'     => 'rivas-rossy@hotmail.com.ar',
            'password'  => bcrypt('rosarivas'),
            'rol_id'    => 2,
        ]);

        User::create([
            'name'      => 'susana Pluhator',
            'email'     => 'susanapluhator@gmail.com',
            'password'  => bcrypt('susanita'),
            'rol_id'    => 2,
        ]);
            
        User::create([
            'name'      => 'Alejandro Pezuk',
            'email'     => 'alejandropezuk@gmail.com',
            'password'  => bcrypt('elauditor'),
            'rol_id'    => 3,
        ]);
                
        User::create([
            'name'      => 'Tatiana Krayeski',
            'email'     => 'maiakrayeski@gmail.com',
            'password'  => bcrypt('registrador'),
            'rol_id'    => 4,
        ]);
    }
}
