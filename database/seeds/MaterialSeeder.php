<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Material;

class MaterialSeeder extends Seeder
{

    public function run()
    {
        /* Telas */
        DB::table('materiales')->insert([
            'codigo' => 'T001',
            'stock' => '1500',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '3',
            'rubro_id' => '1',
            'color_id' => '1',
        ]);

        DB::table('materiales')->insert([
            'codigo' => 'T002',

            'stock' => '2000',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '1',
            'rubro_id' => '1',
            'color_id' => '6',
        ]);

        DB::table('materiales')->insert([
            'codigo' => 'T003',

            'stock' => '1000',
            'es_liso' => false,
            'descripcion' => 'patitos',
            'tipo_material_id' => '2',
            'rubro_id' => '1',
            'color_id' => '5',
        ]);

        DB::table('materiales')->insert([
            'codigo' => 'T004',

            'stock' => '500',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '2',
            'rubro_id' => '1',
            'color_id' => '6',
        ]);

        
        DB::table('materiales')->insert([
            'codigo' => 'T005',

            'stock' => '600',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '1',
            'rubro_id' => '1',
            'color_id' => '9',
        ]);

        /* Botones */
    
        DB::table('materiales')->insert([
            'codigo' => 'BT001',
           
            'stock' => '100',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '7',
            'rubro_id' => '2',
            'color_id' => '6',
        ]);
            
        DB::table('materiales')->insert([
            'codigo' => 'BT002',
           
            'stock' => '100',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '7',
            'rubro_id' => '2',
            'color_id' => '9',
        ]);
            
        DB::table('materiales')->insert([
            'codigo' => 'BT003',
           
            'stock' => '100',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '8',
            'rubro_id' => '2',
            'color_id' => '1',
        ]);

        /* Elasticos */

        DB::table('materiales')->insert([
            'codigo' => 'EL001',
            
            'stock' => '3000',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '16',
            'rubro_id' => '2',
            'color_id' => '4',
        ]);

        DB::table('materiales')->insert([
            'codigo' => 'EL002',
            
            'stock' => '3000',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '18',
            'rubro_id' => '2',
            'color_id' => '4',
        ]);

        /* Lentejuelas */
        
        DB::table('materiales')->insert([
            'codigo' => 'LT001',
            
            'stock' => '200',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '19',
            'rubro_id' => '2',
            'color_id' => '7',
        ]);

        /* Cierres */

        DB::table('materiales')->insert([
            'codigo' => 'C001',
            
            'stock' => '15',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '11',
            'rubro_id' => '2',
            'color_id' => '1',
        ]);

        
        DB::table('materiales')->insert([
            'codigo' => 'C002',
           
            'stock' => '15',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '11',
            'rubro_id' => '2',
            'color_id' => '2',
        ]);

        DB::table('materiales')->insert([
            'codigo' => 'C003',
            
            'stock' => '13',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '12',
            'rubro_id' => '2',
            'color_id' => '2',
        ]);

        DB::table('materiales')->insert([
            'codigo' => 'T006',
            
            'stock' => '100',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '5',
            'rubro_id' => '2',
            'color_id' => '10',
        ]);

        DB::table('materiales')->insert([
            'codigo' => 'T007',
            
            'stock' => '100',
            'es_liso' => true,
            'descripcion' => '',
            'tipo_material_id' => '5',
            'rubro_id' => '2',
            'color_id' => '9',
        ]);
    }
}
