<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Proveedor;

class ProveedorSeeder extends Seeder
{

    public function run()
    {
        DB::table('proveedores')->insert([
            'nombre' => 'Makor',
            'telefono' => '3414254500',
            'email' => 'makor@gmail.com',
            'costo_envio' => '1000',
            'demora' => '7',
        ]);

        DB::table('proveedores')->insert([
            'nombre' => 'Todotelas',
            'telefono' => '0341425450',
            'email' => 'todotelas@gmail.com',
            'costo_envio' => '750',
            'demora' => '5',
        ]);
    }
}
