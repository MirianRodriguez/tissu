<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\TipoMaterialProducto;

class TipoMaterialProductoSeeder extends Seeder
{

    public function run()
    {
        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '70',
            'tipo_material_id'      => '1',
            'producto_id'           => '1',
        ]);

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '10',
            'tipo_material_id'      => '7',
            'producto_id'           => '1',
        ]);

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '100',
            'tipo_material_id'      => '2',
            'producto_id'           => '2',
        ]);

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '1',
            'tipo_material_id'      => '11',
            'producto_id'           => '2',
        ]);

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '100',
            'tipo_material_id'      => '5',
            'producto_id'           => '3',
        ]);

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '100',
            'tipo_material_id'      => '18',
            'producto_id'           => '3',
        ]);

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '70',
            'tipo_material_id'      => '1',
            'producto_id'           => '4',
        ]);

        DB::table('tipos_materiales_productos')->insert([
            'cantidad_necesaria'    => '10',
            'tipo_material_id'      => '7',
            'producto_id'           => '4',
        ]);
    }
}
