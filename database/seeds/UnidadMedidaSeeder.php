<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\TipoMaterial;

class UnidadMedidaSeeder extends Seeder
{

    public function run()
    {
        DB::table('unidades_medida')->insert([
            'nombre' => 'Mt.',
        ]);

        DB::table('unidades_medida')->insert([
            'nombre' => 'Cm.',
        ]);

        DB::table('unidades_medida')->insert([
            'nombre' => 'Kg.',
        ]);

        DB::table('unidades_medida')->insert([
            'nombre' => 'Gr.',
        ]);

        DB::table('unidades_medida')->insert([
            'nombre' => 'Unidad',
        ]);
    }
}
