<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfeccionSeeder extends Seeder
{

    public function run()
    {
        DB::table('confecciones')->insert([
            'comentario'     => 'Tatiana',
            'cantidad'       => '1',
            'producto_id'    => '1',
            'pedido_id'      => '1',
        ]);

        DB::table('confecciones')->insert([
            'comentario'     => 'Rosa',
            'cantidad'       => '1',
            'producto_id'    => '1',
            'pedido_id'      => '1',
        ]);

        DB::table('confecciones')->insert([
            'comentario'     => 'Mirian',
            'cantidad'       => '1',
            'producto_id'    => '1',
            'pedido_id'      => '1',
        ]);

        DB::table('confecciones')->insert([
            'comentario'     => 'Tatiana',
            'cantidad'       => '2',
            'producto_id'    => '2',
            'pedido_id'      => '2',
        ]);

        DB::table('confecciones')->insert([
            'comentario'     => 'Rosa',
            'cantidad'       => '1',
            'producto_id'    => '2',
            'pedido_id'      => '2',
        ]);

        DB::table('confecciones')->insert([
            'comentario'     => 'Mirian',
            'cantidad'       => '1',
            'producto_id'    => '3',
            'pedido_id'      => '2',
        ]);

        DB::table('confecciones')->insert([
            'comentario'     => 'Marcelo',
            'cantidad'       => '3',
            'producto_id'    => '4',
            'pedido_id'      => '3',
        ]);
    }
}
