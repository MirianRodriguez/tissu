<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialConfeccionSeeder extends Seeder
{
    public function run()
    {
        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '1',
            'material_id'       => '2',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '1',
            'material_id'       => '7',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '2',
            'material_id'       => '2',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '2',
            'material_id'       => '7',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '3',
            'material_id'       => '2',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '3',
            'material_id'       => '6',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '4',
            'material_id'       => '4',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '4',
            'material_id'       => '12',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '5',
            'material_id'       => '4',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '5',
            'material_id'       => '13',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '6',
            'material_id'       => '15',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '6',
            'material_id'       => '10',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '7',
            'material_id'       => '5',
        ]);

        DB::table('material_confeccion')->insert([
            'confeccion_id'     => '7',
            'material_id'       => '7',
        ]);
    }
}
