<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagoRequeridoSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/11/14',
            'monto'     => '1500',
            'esta_pagado'=> true,
            'pedido_id' => '1',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/11/29',
            'monto'     => '500',
            'esta_pagado'=> true,
            'pedido_id' => '1',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/12/25',
            'monto'     => '500',
            'esta_pagado'=> false,
            'pedido_id' => '1',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/11/14',
            'monto'     => '5000',
            'esta_pagado'=> true,
            'pedido_id' => '2',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/12/15',
            'monto'     => '2500',
            'esta_pagado'=> false,
            'pedido_id' => '2',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/12/30',
            'monto'     => '2500',
            'esta_pagado'=> false,
            'pedido_id' => '2',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/11/29',
            'monto'     => '450',
            'esta_pagado'=> false,
            'pedido_id' => '3',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/12/14',
            'monto'     => '250',
            'esta_pagado'=> false,
            'pedido_id' => '3',
        ]);

        DB::table('pagos_requeridos')->insert([
            'fecha'     => '2020/12/23',
            'monto'     => '250',
            'esta_pagado'=> false,
            'pedido_id' => '3',
        ]);
    }
}
