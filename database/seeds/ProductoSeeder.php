<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Producto;

class ProductoSeeder extends Seeder
{

    public function run()
    {
        DB::table('productos')->insert([
            'nombre'            => 'Camisa dama gabardina',
            'descripcion'       => 'Manga larga',
            'esta_activo'       => true,
            'tipo_producto_id'  => '5',
            'complejidad_id'    => '1',
        ]);

        DB::table('productos')->insert([
            'nombre'            => 'Short piqué',
            'descripcion'       => 'Cintura alta',
            'esta_activo'       => true,
            'tipo_producto_id'  => '2',
            'complejidad_id'    => '2',
        ]);

        DB::table('productos')->insert([
            'nombre'            => 'Pantalón palazzo crep',
            'descripcion'       => 'Cintura alta con lazo',
            'esta_activo'       => true,
            'tipo_producto_id'  => '3',
            'complejidad_id'    => '3',
        ]);

        DB::table('productos')->insert([
            'nombre'            => 'Camisa caballero gabardina',
            'descripcion'       => 'Manga larga',
            'esta_activo'       => true,
            'tipo_producto_id'  => '5',
            'complejidad_id'    => '1',
        ]);
    }
}
