<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DificultadTareaSeeder extends Seeder
{

    public function run()
    {
         DB::table('dificultades_tareas')->insert([
            'nombre' => 'Baja',
            'tiempo_ejecucion' => '1',
        ]);
   
        DB::table('dificultades_tareas')->insert([
            'nombre' => 'Media',
            'tiempo_ejecucion' => '3',
        ]);

        DB::table('dificultades_tareas')->insert([
            'nombre' => 'Alta',
            'tiempo_ejecucion' => '5',
        ]);
    }
}
