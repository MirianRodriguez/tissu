<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoTareaSeeder extends Seeder
{

    public function run()
    {
        DB::table('estados_tareas')->insert([
            'nombre' => 'Pendiente',
        ]);

        DB::table('estados_tareas')->insert([
            'nombre' => 'Asignado',
        ]);

        DB::table('estados_tareas')->insert([
            'nombre' => 'Cumplido',
        ]);
    }
}
