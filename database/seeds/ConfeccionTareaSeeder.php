<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ConfeccionTareaSeeder extends Seeder
{

    public function run()
    {
        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 1,
            'tarea_id'          => 1,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 1,
            'tarea_id'          => 2,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 1,
            'tarea_id'          => 3,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 1,
            'tarea_id'          => 4,
        ]);


        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 2,
            'tarea_id'          => 1,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 2,
            'tarea_id'          => 2,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 2,
            'tarea_id'          => 3,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 2,
            'tarea_id'          => 4,
        ]);


        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 3,
            'tarea_id'          => 1,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 3,
            'tarea_id'          => 2,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 3,
            'tarea_id'          => 3,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 3,
            'tarea_id'          => 4,
        ]);


        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 4,
            'tarea_id'          => 1,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 4,
            'tarea_id'          => 2,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 4,
            'tarea_id'          => 3,
        ]);


        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 5,
            'tarea_id'          => 1,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 5,
            'tarea_id'          => 2,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 5,
            'tarea_id'          => 3,
        ]);


        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 6,
            'tarea_id'          => 1,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 6,
            'tarea_id'          => 2,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 6,
            'tarea_id'          => 3,
        ]);


        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 7,
            'tarea_id'          => 1,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 7,
            'tarea_id'          => 2,
        ]);

        DB::table('confecciones_tareas')->insert([
            'estado_tarea_id'   => 1,
            'confeccion_id'     => 7,
            'tarea_id'          => 3,
        ]);

    }

    function fecha_aleatoria($formato = "Y-m-d", $limiteInferior = "1970-01-01", $limiteSuperior = "2038-01-01"){
        // Convertimos la fecha como cadena a milisegundos
        $milisegundosLimiteInferior = strtotime($limiteInferior);
        $milisegundosLimiteSuperior = strtotime($limiteSuperior);
    
        // Buscamos un número aleatorio entre esas dos fechas
        $milisegundosAleatorios = mt_rand($milisegundosLimiteInferior, $milisegundosLimiteSuperior);
    
        // Regresamos la fecha con el formato especificado y los milisegundos aleatorios
        return date($formato, $milisegundosAleatorios);
    }
}
