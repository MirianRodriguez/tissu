<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TareaSeeder extends Seeder
{

    public function run()
    {
        DB::table('tareas')->insert([
            'nombre'        => 'Molde',
        ]);

        DB::table('tareas')->insert([
            'nombre'        => 'Corte',
        ]);

        DB::table('tareas')->insert([
            'nombre'        => 'Costura',
        ]);

        DB::table('tareas')->insert([
            'nombre'        => 'Bordado',
        ]);
    }
}
