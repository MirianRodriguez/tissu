<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColorSeeder extends Seeder
{

    public function run()
    {
        DB::table('colores')->insert([
            'nombre' => 'Rojo',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Azul',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Amarillo',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Morado',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Anaranjado',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Verde',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Celeste',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Rosado',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Blanco',
        ]);

        DB::table('colores')->insert([
            'nombre' => 'Negro',
        ]);
    }
}
