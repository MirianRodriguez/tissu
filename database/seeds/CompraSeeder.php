<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Compra;

class CompraSeeder extends Seeder
{

    public function run()
    {
        DB::table('compras')->insert([
            'fecha' => '2019/12/12',
            'nro_comprobante' => '0001-10101010',
            'proveedor_id' => '1',
            'total' => '12500',
        ]);

        DB::table('compras')->insert([
            'fecha' => '2019/10/10',
            'nro_comprobante' => '0001-10101011',
            'proveedor_id' => '2',
            'total' => '1700',
        ]);

        DB::table('compras')->insert([
            'fecha' => '2020/08/10',
            'nro_comprobante' => '0001-10101012',
            'proveedor_id' => '1',
            'total' => '1520',
        ]);

        DB::table('compras')->insert([
            'fecha' => '2020/09/10',
            'nro_comprobante' => '0001-10101013',
            'proveedor_id' => '2',
            'total' => '1500',
        ]);
    }
}
