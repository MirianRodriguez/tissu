<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedidaRequeridaTipoProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '1',
            'tipo_producto_id'      => '1',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '2',
            'tipo_producto_id'      => '1',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '7',
            'tipo_producto_id'      => '1',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '12',
            'tipo_producto_id'      => '1',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '2',
            'tipo_producto_id'      => '2',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '3',
            'tipo_producto_id'      => '2',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '11',
            'tipo_producto_id'      => '2',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '2',
            'tipo_producto_id'      => '3',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '3',
            'tipo_producto_id'      => '3',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '2',
            'tipo_producto_id'      => '4',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '3',
            'tipo_producto_id'      => '4',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '11',
            'tipo_producto_id'      => '4',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '1',
            'tipo_producto_id'      => '5',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '2',
            'tipo_producto_id'      => '5',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '7',
            'tipo_producto_id'      => '5',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '8',
            'tipo_producto_id'      => '5',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '10',
            'tipo_producto_id'      => '5',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '12',
            'tipo_producto_id'      => '5',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '13',
            'tipo_producto_id'      => '5',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '14',
            'tipo_producto_id'      => '5',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '15',
            'tipo_producto_id'      => '6',
        ]);

        DB::table('medida_requerida_tipo_producto')->insert([
            'medida_requerida_id'   => '16',
            'tipo_producto_id'      => '6',
        ]);
    }
}
