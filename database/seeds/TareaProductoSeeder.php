<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TareaProductoSeeder extends Seeder
{

    public function run()
    {
        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => '1',
            'tarea_id'          => '1',
            'producto_id'       => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '1',
            'dificultad_id'     => '2',
            'tarea_id'          => '2',
            'producto_id'       => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '2',
            'dificultad_id'     => '3',
            'tarea_id'          => '3',
            'producto_id'       => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '3',
            'dificultad_id'     => '3',
            'tarea_id'          => '4',
            'producto_id'       => '1',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => '1',
            'tarea_id'          => '1',
            'producto_id'       => '2',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '1',
            'dificultad_id'     => '2',
            'tarea_id'          => '2',
            'producto_id'       => '2',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '2',
            'dificultad_id'     => '3',
            'tarea_id'          => '3',
            'producto_id'       => '2',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => '1',
            'tarea_id'          => '1',
            'producto_id'       => '3',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '1',
            'dificultad_id'     => '2',
            'tarea_id'          => '2',
            'producto_id'       => '3',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '2',
            'dificultad_id'     => '3',
            'tarea_id'          => '3',
            'producto_id'       => '3',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => null,
            'dificultad_id'     => '1',
            'tarea_id'          => '1',
            'producto_id'       => '4',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '1',
            'dificultad_id'     => '2',
            'tarea_id'          => '2',
            'producto_id'       => '4',
        ]);

        DB::table('tareas_productos')->insert([
            'predecesora_id'    => '2',
            'dificultad_id'     => '3',
            'tarea_id'          => '3',
            'producto_id'       => '4',
        ]);
    }
}
