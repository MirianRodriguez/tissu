<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParametroSeeder extends Seeder
{

    public function run()
    {
        DB::table('parametros')->insert([
            'clave'     => 'valor_dolar',
            'valor'     => '79.43',
            'updated_at'=> '2020-12-13',
        ]);
    }
}
