<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Complejidad;

class ComplejidadSeeder extends Seeder
{

    public function run()
    {
        DB::table('complejidades')->insert([
            'tipo' => 'Alta',
            'porcentaje_ganancia' => '150',
        ]);

        DB::table('complejidades')->insert([
            'tipo' => 'Media',
            'porcentaje_ganancia' => '125',
        ]);

        DB::table('complejidades')->insert([
            'tipo' => 'Baja',
            'porcentaje_ganancia' => '100',
        ]);
    }
}
