<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\TipoMaterial;

class TipoMaterialSeeder extends Seeder
{

    public function run()
    {
        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Gabardina',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '2',
            'costo'                     => '4.41',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Piqué',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '2',
            'costo'                     => '5.67',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Denim',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '2',
            'costo'                     => '6.92',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Tropical',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '2',
            'costo'                     => '3.15',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Crepp',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '2',
            'costo'                     => '4.41',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Botón 5 mm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.025',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Botón 10 mm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.025',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Botón 15 mm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.025',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Botón 20 mm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.025',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Botón 25 mm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.025',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Cierre 15 cm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.19',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Cierre 20 cm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.25',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Cierre 30 cm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.31',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Cierre 40 cm',
            'unidad_medida_por_bulto'   => 'Unidad',
            'unidad_medida_en_stock'    => 'Unidad',
            'proveedor_id'              => '1',
            'costo'                     => '0.31',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Elástico redondo',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '1',
            'costo'                     => '0.44',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Elástico plano 1 cm',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '1',
            'costo'                     => '0.25',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Elástico plano 2 cm',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '1',
            'costo'                     => '0.25',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Elástico plano 5 cm',
            'unidad_medida_por_bulto'   => 'Mt.',
            'unidad_medida_en_stock'    => 'Cm.',
            'proveedor_id'              => '1',
            'costo'                     => '0.31',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre'                    => 'Lentejuela',
            'unidad_medida_por_bulto'   => 'Kg.',
            'unidad_medida_en_stock'    => 'Gr.',
            'proveedor_id'              => '1',
            'costo'                     => '1.26',
        ]);
    }
}
