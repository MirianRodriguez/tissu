<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DetalleCompraSeeder extends Seeder
{

    public function run()
    {
        DB::table('detalles_compra')->insert([
            'cantidad' => '3',
            'precio_unitario' => '550',
            'material_id' => '1',
            'compra_id' => '2',
        ]);

        DB::table('detalles_compra')->insert([
            'cantidad' => '25',
            'precio_unitario' => '2',
            'material_id' => '6',
            'compra_id' => '2',
        ]);

        DB::table('detalles_compra')->insert([
            'cantidad' => '10',
            'precio_unitario' => '350',
            'material_id' => '2',
            'compra_id' => '1',
        ]);

        DB::table('detalles_compra')->insert([
            'cantidad' => '20',
            'precio_unitario' => '450',
            'material_id' => '3',
            'compra_id' => '1',
        ]);

        DB::table('detalles_compra')->insert([
            'cantidad' => '3',
            'precio_unitario' => '500',
            'material_id' => '3',
            'compra_id' => '4',
        ]);

        DB::table('detalles_compra')->insert([
            'cantidad' => '4',
            'precio_unitario' => '380',
            'material_id' => '2',
            'compra_id' => '3',
        ]);
    }
}
