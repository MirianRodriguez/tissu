<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\MedidaRequerida;

class MedidaRequeridaSeeder extends Seeder
{

    public function run()
    {
        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Contorno de busto/pecho',
            'descripcion'   => 'Tomar rodeando el pecho en la parte más saliente. Se añaden 4 cm para holgura.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Contorno de cintura',
            'descripcion'   => 'Tomar rodeando la cintura a la altura del codo.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Contorno de cadera',
            'descripcion'   => 'Tomar rodeando la cadera en la parte más saliente.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Contorno de cuello',
            'descripcion'   => 'Tomar rodeando el cuello en la parte baja, con holgura de modo que entre el dedo índice.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Contorno de brazo',
            'descripcion'   => 'Tomar rodeando el brazo debajo de la axila. Sumar 4 o 5 cm de flojedad',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Contorno de puño',
            'descripcion'   => 'Tomar rodeando la muñeca dejando 2 cm de holgura.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Largo de espalda',
            'descripcion'   => 'Se toma por el centro de la espalda desde el punto de unión con el cuello hasta la cintura.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Altura de busto',
            'descripcion'   => 'Desde el nacimiento del hombro junto al cuello hasta la punta del busto.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Altura de cadera',
            'descripcion'   => 'Desde la cintura hasta la parte más prominente de la cadera.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Largo de manga',
            'descripcion'   => 'Desde el nacimiento del brazo, en el hombro hasta la muñeca.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Largo',
            'descripcion'   => '',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Ancho de espalda',
            'descripcion'   => 'Se mide a la altura de la axila. Sumar 2 cm de holgura.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Ancho de hombro',
            'descripcion'   => 'Desde el nacimiento del cuello hasta el nacimiento del brazo.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Separación de busto',
            'descripcion'   => 'Desde la punta de un busto hasta la punta del otro.',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Ancho',
            'descripcion'   => '',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Alto',
            'descripcion'   => '',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Profundidad',
            'descripcion'   => '',
        ]);

        DB::table('medidas_requeridas')->insert([
            'nombre'        => 'Diámetro',
            'descripcion'   => '',
        ]);
    }
}
