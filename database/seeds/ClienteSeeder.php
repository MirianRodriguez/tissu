<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClienteSeeder extends Seeder
{

    public function run()
    {
        DB::table('clientes')->insert([
            'apellidos'     => 'Pluhator',
            'nombres'       => 'Gladys Susana',
            'telefono'      => '3758435612',
            'email'         => 'susanapluhator@gmail.com',
            'saldo'         => '50',
        ]);

        DB::table('clientes')->insert([
            'apellidos'     => 'Koyuk',
            'nombres'       => 'Florencia',
            'telefono'      => '3758453678',
            'email'         => 'florencia@gmail.com',

        ]);

        DB::table('clientes')->insert([
            'apellidos'     => 'Rodriguez',
            'nombres'       => 'Magno Marcelo',
            'telefono'      => '3758489923',
            'email'         => 'mirodriiguez8@gmail.com',

        ]);

        
        DB::table('clientes')->insert([
            'apellidos'     => 'Rodriguez',
            'nombres'       => 'Mirian Itati',
            'telefono'      => '3758487732',
            'email'         => 'mirodriiguez8@gmail.com',

        ]);
    }
}
