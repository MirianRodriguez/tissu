<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\TipoProducto;

class TipoProductoSeeder extends Seeder
{

    public function run()
    {
        DB::table('tipos_productos')->insert([
            'nombre'        => 'Remera',
            'descripcion'   => 'Remera simple.',
        ]);

        DB::table('tipos_productos')->insert([
            'nombre'        => 'Short',
            'descripcion'   => '',
        ]);

        DB::table('tipos_productos')->insert([
            'nombre'        => 'Pantalón',
            'descripcion'   => '',
        ]);

        DB::table('tipos_productos')->insert([
            'nombre'        => 'Pollera',
            'descripcion'   => '',
        ]);

        DB::table('tipos_productos')->insert([
            'nombre'        => 'Camisa',
            'descripcion'   => '',
        ]);

        DB::table('tipos_productos')->insert([
            'nombre'        => '2D',
            'descripcion'   => 'Productos simples con medidas de alto y ancho.',
        ]);
    }
}
