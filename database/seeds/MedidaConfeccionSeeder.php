<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedidaConfeccionSeeder extends Seeder
{
    public function run()
    {
        DB::table('medidas_confeccion')->insert([
            'valor'                 => '90',
            'confeccion_id'         => '1',
            'medida_requerida_id'   => '1',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '100',
            'confeccion_id'         => '1',
            'medida_requerida_id'   => '2',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '50',
            'confeccion_id'         => '1',
            'medida_requerida_id'   => '10',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '50',
            'confeccion_id'         => '1',
            'medida_requerida_id'   => '12',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '95',
            'confeccion_id'         => '2',
            'medida_requerida_id'   => '1',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '115',
            'confeccion_id'         => '2',
            'medida_requerida_id'   => '2',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '49',
            'confeccion_id'         => '2',
            'medida_requerida_id'   => '10',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '53',
            'confeccion_id'         => '2',
            'medida_requerida_id'   => '12',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '100',
            'confeccion_id'         => '3',
            'medida_requerida_id'   => '1',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '83',
            'confeccion_id'         => '3',
            'medida_requerida_id'   => '2',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '52',
            'confeccion_id'         => '3',
            'medida_requerida_id'   => '10',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '53',
            'confeccion_id'         => '3',
            'medida_requerida_id'   => '12',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '75',
            'confeccion_id'         => '4',
            'medida_requerida_id'   => '2',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '100',
            'confeccion_id'         => '4',
            'medida_requerida_id'   => '3',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '105',
            'confeccion_id'         => '5',
            'medida_requerida_id'   => '2',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '120',
            'confeccion_id'         => '5',
            'medida_requerida_id'   => '3',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '60',
            'confeccion_id'         => '5',
            'medida_requerida_id'   => '11',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '85',
            'confeccion_id'         => '6',
            'medida_requerida_id'   => '2',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '100',
            'confeccion_id'         => '6',
            'medida_requerida_id'   => '3',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '115',
            'confeccion_id'         => '7',
            'medida_requerida_id'   => '1',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '110',
            'confeccion_id'         => '7',
            'medida_requerida_id'   => '2',
        ]);

        DB::table('medidas_confeccion')->insert([
            'valor'                 => '70',
            'confeccion_id'         => '7',
            'medida_requerida_id'   => '7',
        ]);

    }
}
