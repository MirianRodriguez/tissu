<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(RolSeeder::class);

        /*FK: Rol*/ 
        $this->call(UserSeeder::class);
        
        $this->call(RubroSeeder::class);
        
        $this->call(ColorSeeder::class);
        
        $this->call(UnidadMedidaSeeder::class);

        $this->call(ProveedorSeeder::class);

        /* FK unidad de medida y proveedor*/
        $this->call(TipoMaterialSeeder::class);

        /* FK tipo de material, color, rubro*/
        $this->call(MaterialSeeder::class);

        /* FK material y proveedor */
        $this->call(CompraSeeder::class);

        /* FK compra */
        $this->call(DetalleCompraSeeder::class);

        $this->call(MedidaRequeridaSeeder::class);

        $this->call(TipoProductoSeeder::class);

        /* FK medida y tipo producto */
        $this->call(MedidaRequeridaTipoProductoSeeder::class);

        $this->call(ComplejidadSeeder::class);

        /* FK tipo de producto y complejidad */
        $this->call(ProductoSeeder::class);

        /* FK producto y tipo material */
        $this->call(TipoMaterialProductoSeeder::class);

        $this->call(ClienteSeeder::class);

        /* FK cliente*/
        $this->call(PedidoSeeder::class);

        /* FK pedido */
        $this->call(ConfeccionSeeder::class);

        /* FK confeccion y material */
        $this->call(MaterialConfeccionSeeder::class);

        /* FK confeccion y medidas requeridas */
        $this->call(MedidaConfeccionSeeder::class);

        $this->call(DificultadTareaSeeder::class);

        $this->call(EstadoTareaSeeder::class);

        /* FK dificultad tarea*/
        $this->call(TareaSeeder::class);

        $this->call(TareaProductoSeeder::class);

        $this->call(ConfeccionTareaSeeder::class);

        $this->call(ParametroSeeder::class);

        $this->call(PagoRequeridoSeeder::class);

        $this->call(MovimientoSeeder::class);
        
    }
}
