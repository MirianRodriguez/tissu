<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration
{
    public function up()
    {
        /*Por cada par de tablas que deseo vincular crear un esquema dentro de la funcion up*/ 
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('rol_id')->references('id')->on('roles');
        });

        Schema::table('materiales', function (Blueprint $table) {
            $table->foreign('rubro_id')->references('id')->on('rubros');
        });

        Schema::table('materiales', function (Blueprint $table) {
            $table->foreign('color_id')->references('id')->on('colores');
        });

        Schema::table('materiales', function (Blueprint $table) {
            $table->foreign('tipo_material_id')->references('id')->on('tipos_materiales');
        });

        Schema::table('compras', function (Blueprint $table) {
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
        });

        Schema::table('detalles_compra', function (Blueprint $table) {
            $table->foreign('compra_id')->references('id')->on('compras');
        });

        Schema::table('detalles_compra', function (Blueprint $table) {
            $table->foreign('material_id')->references('id')->on('materiales');
        });

        Schema::table('medida_requerida_tipo_producto', function (Blueprint $table) {
            $table->foreign('tipo_producto_id')->references('id')->on('tipos_productos');
            $table->foreign('medida_requerida_id')->references('id')->on('medidas_requeridas');
        });

        Schema::table('tipos_materiales', function(Blueprint $table) {
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
        });

        Schema::table('tipos_materiales_productos', function (Blueprint $table) {
            $table->foreign('tipo_material_id')->references('id')->on('tipos_materiales');
            $table->foreign('producto_id')->references('id')->on('productos');
        });

        Schema::table('productos', function (Blueprint $table) {
            $table->foreign('tipo_producto_id')->references('id')->on('tipos_productos');
            $table->foreign('complejidad_id')->references('id')->on('complejidades');
        });

        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('cliente_id')->references('id')->on('clientes');
        });

        Schema::table('pagos_requeridos', function (Blueprint $table) {
            $table->foreign('pedido_id')->references('id')->on('pedidos');
        });

        Schema::table('movimientos', function (Blueprint $table) {
            $table->foreign('cliente_id')->references('id')->on('clientes');
        });

        Schema::table('confecciones', function (Blueprint $table) {
            $table->foreign('pedido_id')->references('id')->on('pedidos');
            $table->foreign('producto_id')->references('id')->on('productos');
        });

        Schema::table('medidas_confeccion', function (Blueprint $table) {
            $table->foreign('confeccion_id')->references('id')->on('confecciones');
            $table->foreign('medida_requerida_id')->references('id')->on('medidas_requeridas');
        });

        Schema::table('material_confeccion', function (Blueprint $table) {
            $table->foreign('confeccion_id')->references('id')->on('confecciones');
            $table->foreign('material_id')->references('id')->on('materiales');
        });

        Schema::table('asignaciones', function (Blueprint $table) {
            $table->foreign('confeccion_tarea_id')->references('id')->on('confecciones_tareas');
            $table->foreign('usuario_id')->references('id')->on('users');
        });

        Schema::table('tareas_productos', function (Blueprint $table) {
            $table->foreign('predecesora_id')->references('id')->on('tareas');
            $table->foreign('dificultad_id')->references('id')->on('dificultades_tareas');
            $table->foreign('producto_id')->references('id')->on('productos');
            $table->foreign('tarea_id')->references('id')->on('tareas');
        });

        Schema::table('confecciones_tareas', function (Blueprint $table) {
            $table->foreign('tarea_id')->references('id')->on('tareas');
            $table->foreign('confeccion_id')->references('id')->on('confecciones');
            $table->foreign('estado_tarea_id')->references('id')->on('estados_tareas');
            $table->foreign('asignacion_id')->references('id')->on('asignaciones');
        });

        Schema::table('ajustes_stock', function (Blueprint $table) {
            $table->foreign('material_id')->references('id')->on('materiales');
        });
    }

    public function down()
    {
        Schema::dropIfExists('foreign_keys');
    }
}
