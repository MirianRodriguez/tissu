<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialConfeccionTable extends Migration
{

    public function up()
    {
        Schema::create('material_confeccion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('confeccion_id')->required();
            $table->unsignedBigInteger('material_id')->required();
        });
    }

    public function down()
    {
        Schema::dropIfExists('material_confeccion');
    }
}
