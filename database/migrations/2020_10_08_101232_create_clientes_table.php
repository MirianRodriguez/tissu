<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{

    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('apellidos');
            $table->string('nombres');
            $table->string('telefono');
            $table->string('email');
            $table->double('saldo')->nullable()->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
