<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedidasConfeccionTable extends Migration
{

    public function up()
    {
        Schema::create('medidas_confeccion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('valor')->nullable();
            $table->unsignedBigInteger('confeccion_id');
            $table->unsignedBigInteger('medida_requerida_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('medidas_confeccion');
    }
}
