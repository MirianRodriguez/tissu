<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedidaRequeridaTipoProductoTable extends Migration
{

    public function up()
    {
        Schema::create('medida_requerida_tipo_producto', function (Blueprint $table) {
            $table->unsignedBigInteger('medida_requerida_id');
            $table->unsignedBigInteger('tipo_producto_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('medida_requerida_tipo_producto');
    }
}
