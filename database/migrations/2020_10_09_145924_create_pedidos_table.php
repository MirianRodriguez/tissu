<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('presupuesto')->nullable();
            $table->date('fecha_pactada')->nullable();
            $table->date('fecha');
            $table->double('porcentaje_avance')->nullable()->default(0);
            $table->string('comentario')->nullable();
            $table->unsignedBigInteger('cliente_id')->required();
            $table->boolean('entregado')->default(false);
        });
    }

    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
