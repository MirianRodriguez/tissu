<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfeccionesTable extends Migration
{

    public function up()
    {
        Schema::create('confecciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('comentario')->nullable();
            $table->integer('cantidad')->default(1);
            $table->unsignedBigInteger('producto_id');
            $table->unsignedBigInteger('pedido_id');
        });
    }


    public function down()
    {
        Schema::dropIfExists('confecciones');
    }
}
