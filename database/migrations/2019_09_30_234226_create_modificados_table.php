<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModificadosTable extends Migration
{
    public function up()
    {
        Schema::create('modificados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tabla');
            $table->string('id_modificado');
            $table->string('clave');
            $table->string('valor_anterior')->nullable();
            $table->string('valor_nuevo')->nullable();
            $table->string('fecha');
            $table->unsignedBigInteger('user_id')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('modificados');
    }
}
