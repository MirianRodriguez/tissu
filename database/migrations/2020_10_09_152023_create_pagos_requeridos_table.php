<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagosRequeridosTable extends Migration
{

    public function up()
    {
        Schema::create('pagos_requeridos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha')->required();
            $table->double('monto')->required();
            $table->boolean('esta_pagado')->default(false);
            $table->unsignedBigInteger('pedido_id')->required();
        });
    }


    public function down()
    {
        Schema::dropIfExists('pagos_requeridos');
    }
}
