<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposMaterialesProductosTable extends Migration
{

    public function up()
    {
        Schema::create('tipos_materiales_productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('cantidad_necesaria')->required();
            $table->unsignedBigInteger('tipo_material_id')->required();
            $table->unsignedBigInteger('producto_id')->required();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tipos_materiales_productos');
    }
}
