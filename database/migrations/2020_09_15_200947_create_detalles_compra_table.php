<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallesCompraTable extends Migration
{

    public function up()
    {
        Schema::create('detalles_compra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cantidad');
            $table->double('precio_unitario');
            $table->unsignedBigInteger('material_id');
            $table->unsignedBigInteger('compra_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('detalles_compra');
    }
}
