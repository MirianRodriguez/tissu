<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposProductosTable extends Migration
{

    public function up()
    {
        Schema::create('tipos_productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->required()->unique();
            $table->string('descripcion')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tipos_productos');
    }
}
