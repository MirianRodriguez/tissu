<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComprasTable extends Migration
{
 
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha')->required();
            $table->string('nro_comprobante')->required();
            $table->unsignedBigInteger('proveedor_id')->required();
            $table->double('total')->required();
        });
    }

    public function down()
    {
        Schema::dropIfExists('compras');
    }
}
