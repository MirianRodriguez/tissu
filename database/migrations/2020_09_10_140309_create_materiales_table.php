<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialesTable extends Migration
{
    
    public function up()
    {
        Schema::create('materiales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo')->nullable(); 
            $table->double('stock')->default(0)->nullable();
            $table->double('reservado')->default(0)->nullable();
            $table->double('faltante')->default(0)->nullable();
            $table->boolean('es_liso')->default(false);
            $table->string('descripcion')->nullable();   
            $table->unsignedBigInteger('tipo_material_id'); //claves foraneas siempre este formato
            $table->unsignedBigInteger('rubro_id')->nullable(); 
            $table->unsignedBigInteger('color_id')->nullable();       
        });
    }

    public function down()
    {
        Schema::dropIfExists('materiales');
    }
}
