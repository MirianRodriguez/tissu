<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedidasRequeridasTable extends Migration
{

    public function up()
    {
        Schema::create('medidas_requeridas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->required();
            $table->string('descripcion')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('medidas_requeridas');
    }
}
