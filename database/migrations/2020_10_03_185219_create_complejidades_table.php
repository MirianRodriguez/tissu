<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplejidadesTable extends Migration
{

    public function up()
    {
        Schema::create('complejidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo')->required();
            $table->integer('porcentaje_ganancia')->required();
        });
    }

    public function down()
    {
        Schema::dropIfExists('complejidades');
    }
}
