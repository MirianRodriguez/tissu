<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProveedoresTable extends Migration
{
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->required();
            $table->string('telefono')->required();
            $table->string('email')->required();
            $table->double('costo_envio')->nullable();
            $table->integer('demora')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('proveedores');
    }
}
