<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreadosTable extends Migration
{
    public function up()
    {
        Schema::create('creados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tabla');
            $table->string('id_creado');
            $table->string('clave');
            $table->string('valor');
            $table->string('fecha');
            $table->unsignedBigInteger('user_id')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('creados');
    }
}
